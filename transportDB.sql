drop database if exists transport;
create database transport CHARACTER SET 'utf8';
use transport;

drop table if exists tbProvider;
create table tbProvider
(
provider_id varchar(3) not null,
provider_name varchar(16) NOT NULL,
detail varchar(128),
PRIMARY KEY (provider_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

LOCK TABLES tbProvider WRITE;
INSERT INTO tbProvider VALUES ('YTA','YTAsia',null);
INSERT INTO tbProvider VALUES ('AMZ','Amazon',null);
INSERT INTO tbProvider VALUES ('RKT','Rakuten',null);
UNLOCK TABLES;


drop table if exists tbUser;
create table  tbUser
(
user_id int AUTO_INCREMENT,
email varchar(64) not null unique,
name varchar(64) not null,
password varchar(128) NOT NULL, -- hash128
type int, -- 0:superadmin, 1:admin, 2: user
status int, -- 0:active ,1:deactive 
PRIMARY KEY (user_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

INSERT INTO tbUser VALUES (null,'phucnt@ytasia.co.jp','Nguyen Trong Phuc', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,0);
INSERT INTO tbUser VALUES (null,'truongnv@ytasia.co.jp','Nguyen Van Truong', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',2,0);
INSERT INTO tbUser VALUES (null,'hoatd@ytasia.co.jp','Tran Duc Hoa', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,0);


drop table if exists tbCustomerType;
create table tbCustomerType
(
type_id int not null,
type_name varchar(16) NOT NULL,
detail varchar(128),
PRIMARY KEY (type_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

LOCK TABLES tbCustomerType WRITE;
INSERT INTO tbCustomerType VALUES (1,'Normal',null);
INSERT INTO tbCustomerType VALUES (2,'Discount',null);
INSERT INTO tbCustomerType VALUES (3,'Loyal',null);
UNLOCK TABLES;


drop table if exists tbCustomer;
create table tbCustomer
(
customer_id int AUTO_INCREMENT,
name varchar(64),
email varchar(32),
adress varchar(128),
phone varchar(16),
type_id int, -- 1:normal, 2:discount, 3:loyal
detail varchar(128),
foreign key (type_id) references tbCustomerType(type_id),
PRIMARY KEY (customer_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

LOCK TABLES tbCustomer WRITE;
INSERT INTO tbCustomer VALUES (null,'JAPO','japo@ytasia.co.jp','takadanobaba','00000000001',1,null);
INSERT INTO tbCustomer VALUES (null,'phuc','phucnt@ytasia.co.jp','otsuka','00000000002',1,null);
INSERT INTO tbCustomer VALUES (null,'duy','duyla@ytasia.co.jp','ikebukuro','00000000003',1,null);
INSERT INTO tbCustomer VALUES (null,'truong','truongnv@ytasia.co.jp','shin-okubo','00000000004',2,null);
INSERT INTO tbCustomer VALUES (null,'a','a@ytasia.co.jp','shin-okubo','00000000004',2,null);
INSERT INTO tbCustomer VALUES (null,'b','b@ytasia.co.jp','shin-okubo','00000000004',2,null);
INSERT INTO tbCustomer VALUES (null,'c','truongnv@ytasia.co.jp','shin-okubo','00000000004',2,null);
INSERT INTO tbCustomer VALUES (null,'d','c@ytasia.co.jp','shin-okubo','00000000004',3,null);
INSERT INTO tbCustomer VALUES (null,'s','s@ytasia.co.jp','shin-okubo','00000000004',3,null);
INSERT INTO tbCustomer VALUES (null,'t','t@ytasia.co.jp','shin-okubo','00000000004',1,null);
INSERT INTO tbCustomer VALUES (null,'g','g@ytasia.co.jp','shin-okubo','00000000004',1,null);
INSERT INTO tbCustomer VALUES (null,'h','h@ytasia.co.jp','shin-okubo','00000000004',1,null);
INSERT INTO tbCustomer VALUES (null,'u','u@ytasia.co.jp','shin-okubo','00000000004',1,null);
UNLOCK TABLES;


drop table if exists tbStatus;
create table tbStatus
(
status_id varchar(3) not null,
status_name varchar(64) NOT NULL,
detail varchar(128),
PRIMARY KEY (status_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

LOCK TABLES tbStatus WRITE;
INSERT INTO tbStatus VALUES ('AVA','Available-OK',null);
INSERT INTO tbStatus VALUES ('NOT','Not Available',null);
INSERT INTO tbStatus VALUES ('WRO','Wrong',null);
INSERT INTO tbStatus VALUES ('DET','Deleted',null);
UNLOCK TABLES;


drop table if exists tbLocation;
create table tbLocation
(
location_id varchar(3) not null,
location_name varchar(64) NOT NULL,
detail varchar(128),
PRIMARY KEY (location_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

LOCK TABLES tbLocation WRITE;
INSERT INTO tbLocation VALUES ('WAI','Waiting',null);
INSERT INTO tbLocation VALUES ('JPI','In Japan',null);
INSERT INTO tbLocation VALUES ('JPO','Out Japan',null);
INSERT INTO tbLocation VALUES ('VNI','In Vietnam',null);
INSERT INTO tbLocation VALUES ('VNO','Out Vietnam',null);
UNLOCK TABLES;


drop table if exists tbProductType;
create table tbProductType
(
type_id int AUTO_INCREMENT,
type_name varchar(64) NOT NULL,
unit varchar(16) not null,
price int not null,
detail varchar(128),
PRIMARY KEY (type_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

LOCK TABLES tbProductType WRITE;
INSERT INTO tbProductType VALUES (null,'Bình thường','kg',1100,null);
INSERT INTO tbProductType VALUES (null,'Thực phẩm','kg',1050,null);
INSERT INTO tbProductType VALUES (null,'Đồ chơi trẻ em','kg',1050,null);
INSERT INTO tbProductType VALUES (null,'Quần áo','kg',1100,null);
INSERT INTO tbProductType VALUES (null,'Mỹ phẩm - thuốc','kg',1100,null);
INSERT INTO tbProductType VALUES (null,'Nước hoa','chiếc',700,null);
INSERT INTO tbProductType VALUES (null,'Trang sức','chiếc',1500,null);
INSERT INTO tbProductType VALUES (null,'Máy nghe nhạc','chiếc',2000,null);
INSERT INTO tbProductType VALUES (null,'Smart phone - tablet','chiếc',3000,null);
INSERT INTO tbProductType VALUES (null,'Loa - ampli','kg',1200,null);
INSERT INTO tbProductType VALUES (null,'Đồng hồ','chiếc',1000,null);
INSERT INTO tbProductType VALUES (null,'Hàng cồng kềnh 1','kg',700,null);
INSERT INTO tbProductType VALUES (null,'Hàng cồng kềnh 2','kg',650,null);
INSERT INTO tbProductType VALUES (99,'japo','',0,null);
UNLOCK TABLES;


drop table if exists tbShipment;
create table tbShipment
(
id int AUTO_INCREMENT,
shipment_id varchar(16) unique not null,
depart_time date null,
delivery_time date null,
user_id int,
detail varchar(128),
PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;


LOCK TABLES tbShipment WRITE;
INSERT INTO tbShipment VALUES (null,'5d222-4387','2016-04-24','2016-04-25',1,null);
INSERT INTO tbShipment VALUES (null,'5d333-4387','2016-05-24','2016-05-25',1,null);
UNLOCK TABLES;


drop table if exists tbTransportOrder;
create table tbTransportOrder
(
id int AUTO_INCREMENT,
transport_order_id varchar(16) not null unique,
shipment_id varchar(16),
provider_id varchar(3),
status_id varchar(3),
location_id varchar(3),
sender_id int,
receipter_id int,
provider_order_id varchar(64),
transport_code varchar(64),
other varchar(128),
authen_code varchar(64),
paid_status int, -- 0: not pay yet, 1: paid
is_ship int,
foreign key (shipment_id) references tbShipment(shipment_id),
foreign key (sender_id) references tbCustomer(customer_id),
foreign key (receipter_id) references tbCustomer(customer_id),
foreign key (provider_id) references tbProvider(provider_id),
foreign key (status_id) references tbStatus(status_id),
foreign key (location_id) references tbLocation(location_id),
PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

LOCK TABLES tbTransportOrder WRITE;
INSERT INTO tbTransportOrder VALUES (null,'6e222-0001','5d222-4387','AMZ', 'AVA', 'JPI',2, 3, '4902778692448', '480156905550',null, null,1, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0002','5d222-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,1, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0003','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,1, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0004','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0005','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0006','5d333-4387','AMZ', 'AVA', 'WAI',3, 4, '4902778692462', '379253948570',null, null,1, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0007','5d333-4387','AMZ', 'AVA', 'WAI',3, 4, '4902778692462', '379253948570',null, null,1, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0008','5d333-4387','AMZ', 'AVA', 'WAI',3, 4, '4902778692462', '379253948570',null, null,1, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0009','5d333-4387','AMZ', 'AVA', 'WAI',3, 4, '4902778692462', '379253948570',null, null,1, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0010','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,1, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0011','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,1, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0012','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,1, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0013','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,1, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0014','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,1, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0015','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,1, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0016','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,1, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0017','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,1, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0018','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0019','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0020','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0021','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0022','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0023','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0024','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0025','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0026','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0027','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0028','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0029','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0030','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0031','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0032','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0033','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0034','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0035','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0036','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0037','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0038','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0039','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0040','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
INSERT INTO tbTransportOrder VALUES (null,'6e222-0041','5d333-4387','AMZ', 'AVA', 'JPI',3, 4, '4902778692462', '379253948570',null, null,0, 0);
UNLOCK TABLES;


drop table if exists tbProduct;
create table tbProduct
(
id int AUTO_INCREMENT,
product_id varchar(16) not null unique,
transport_order_id varchar(16) not null,
name varchar(64) not null,
quantity float NOT NULL,
type_id int, -- 1: normal, 2
attachment varchar(64),
other varchar(128),
foreign key (transport_order_id) references tbTransportOrder(transport_order_id),
foreign key (type_id) references tbProductType(type_id),
PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

LOCK TABLES tbProduct WRITE;
INSERT INTO tbProduct VALUES (null,'7f999-0001','6e222-0001','sua',10,1,null,null);
INSERT INTO tbProduct VALUES (null,'7f999-0002','6e222-0001','dong ho',2,1,null,null);
INSERT INTO tbProduct VALUES (null,'7f999-0003','6e222-0002','my pham',10,1,null,null);
INSERT INTO tbProduct VALUES (null,'7f999-0004','6e222-0002','do choi',2,1,null,null);
UNLOCK TABLES;


drop table if exists tbSubProduct;
create table tbSubProduct
(
id int AUTO_INCREMENT,
sub_product_id varchar(16) not null unique,
transport_order_id varchar(16) not null,
location_id varchar(3),
foreign key (transport_order_id) references tbTransportOrder(transport_order_id),
foreign key (location_id) references tbLocation(location_id),
PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

LOCK TABLES tbSubProduct WRITE;
INSERT INTO tbSubProduct VALUES (null,'6e222-0001-1-2','6e222-0001','JPI');
INSERT INTO tbSubProduct VALUES (null,'6e222-0001-2-2','6e222-0001','JPI');
UNLOCK TABLES;


drop table if exists tbStatusHistory;
create table tbStatusHistory
(
id int AUTO_INCREMENT,
transport_order_id varchar(16) not null ,
status_id varchar(3) not null,
time timestamp null,
user_id int,
detail varchar(512) null,
foreign key (transport_order_id) references tbTransportOrder(transport_order_id),
foreign key (status_id) references tbStatus(status_id),
foreign key (user_id) references tbUser(user_id),
PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

LOCK TABLES tbStatusHistory WRITE;
INSERT INTO 	tbStatusHistory VALUES (null,'6e222-0001','AVA','2016-04-24 14:24',1,null);
INSERT INTO tbStatusHistory VALUES (null,'6e222-0001','NOT','2016-06-26 16:26',3,null);
INSERT INTO tbStatusHistory VALUES (null,'6e222-0001','AVA','2016-09-29 19:29',2,null);
UNLOCK TABLES;


drop table if exists tbLocationHistory;
create table tbLocationHistory
(
id int AUTO_INCREMENT,
transport_order_id varchar(16) not null,
location_id varchar(3) not null,
time timestamp null,
user_id int,
detail varchar(512) null,
foreign key (transport_order_id) references tbTransportOrder(transport_order_id),
foreign key (location_id) references tbLocation(location_id),
foreign key (user_id) references tbUser(user_id),
PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

LOCK TABLES tbLocationHistory WRITE;
INSERT INTO tbLocationHistory VALUES (null,'6e222-0001','WAI','2016-04-01 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0001','JPI','2016-04-25 16:26',3,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0001','JPO','2016-04-26 19:29',2,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0001','VNI','2016-04-27 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0001','VNO','2016-04-28 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0002','WAI','2016-04-24 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0002','JPI','2016-06-25 16:26',3,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0002','JPO','2016-09-26 19:29',2,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0003','JPI','2016-04-05 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0004','JPI','2016-04-05 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0005','JPI','2016-04-05 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0010','JPI','2016-04-13 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0011','JPI','2016-04-13 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0012','JPI','2016-04-13 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0013','JPI','2016-04-15 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0014','JPI','2016-04-16 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0014','JPI','2016-04-18 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0015','JPI','2016-04-22 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0016','JPI','2016-04-24 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0017','JPI','2016-04-24 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0018','JPI','2016-04-29 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0019','JPI','2016-04-29 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0020','JPI','2016-04-30 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0021','JPI','2016-05-30 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0022','JPI','2016-05-30 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0023','JPI','2016-06-30 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0024','JPI','2016-06-30 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0025','JPI','2016-06-30 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0026','JPI','2016-07-30 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0027','JPI','2016-08-30 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0028','JPI','2016-08-30 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0029','JPI','2016-08-30 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0030','JPI','2016-08-30 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0031','JPI','2016-08-30 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0032','JPI','2016-08-30 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0033','JPI','2016-10-30 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0034','JPI','2016-11-30 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0035','JPI','2016-11-30 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0036','JPI','2016-11-30 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0037','JPI','2016-12-30 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0038','JPI','2016-12-30 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0039','JPI','2016-12-30 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0040','JPI','2016-12-30 14:24',1,null);
INSERT INTO tbLocationHistory VALUES (null,'6e222-0041','JPI','2016-12-30 14:24',1,null);
UNLOCK TABLES;


drop table if exists tbCustomerHistory;
create table tbCustomerHistory
(
id int AUTO_INCREMENT,
customer_id int,
time timestamp null,
user_id int,
foreign key (customer_id) references tbCustomer(customer_id),
foreign key (user_id) references tbUser(user_id),
PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

LOCK TABLES tbCustomerHistory WRITE;
INSERT INTO tbCustomerHistory VALUES (null,1,'2016-04-24 14:24',1);
INSERT INTO tbCustomerHistory VALUES (null,2,'2016-04-24 14:24',1);
INSERT INTO tbCustomerHistory VALUES (null,3,'2016-04-24 14:24',1);
INSERT INTO tbCustomerHistory VALUES (null,4,'2016-04-24 14:24',1);
INSERT INTO tbCustomerHistory VALUES (null,5,'2016-07-24 14:24',1);
INSERT INTO tbCustomerHistory VALUES (null,6,'2016-07-24 14:24',1);
INSERT INTO tbCustomerHistory VALUES (null,7,'2016-07-24 14:24',1);
INSERT INTO tbCustomerHistory VALUES (null,8,'2016-08-24 14:24',1);
INSERT INTO tbCustomerHistory VALUES (null,9,'2016-08-24 14:24',1);
INSERT INTO tbCustomerHistory VALUES (null,10,'2016-08-24 14:24',1);
INSERT INTO tbCustomerHistory VALUES (null,11,'2016-08-24 14:24',1);
INSERT INTO tbCustomerHistory VALUES (null,12,'2016-08-24 14:24',1);
INSERT INTO tbCustomerHistory VALUES (null,13,'2016-09-24 14:24',1);
UNLOCK TABLES;


drop table if exists tbAttachment;
create table tbAttachment
(
id int AUTO_INCREMENT,
transport_order_id varchar(16) not null,
content varchar(64) not null,
detail varchar(128),
foreign key (transport_order_id) references tbTransportOrder(transport_order_id),
PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

LOCK TABLES tbLocation WRITE;
UNLOCK TABLES;