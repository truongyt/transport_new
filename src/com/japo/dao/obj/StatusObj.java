package com.japo.dao.obj;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author PhucNT
 *
 */

@Entity
@Table(name = "tbStatus")
public class StatusObj extends AbstractObj {
	@Id
	@Column(name = "status_id")
	String statusId;
	@Column(name = "status_name")
	String statusName;
	@Column(name = "detail")
	String detail;

	public StatusObj() {
		// TODO Auto-generated constructor stub
	}

	public StatusObj(String id, String name) {
		// TODO Auto-generated constructor stub
		this.statusId = id;
		this.statusName = name;
	}

	public String getStatusId() {
		return statusId;
	}

	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

}
