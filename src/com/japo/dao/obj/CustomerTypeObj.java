package com.japo.dao.obj;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author PhucNT
 *
 */

@Entity
@Table(name = "tbCustomerType")
public class CustomerTypeObj extends AbstractObj {
	@Id
	@Column(name = "type_id")
	int id;
	@Column(name = "type_name")
	String type;
	@Column(name = "detail")
	String detail;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}
}
