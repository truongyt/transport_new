package com.japo.dao.obj;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author PhucNT
 *
 */

@Entity
@Table(name = "tbCustomerHistory")
public class CustomerHistoryObj extends AbstractObj {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	int id;
	@Column(name = "customer_id")
	int customerId;
	@Column(name = "time")
	Timestamp time;
	@Column(name = "user_id")
	int userId;

	public CustomerHistoryObj() {
		// TODO Auto-generated constructor stub
		this.time = new Timestamp(new Date().getTime());
	}

	public CustomerHistoryObj(int id, int customerId, Timestamp time, int userId) {
		this.id = id;
		this.customerId = customerId;
		this.time = new Timestamp(new Date().getTime());
		this.userId = userId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}
