package com.japo.dao.obj;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author PhucNT
 *
 */

@Entity
@Table(name = "tbLocation")
public class LocationObj extends AbstractObj {

	@Id
	@Column(name = "location_id")
	String locationId;
	@Column(name = "location_name")
	String locationName;
	@Column(name = "detail")
	String detail;

	public LocationObj() {
		// TODO Auto-generated constructor stub
	}

	public LocationObj(String locationId, String locationName) {
		this.locationId = locationId;
		this.locationName = locationName;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

}
