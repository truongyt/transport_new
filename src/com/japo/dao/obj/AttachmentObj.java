package com.japo.dao.obj;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author PhucNT
 *
 */

@Entity
@Table(name = "tbAttachment")
public class AttachmentObj extends AbstractObj {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	int id;
	@Column(name = "transport_order_id")
	String transOrderId;
	@Column(name = "content")
	String content;
	@Column(name = "detail")
	String detail;

	public AttachmentObj() {
		// TODO Auto-generated constructor stub
	}

	public AttachmentObj(String content) {
		// TODO Auto-generated constructor stub
		this.content = content;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTransOrderId() {
		return transOrderId;
	}

	public void setTransOrderId(String transOrderId) {
		this.transOrderId = transOrderId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}
}
