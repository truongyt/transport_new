package com.japo.dao.obj;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.japo.Util.Util;

/**
 * 
 * @author PhucNT
 *
 */

@Entity
@Table(name = "tbTransportOrder")
public class TransportOrderObj extends AbstractObj {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	int id;
	@Column(name = "transport_order_id")
	String transportOrderId;
	@Column(name = "shipment_id")
	String shipmentId;
	@Column(name = "provider_id")
	String providerId;
	@Column(name = "status_id")
	String statusId;
	@Column(name = "location_id")
	String locationId;
	@Column(name = "sender_id")
	Integer senderId;
	@Column(name = "receipter_id")
	Integer receipterId;
	@Column(name = "provider_order_id")
	String providerOrderId;
	@Column(name = "transport_code")
	String transCode;
	@Column(name = "other")
	String other;
	@Column(name = "authen_code")
	String authenCode;
	@Column(name = "paid_status")
	Integer paidStatus;
	@Column(name = "is_ship")
	Integer isShip;

	public TransportOrderObj() {
		// TODO Auto-generated constructor stub
		this.transportOrderId = Util.generateId();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getTransportOrderId() {
		return transportOrderId;
	}

	public void setTransportOrderId(String transportOrderId) {
		this.transportOrderId = transportOrderId;
	}

	public String getShipmentId() {
		return shipmentId;
	}

	public void setShipmentId(String shipmentId) {
		this.shipmentId = shipmentId;
	}

	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getStatusId() {
		return statusId;
	}

	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public Integer getSenderId() {
		return senderId;
	}

	public void setSenderId(Integer senderId) {
		this.senderId = senderId;
	}

	public Integer getReceipterId() {
		return receipterId;
	}

	public void setReceipterId(Integer receipterId) {
		this.receipterId = receipterId;
	}

	public String getProviderOrderId() {
		return providerOrderId;
	}

	public void setProviderOrderId(String providerOrderId) {
		this.providerOrderId = providerOrderId;
	}

	public String getTransCode() {
		return transCode;
	}

	public void setTransCode(String transCode) {
		this.transCode = transCode;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public String getAuthenCode() {
		return authenCode;
	}

	public void setAuthenCode(String authenCode) {
		this.authenCode = authenCode;
	}

	public Integer getPaidStatus() {
		return paidStatus;
	}

	public void setPaidStatus(Integer paidStatus) {
		this.paidStatus = paidStatus;
	}

	public Integer getIsShip() {
		return isShip;
	}

	public void setIsShip(Integer isShip) {
		this.isShip = isShip;
	}

}
