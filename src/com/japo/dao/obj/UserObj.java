package com.japo.dao.obj;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.japo.Util.Constant;
import com.japo.Util.Util;

/**
 * 
 * @author PhucNT
 *
 */

@Entity
@Table(name = "tbUser")
public class UserObj extends AbstractObj {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	int userId;
	@Column(name = "email")
	String email;
	@Column(name = "name")
	String name;
	@Column(name = "password")
	String password;
	@Column(name = "type")
	Integer type;
	@Column(name = "status")
	Integer status;

	public UserObj() {
		// TODO Auto-generated constructor stub
		this.status = 0;
		this.password = Util.hash256(Constant.defaultPassword);
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}
