package com.japo.dao.obj;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author PhucNT
 *
 */

@Entity
@Table(name = "tbStatusHistory")
public class StatusHistoryObj extends AbstractObj {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	int id;
	@Column(name = "transport_order_id")
	String transOrderId;
	@Column(name = "status_id")
	String statusId;
	@Column(name = "time")
	Timestamp time;
	@Column(name = "user_id")
	int userId;
	@Column(name = "detail")
	String detail;

	public StatusHistoryObj() {
		// TODO Auto-generated constructor stub

	}

	public StatusHistoryObj(String orderId, String statusId, int userId, String detail) {
		this.transOrderId = orderId;
		this.statusId = statusId;
		this.userId = userId;
		this.detail = detail;
		this.time = new Timestamp(new Date().getTime());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTransOrderId() {
		return transOrderId;
	}

	public void setTransOrderId(String transOrderId) {
		this.transOrderId = transOrderId;
	}

	public String getStatusId() {
		return statusId;
	}

	public void setStatusId(String sstatusId) {
		this.statusId = sstatusId;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}
}
