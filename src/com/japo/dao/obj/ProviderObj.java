package com.japo.dao.obj;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author PhucNT
 *
 */

@Entity
@Table(name = "tbProvider")
public class ProviderObj extends AbstractObj {
	@Id
	@Column(name = "provider_id")
	String providerId;
	@Column(name = "provider_name")
	String providerName;
	@Column(name = "detail")
	String detail;

	public ProviderObj() {
		// TODO Auto-generated constructor stub
	}

	public ProviderObj(String id, String name) {
		// TODO Auto-generated constructor stub
		this.providerId = id;
		this.providerName = name;
	}

	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

}
