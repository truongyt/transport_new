package com.japo.dao.obj;

public class OrderResultObj {
	private String orderId;
	private String provider;
	private String detail;
	private String receipter;
	private String time1;
	private String time2;
	private String time3;
	private String time4;
	private String time5;
	private String user1;
	private String user2;
	private String user3;
	private String user4;
	private String user5;

	public OrderResultObj() {
		// TODO Auto-generated constructor stub
		orderId = null;
		provider = null;
		detail = null;
		receipter = null;
		time1 = null;
		time2 = null;
		time3 = null;
		time4 = null;
		time5 = null;
		user1 = null;
		user2 = null;
		user3 = null;
		user4 = null;
		user5 = null;
	}

	public String toString() {
		return "" + orderId + " " + detail + " " + time1 + " " + user1 + " " + time2 + " " + user2;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getReceipter() {
		return receipter;
	}

	public void setReceipter(String receipter) {
		this.receipter = receipter;
	}

	public String getTime1() {
		return time1;
	}

	public void setTime1(String time1) {
		this.time1 = time1;
	}

	public String getTime2() {
		return time2;
	}

	public void setTime2(String time2) {
		this.time2 = time2;
	}

	public String getTime3() {
		return time3;
	}

	public void setTime3(String time3) {
		this.time3 = time3;
	}

	public String getTime4() {
		return time4;
	}

	public void setTime4(String time4) {
		this.time4 = time4;
	}

	public String getTime5() {
		return time5;
	}

	public void setTime5(String time5) {
		this.time5 = time5;
	}

	public String getUser1() {
		return user1;
	}

	public void setUser1(String user1) {
		this.user1 = user1;
	}

	public String getUser2() {
		return user2;
	}

	public void setUser2(String user2) {
		this.user2 = user2;
	}

	public String getUser3() {
		return user3;
	}

	public void setUser3(String user3) {
		this.user3 = user3;
	}

	public String getUser4() {
		return user4;
	}

	public void setUser4(String user4) {
		this.user4 = user4;
	}

	public String getUser5() {
		return user5;
	}

	public void setUser5(String user5) {
		this.user5 = user5;
	}

}
