package com.japo.dao.obj;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author PhucNT
 *
 */

@Entity
@Table(name = "tbLocationHistory")
public class LocationHistoryObj extends AbstractObj {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	int id;
	@Column(name = "transport_order_id")
	String transOrderId;
	@Column(name = "location_id")
	String locationId;
	@Column(name = "time")
	Timestamp time;
	@Column(name = "user_id")
	int userId;
	@Column(name = "detail")
	String detail;

	public LocationHistoryObj() {
		// TODO Auto-generated constructor stub
		this.time = new Timestamp(new Date().getTime());
	}

	public LocationHistoryObj(String orderId, String locationId, int userId, String detail) {
		this.transOrderId = orderId;
		this.locationId = locationId;
		this.userId = userId;
		this.detail = detail;
		this.time = new Timestamp(new Date().getTime());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTransOrderId() {
		return transOrderId;
	}

	public void setTransOrderId(String transOrderId) {
		this.transOrderId = transOrderId;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}
}
