package com.japo.dao.obj;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.japo.dao.obj.AbstractObj;

/**
 * 
 * @author PhucNT
 *
 */

@Entity
@Table(name = "tbProductType")
public class ProductTypeObj extends AbstractObj {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "type_id")
	int typeId;
	@Column(name = "type_name")
	String typeName;
	@Column(name = "unit")
	String unit;
	@Column(name = "price")
	Integer price;
	@Column(name = "detail")
	String detail;
	@Column(name = "updatetime")
	Timestamp updatetime;

	public ProductTypeObj() {
		// TODO Auto-generated constructor stub
	}

	public ProductTypeObj(String name, String unit, int price) {
		// TODO Auto-generated constructor stub
		this.typeName = name;
		this.unit = unit;
		this.price = price;
	}

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Timestamp getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(Timestamp updatetime) {
		this.updatetime = updatetime;
	}
}
