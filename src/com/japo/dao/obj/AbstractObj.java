/**
 * 
 */
package com.japo.dao.obj;

import java.lang.reflect.Field;

/**
 * @author truongnguyen
 *
 */
public abstract class AbstractObj {

	public void updateFromOther(AbstractObj otherObj) throws Exception {
		if (!(otherObj.getClass() == this.getClass()))
			return;
		Field[] fields = this.getClass().getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			if (!(fields[i].getAnnotations()[0] instanceof javax.persistence.Id) && fields[i].get(otherObj) != null) {
				fields[i].set(this, fields[i].get(otherObj));
			}
		}
	}
}
