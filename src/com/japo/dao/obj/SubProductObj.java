package com.japo.dao.obj;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.japo.Util.Constant;
import com.japo.Util.Util;

/**
 * 
 * @author PhucNT
 *
 */
@Entity
@Table(name = "tbSubProduct")
public class SubProductObj extends AbstractObj implements Serializable{
	
	@Id
	@Column(name = "sub_product_id")
	Integer subProductId;
	@Id
	@Column(name = "transport_order_id")
	String transportOrderId;
	@Column(name = "location_id")
	String locationId;

	public SubProductObj() {
		// TODO Auto-generated constructor stub
		this.locationId = Constant.inJapanLocation;
	}

	


	public String getTransportOrderId() {
		return transportOrderId;
	}

	public void setTransportOrderId(String transportOrderId) {
		this.transportOrderId = transportOrderId;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	/*class SubProductId implements Serializable {
	    protected Integer SubProductId;
	    protected Integer subId;

	    public SubProductId() {}

	    public SubProductId(Integer SubProductId, Integer subId) {
	        this.SubProductId = SubProductId;
	        this. subId =  subId;
	    }*/




	public void setSubProductId(int subId) {
		// TODO Auto-generated method stub
		subProductId=subId;
		
	}
	public String getSubProductId() {
		// TODO Auto-generated method stub
		return transportOrderId+subProductId;
	}

}
