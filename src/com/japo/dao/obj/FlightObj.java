package com.japo.dao.obj;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


	@Entity
	@Table(name = "tbFlight")
	public class FlightObj extends AbstractObj {
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "fligh_id")
		private int flighId;
		@Column(name = "diemxuatphat")
		private String diemxuatphat;
		@Column(name = "diemden")
		private String diemden;
		@Column(name = "thoigiandi")
		private Timestamp thoigiandi;
		@Column(name = "thoigianden")
		private String thoigianden;
		@Column(name = "ghichu")
		private String ghichu;
		@Column(name = "tenhanhkhach")
		private String tenhanhkhach;
		@Column(name = "thongtinhanhkhach")
		private String thongtinhanhkhach;
		
		public FlightObj() {
			// TODO Auto-generated constructor stub
		}

		public int getFlighId() {
			return flighId;
		}

		public void setFlighId(int flighId) {
			this.flighId = flighId;
		}

		public String getDiemxuatphat() {
			return diemxuatphat;
		}

		public void setDiemxuatphat(String diemxuatphat) {
			this.diemxuatphat = diemxuatphat;
		}

		public String getDiemden() {
			return diemden;
		}

		public void setDiemden(String diemden) {
			this.diemden = diemden;
		}

		public Timestamp getThoigiandi() {
			return thoigiandi;
		}

		public void setThoigiandi(Timestamp thoigiandi) {
			this.thoigiandi = thoigiandi;
		}

		public String getThoigianden() {
			return thoigianden;
		}

		public void setThoigianden(String thoigianden) {
			this.thoigianden = thoigianden;
		}

		public String getGhichu() {
			return ghichu;
		}

		public void setGhichu(String ghichu) {
			this.ghichu = ghichu;
		}

		public String getTenhanhkhach() {
			return tenhanhkhach;
		}

		public void setTenhanhkhach(String tenhanhkhach) {
			this.tenhanhkhach = tenhanhkhach;
		}

		public String getThongtinhanhkhach() {
			return thongtinhanhkhach;
		}

		public void setThongtinhanhkhach(String thongtinhanhkhach) {
			this.thongtinhanhkhach = thongtinhanhkhach;
		}

		
	}

