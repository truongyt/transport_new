package com.japo.dao.obj;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.japo.Util.Util;

/**
 * 
 * @author PhucNT
 *
 */

@Entity
@Table(name = "tbProduct")
public class ProductObj extends AbstractObj {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	int id;
	@Column(name = "product_id")
	String productId;
	@Column(name = "transport_order_id")
	String transportOrderId;
	@Column(name = "name")
	String name;
	@Column(name = "quantity")
	float quantity;
	@Column(name = "type_id")
	Integer typeId;
	@Column(name = "attachment")
	String attach;
	@Column(name = "other")
	String other;

	public ProductObj() {
		// TODO Auto-generated constructor stub
		this.productId = Util.generateId();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getTransportOrderId() {
		return transportOrderId;
	}

	public void setTransportOrderId(String transportOrderId) {
		this.transportOrderId = transportOrderId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getQuantity() {
		return quantity;
	}

	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer type) {
		this.typeId = type;
	}

	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}
}
