/**
 * 
 */
package com.japo.dao;

import java.io.File;
import java.util.List;

import javax.persistence.Table;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.hibernate.service.ServiceRegistry;

import com.japo.dao.obj.*;




/**
 * @author PhucNT
 *
 */
public class CommonDao {

	private static SessionFactory sessionFactory;

	public static void setFactory(String configPath) {
		try {
			if (sessionFactory == null) {
				// loads configuration and mappings

				//File f = new File(configPath);
				File f = new File("/Users/truongnguyen/workspace/japo/japo_transport_final/japo_transport_server/WebContent/WEB-INF/config/hibernate.cfg.xml");
				//File f = new File("/Users/Apple/workspace/transport-server/resources/hibernate.cfg.test.xml");
				Configuration configuration = new Configuration().configure(f);
				ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
						.applySettings(configuration.getProperties()).build();
				sessionFactory = configuration.addPackage("com.japo.dao.obj").addAnnotatedClass(UserObj.class)
						.addAnnotatedClass(ProviderObj.class).addAnnotatedClass(CustomerObj.class)
						.addAnnotatedClass(TransportOrderObj.class).addAnnotatedClass(StatusObj.class)
						.addAnnotatedClass(StatusHistoryObj.class).addAnnotatedClass(LocationObj.class)
						.addAnnotatedClass(LocationHistoryObj.class).addAnnotatedClass(AttachmentObj.class)
						.addAnnotatedClass(CustomerTypeObj.class).addAnnotatedClass(ProductObj.class)
						.addAnnotatedClass(ProductTypeObj.class).addAnnotatedClass(ShipmentObj.class)
						.addAnnotatedClass(SubProductObj.class).addAnnotatedClass(CustomerHistoryObj.class)
						.addAnnotatedClass(FlightObj.class).buildSessionFactory(serviceRegistry);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Session getSession() {
		return sessionFactory.openSession();
	}

	protected AbstractObj getByField(String fieldName, Object value) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Criteria cr = session.createCriteria(this.getClass());
			SimpleExpression cr1 = Restrictions.eq(fieldName, value);
			cr.add(cr1);
			cr.setProjection(Projections.rowCount());
			cr.setMaxResults(1);
			AbstractObj so = (AbstractObj) cr.uniqueResult();
			tx.commit();
			return so;
		} catch (Exception e) {
			e.printStackTrace();
			// Log.err("", "getList", "[fieldName]="+fieldName+"
			// [value]="+value);
			return null;
		} finally {
			session.close();
		}
	}

	public String add(AbstractObj ob) {
		Session session = sessionFactory.openSession();

		// System.out.println("[ADD] session connected:"+session.isConnected());

		Transaction tx = null;
		// Integer objectId = -1;
		try {
			tx = session.beginTransaction();
			String id = session.save(ob).toString();
			tx.commit();
			return id;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			// Log.err("", "add", e.getMessage());
			return null;

		} finally {
			session.close();
		}
	}

	// return object after added
	public AbstractObj add2(AbstractObj ob) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Object id = session.save(ob);
			tx.commit();
			// Generate set Id function
			String s1 = ob.getClass().getDeclaredFields()[0].getName();
			String s2 = "set" + s1.substring(0, 1).toUpperCase() + s1.substring(1);
			try {
				// invoke function
				ob.getClass().getMethod(s2, new Class[] { id.getClass() }).invoke(ob, id);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return ob;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			// Log.err("", "add", e.getMessage());
			return null;
		} finally {
			session.close();
		}
	}

	protected boolean update(AbstractObj ob) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(ob);
			tx.commit();
			return true;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			// Log.err("", "update", e.getMessage());
			// throw e;
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}

	}

	public boolean addOrUpdate(AbstractObj ob) {
		Session session = sessionFactory.openSession();

		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.saveOrUpdate(ob);
			tx.commit();
			return true;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			// Log.err("", "addOrUpdate", e.getMessage());
			e.printStackTrace();
			return false;
			// throw e;
		} finally {
			session.close();
		}

	}

	public List getList(Class<?> T) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Criteria cr = session.createCriteria(T);
			List results = cr.list();
			tx.commit();
			return results;
		} catch (Exception e) {
			e.printStackTrace();
			// Log.err("", "getList", "object="+T.getName());
			return null;
		} finally {
			session.close();
		}

	}

	public boolean delete(AbstractObj ob) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.delete(ob);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			// Log.err("", "delete", e.getMessage());
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		return true;
	}

	protected List query(Class<?> T, String sqlCondition) throws Exception {

		String tbName = T.getAnnotation(Table.class).name();
		String hql = "FROM " + tbName + " " + sqlCondition;
		Session session = sessionFactory.openSession();
		// Transaction tx = null;
		// tx=session.beginTransaction();
		session.beginTransaction();
		Query query = session.createQuery(hql);
		List results = query.list();

		return results;
	}

	public void close() {
		// TODO Auto-generated method stub
		if (sessionFactory != null && !sessionFactory.isClosed())
			sessionFactory.close();
		System.out.println("sessionFactory stoped!");
	}

	/**
	 * @param t
	 * @param fieldName
	 * @param value
	 * @return
	 */
	public List<AbstractObj> getListByField(Class<? extends AbstractObj> T, String fieldName, Object value) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Criteria cr = session.createCriteria(T);
			SimpleExpression cr0 = Restrictions.eq(fieldName, value);
			cr.add(cr0);
			List results = cr.list();
			tx.commit();
			return results;
		} catch (Exception e) {
			e.printStackTrace();
			// Log.err("", "getList", "object="+T.getName());
			return null;
		} finally {
			session.close();
		}
	}

}
