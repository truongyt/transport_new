package com.japo.service.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonObject;
import com.japo.Util.Constant;
import com.japo.dao.obj.ProductObj;
import com.japo.dao.obj.ProductTypeObj;
import com.japo.service.LocationService;
import com.japo.service.ProductService;
import com.japo.service.ProductTypeService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/getProducts")
public class GetProductsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetProductsServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String orderId = request.getParameter("orderId");

		List<JsonObject> result = new ArrayList<>();
		List<ProductObj> products = new ProductService().getListByOrderId(orderId);

		if (!products.isEmpty()) {
			for (int i = 0; i < products.size(); i++) {
				ProductObj product = products.get(i);
				ProductTypeObj productTypeObj = new ProductTypeService().getObjectById(product.getTypeId());

				JsonObject o = new JsonObject();

				o.addProperty("id", product.getProductId());
				o.addProperty("name", product.getName());
				o.addProperty("quantity", product.getQuantity());
				o.addProperty("unit", productTypeObj.getUnit());
				float price = product.getQuantity() * productTypeObj.getPrice();
				o.addProperty("price", price);

				result.add(o);
			}
		}

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(result.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
