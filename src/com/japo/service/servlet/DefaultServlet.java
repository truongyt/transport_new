package com.japo.service.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.japo.Util.Cache;
import com.japo.Util.Constant;
import com.japo.dao.CommonDao;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/manage")
public class DefaultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DefaultServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init() {
		String hbn = this.getServletContext().getRealPath(Constant.hibernateConfig);
		CommonDao.setFactory(hbn);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession(true);

		if (session.getAttribute("type") != null) {
			String type = (String) session.getAttribute("type");

			switch (type) {
			case "superadmin":
				response.sendRedirect(request.getContextPath() + "/adminMainPage");
				break;
			case "admin":
				response.sendRedirect(request.getContextPath() + "/adminMainPage");
				break;
			case "user":
				response.sendRedirect(request.getContextPath() + "/userMainPage");
				break;
			}
		} else {
			RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/Jsp/login.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
