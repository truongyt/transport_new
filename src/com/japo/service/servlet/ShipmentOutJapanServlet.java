package com.japo.service.servlet;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.eclipse.jdt.internal.compiler.lookup.Scope.Substitutor;

import com.japo.Util.Constant;
import com.japo.Util.Util;
import com.japo.dao.obj.SubProductObj;
import com.japo.dao.obj.TransportOrderObj;
import com.japo.service.SubProductService;
import com.japo.service.TransportOrderService;
import com.japo.service.UserService;

/**
 * @author phucnt
 */
@WebServlet(urlPatterns = { "/shipmentOutJapan" })
public class ShipmentOutJapanServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ShipmentOutJapanServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		TransportOrderService orderService = new TransportOrderService();
		SubProductService subProductService = new SubProductService();

		String shipmentId = request.getParameter("id");
		List<TransportOrderObj> orders = orderService.getListBy(null, null, null, shipmentId);
		for (TransportOrderObj o : orders) {
			List<SubProductObj> subProductObjs = subProductService.getListByOrderId(o.getTransportOrderId());
			for (SubProductObj s : subProductObjs) {
				s.setLocationId(Constant.outJapanLocation);
				boolean update = subProductService.update(s);
				System.out.println("update sub-product: " + update);
			}
			o.setLocationId(Constant.outJapanLocation);
			boolean update = orderService.update(o);
			System.out.println("update order: " + update);
		}

		response.sendRedirect(request.getContextPath() + "/userMainPage");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
