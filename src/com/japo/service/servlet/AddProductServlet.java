package com.japo.service.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.japo.Util.Cache;
import com.japo.Util.Constant;
import com.japo.dao.obj.ProductObj;
import com.japo.dao.obj.ProductTypeObj;
import com.japo.dao.obj.SubProductObj;
import com.japo.service.ProductService;
import com.japo.service.ProductTypeService;
import com.japo.service.SubProductService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/addProducts")
public class AddProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddProductServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		ProductService productService = new ProductService();

		// Add new product to database

		String orderId = request.getParameter("orderId");
		int type = Integer.parseInt(request.getParameter("type"));
		float quantity = Float.parseFloat(request.getParameter("quantity"));
		String name = request.getParameter("name");

		ProductObj newProduct = new ProductObj();
		newProduct.setTypeId(type);
		newProduct.setQuantity(quantity);
		newProduct.setName(name);
		newProduct.setTransportOrderId(orderId);

		String add = productService.add(newProduct);
		System.out.println(add);

		// Get all product to display

		List<JsonObject> result = new ArrayList<>();
//		List<ProductObj> products = productService.getListByOrderId(orderId);
//
//		if (!products.isEmpty()) {
//			for (int i = 0; i < products.size(); i++) {
//				ProductObj product = products.get(i);
//				ProductTypeObj productTypeObj = new ProductTypeService().getObjectById(product.getTypeId());
//
//				JsonObject o = new JsonObject();
//
//				o.addProperty("id", product.getId());
//				o.addProperty("name", product.getName());
//				o.addProperty("quantity", product.getQuantity());
//				o.addProperty("unit", productTypeObj.getUnit());
//				float price = product.getQuantity() * productTypeObj.getPrice();
//				o.addProperty("price", price);
//
//				result.add(o);
//			}
//		}

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(result.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
