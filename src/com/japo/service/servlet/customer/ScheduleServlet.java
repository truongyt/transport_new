package com.japo.service.servlet.customer;


import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.japo.Util.Constant;
import com.japo.dao.CommonDao;
import com.japo.dao.obj.FlightObj;
import com.japo.dao.obj.UserObj;
import com.japo.service.FlightService;
import com.japo.service.UserService;

/**
 * Servlet implementation class SchedulePageServlet
 */
@WebServlet(urlPatterns = {"/flightschedule"})
public class ScheduleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ScheduleServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    @Override
	public void init() {
		String hbn = this.getServletContext().getRealPath(Constant.hibernateConfig);
		CommonDao.setFactory(hbn);
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		List <FlightObj> listFull = new FlightService().getList();
		System.out.println("List size:" + listFull.size());
		request.setAttribute("listUserFull", listFull);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/Jsp/customer/flightSchedule.jsp");
        dispatcher.forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
