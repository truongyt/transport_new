package com.japo.service.servlet.user.order;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.japo.Util.Constant;
import com.japo.dao.obj.ProductObj;
import com.japo.dao.obj.StatusHistoryObj;
import com.japo.dao.obj.TransportOrderObj;
import com.japo.service.ProductService;
import com.japo.service.StatusHistoryService;
import com.japo.service.TransportOrderService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/userDeleteOrder")
public class UserDeleteOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TransportOrderService transportOrderService;
	private StatusHistoryService statusHistoryService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserDeleteOrderServlet() {
		super();
		// TODO Auto-generated constructor stub
		transportOrderService = new TransportOrderService();
		statusHistoryService = new StatusHistoryService();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");
		String from = request.getParameter("from");

		HttpSession session = request.getSession(true);
		int userId = Integer.parseInt(session.getAttribute("userId").toString());

		TransportOrderObj obj = transportOrderService.getObjectById(id);
		obj.setStatusId(Constant.deleteStatus);

		StatusHistoryObj statusHistoryObj = new StatusHistoryObj(id, Constant.deleteStatus, userId, null);

		Boolean delete = transportOrderService.edit(obj);
		String history = statusHistoryService.add(statusHistoryObj);

		System.out.println("Order " + id + " deleted: " + delete);

		switch (from) {
		case "new":
			response.sendRedirect(request.getContextPath() + "/userMainPage");
			break;

		case "main":
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("");
			break;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
