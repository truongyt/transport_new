package com.japo.service.servlet.user.order;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.JsonObject;
import com.japo.Util.Constant;
import com.japo.dao.obj.LocationHistoryObj;
import com.japo.dao.obj.ProductObj;
import com.japo.dao.obj.TransportOrderObj;
import com.japo.dao.obj.UserObj;
import com.japo.service.CustomerService;
import com.japo.service.LocationHistoryService;
import com.japo.service.LocationService;
import com.japo.service.ProductService;
import com.japo.service.ProviderService;
import com.japo.service.ReportService;
import com.japo.service.StatusService;
import com.japo.service.TransportOrderService;
import com.japo.service.UserService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/userDonePreOrder")
public class UserDonePreOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserDonePreOrderServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");
		HttpSession session = request.getSession(true);
		String userId = session.getAttribute("userId").toString();
		UserObj user = new UserService().getObjectById(Integer.parseInt(userId));

		TransportOrderService service = new TransportOrderService();
		TransportOrderObj o = service.getObjectById(id);
		o.setLocationId(Constant.inJapanLocation);

		boolean orderUpdate = service.update(o);
		String history = new LocationHistoryService().setInJapan(o, user);
		
		String customerEmail = new CustomerService().getObjectById(o.getSenderId()).getEmail();
		try {
			new ReportService().reportToMail(id, customerEmail);
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		response.sendRedirect(request.getContextPath() + "/userMainPage");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
