package com.japo.service.servlet.user.order;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.japo.dao.obj.LocationObj;
import com.japo.dao.obj.OrderResultObj;
import com.japo.dao.obj.ProductTypeObj;
import com.japo.dao.obj.ShipmentObj;
import com.japo.dao.obj.StatusObj;
import com.japo.dao.obj.TransportOrderObj;
import com.japo.dao.obj.UserObj;
import com.japo.service.LocationService;
import com.japo.service.ProductTypeService;
import com.japo.service.ResultService;
import com.japo.service.ShipmentService;
import com.japo.service.StatusService;
import com.japo.service.TransportOrderService;
import com.japo.service.UserService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = { "/userMainPage" })
public class UserMainPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserMainPageServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher dispatcher = null;

		List<TransportOrderObj> list = new TransportOrderService().getAllAvailable();
		request.setAttribute("orders", list);

		List<ShipmentObj> shipments = new ShipmentService().getList();
		request.setAttribute("listShipment", shipments);

		List<StatusObj> statuss = new StatusService().getList();
		request.setAttribute("listStatus", statuss);

		List<LocationObj> locations = new LocationService().getList();
		request.setAttribute("listLocation", locations);

		List<UserObj> listUser = new UserService().getList();
		request.setAttribute("listUser", listUser);

		List<ProductTypeObj> pTypes = new ProductTypeService().getList();
		request.setAttribute("listProductType", pTypes);

		dispatcher = request.getRequestDispatcher("/Jsp/user/userMain.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
