package com.japo.service.servlet.user.order;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.JsonObject;
import com.japo.Util.Constant;
import com.japo.dao.obj.LocationHistoryObj;
import com.japo.dao.obj.ProductObj;
import com.japo.dao.obj.TransportOrderObj;
import com.japo.dao.obj.UserObj;
import com.japo.service.CustomerService;
import com.japo.service.LocationHistoryService;
import com.japo.service.LocationService;
import com.japo.service.ProductService;
import com.japo.service.ProviderService;
import com.japo.service.StatusService;
import com.japo.service.TransportOrderService;
import com.japo.service.UserService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/userDoUpdateOrder")
public class UserDoUpdateOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserDoUpdateOrderServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		
		HttpSession session = request.getSession(true);
		String userId = session.getAttribute("userId").toString();
		String orderId = request.getParameter("orderId");
		String shipment = request.getParameter("shipment");
		String transCode = request.getParameter("transCode");
		String sender = request.getParameter("senderId");
		String receipter = request.getParameter("receipterId");
		String other = request.getParameter("other");

		TransportOrderService service = new TransportOrderService();
		TransportOrderObj obj = service.getObjectById(orderId);
		obj.setShipmentId(shipment);
		obj.setTransCode(transCode);
		obj.setSenderId(Integer.parseInt(sender));
		obj.setReceipterId(Integer.parseInt(receipter));
		obj.setOther(other);
		boolean update = service.update(obj);

		System.out.println(update);

		response.sendRedirect(request.getContextPath() + "/userMainPage");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	private String saveHistory(TransportOrderObj order, UserObj user) {
		LocationHistoryObj locationHistoryObj = new LocationHistoryObj();

		JsonObject detail = new JsonObject();

		locationHistoryObj.setLocationId(Constant.inJapanLocation);

		locationHistoryObj.setTransOrderId(order.getTransportOrderId());
		locationHistoryObj.setUserId(user.getUserId());

		detail.addProperty("orderId", order.getTransportOrderId());
		detail.addProperty("provider", new ProviderService().getObjectById(order.getProviderId()).getProviderName());
		detail.addProperty("status", new StatusService().getObjectById(order.getStatusId()).getStatusName());
		detail.addProperty("user", user.getEmail());
		detail.addProperty("sender", new CustomerService().getObjectById(order.getSenderId()).getName());
		detail.addProperty("receipter", new CustomerService().getObjectById(order.getReceipterId()).getName());
		detail.addProperty("other", order.getOther());
		detail.addProperty("location",
				new LocationService().getObjectById(locationHistoryObj.getLocationId()).getLocationName());

		locationHistoryObj.setDetail(detail.toString());

		String add = new LocationHistoryService().add(locationHistoryObj);
		return add;
	}
}
