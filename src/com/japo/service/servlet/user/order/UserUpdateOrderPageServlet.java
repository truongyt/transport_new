package com.japo.service.servlet.user.order;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.japo.dao.obj.CustomerObj;
import com.japo.dao.obj.CustomerTypeObj;
import com.japo.dao.obj.ShipmentObj;
import com.japo.dao.obj.TransportOrderObj;
import com.japo.service.CustomerService;
import com.japo.service.CustomerTypeService;
import com.japo.service.ShipmentService;
import com.japo.service.TransportOrderService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/userUpdateOrderPage")
public class UserUpdateOrderPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateOrderPageServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher dispatcher = null;
		String orderId = request.getParameter("id");
		
		TransportOrderService orderService = new TransportOrderService();
		TransportOrderObj orderObj = orderService.getObjectById(orderId);
		
		CustomerService customerService = new CustomerService();
		List<CustomerObj> customers = customerService.getList();
		CustomerObj sender = customerService.getObjectById(orderObj.getSenderId());
		CustomerObj receipter = customerService.getObjectById(orderObj.getReceipterId());
		
		ShipmentService shipmentService = new ShipmentService();
		List<ShipmentObj> shipments = shipmentService.getList();
		
		CustomerTypeService typeService = new CustomerTypeService();
		List<CustomerTypeObj> types = typeService.getList();

		request.setAttribute("order", orderObj);
		request.setAttribute("customers", customers);
		request.setAttribute("sender", sender);
		request.setAttribute("receipter", receipter);
		request.setAttribute("shipments", shipments);
		request.setAttribute("types", types);
		dispatcher = this.getServletContext().getRequestDispatcher("/Jsp/user/userUpdateOrder.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
