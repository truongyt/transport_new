package com.japo.service.servlet.user.order;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.japo.Util.Constant;
import com.japo.dao.obj.AttachmentObj;
import com.japo.dao.obj.CustomerObj;
import com.japo.dao.obj.LocationHistoryObj;
import com.japo.dao.obj.ProductObj;
import com.japo.dao.obj.TransportOrderObj;
import com.japo.dao.obj.UserObj;
import com.japo.service.AttachmentService;
import com.japo.service.CustomerService;
import com.japo.service.LocationHistoryService;
import com.japo.service.LocationService;
import com.japo.service.ProductService;
import com.japo.service.ProviderService;
import com.japo.service.ReportService;
import com.japo.service.StatusService;
import com.japo.service.TransportOrderService;
import com.japo.service.UserService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/userDoNewFullOrder")
public class UserDoNewFullOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ReportService reportService;
	private boolean isMultipart;
	private String filePath;
	private int maxFileSize = 50 * 1024 * 1024;
	private int maxMemSize = 4 * 1024 * 1024;
	private File file;
	private String uploadStat = "";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserDoNewFullOrderServlet() {
		super();
		// TODO Auto-generated constructor stub
		reportService = new ReportService();
	}

	public void init() {
		// Get the file location where it would be stored.
		filePath = getServletContext().getInitParameter("file-upload");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		HttpSession session = request.getSession(true);
		String userId = session.getAttribute("userId").toString();
		UserObj user = new UserService().getObjectById(Integer.parseInt(userId));
		String transCode = "";
		int senderId = 0;
		int receipterId = 0;
		String shipment = "";
		String orderId = "";
		String other = "";

		List<AttachmentObj> attachs = new ArrayList<>();
		AttachmentService attachmentService = new AttachmentService();

		/**
		 * ------------ new attach -----------------
		 */

		isMultipart = ServletFileUpload.isMultipartContent(request);

		if (!isMultipart) {
			java.io.PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Servlet upload</title>");
			out.println("</head>");
			out.println("<body>");
			out.println("<p>No file uploaded</p>");
			out.println("</body>");
			out.println("</html>");
			return;
		}
		DiskFileItemFactory factory = new DiskFileItemFactory();
		// maximum size that will be stored in memory
		factory.setSizeThreshold(maxMemSize);

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		// maximum file size to be uploaded.
		upload.setSizeMax(maxFileSize);

		try {
			// Parse the request to get file items.
			List fileItems = upload.parseRequest(request);

			// Process the uploaded file items
			Iterator i = fileItems.iterator();

			while (i.hasNext()) {
				FileItem fi = (FileItem) i.next();

				if (!fi.isFormField()) {
					// Get the uploaded file parameters
					String fieldName = fi.getFieldName();
					String fileName = fi.getName();
					String contentType = fi.getContentType();
					boolean isInMemory = fi.isInMemory();
					long sizeInBytes = fi.getSize();
					System.out.println("file name: " + fileName);
					// Write the file
					if (fileName.lastIndexOf("\\") >= 0) {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
					} else {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
					}
					if (file.exists()) {
						System.out.println("File Exist: " + file.exists());
					}

					fi.write(file);
					AttachmentObj o = new AttachmentObj(fileName);
					attachs.add(o);

				} else {
					if (fi.getFieldName().equals("sender")) {
						senderId = Integer.parseInt(fi.getString("UTF-8").trim());
					}
					if (fi.getFieldName().equals("receipter")) {
						receipterId = Integer.parseInt(fi.getString("UTF-8").trim());
					}
					if (fi.getFieldName().equals("transCode")) {
						transCode = fi.getString("UTF-8").trim();
					}
					if (fi.getFieldName().equals("shipment")) {
						shipment = fi.getString("UTF-8").trim();
					}
					if (fi.getFieldName().equals("orderId")) {
						orderId = fi.getString("UTF-8").trim();
					}
					if (fi.getFieldName().equals("other")) {
						other = fi.getString("UTF-8").trim();
					}
				}
			}
			uploadStat = "upload success";
			request.setAttribute("uploadStat", uploadStat);
		} catch (Exception ex) {
			System.out.println(ex);
			uploadStat = "upload failed";
			request.setAttribute("uploadStat", uploadStat);
		}

		/**
		 * ------------ new order -----------------
		 */
		TransportOrderService service = new TransportOrderService();
		TransportOrderObj newOrder = service.getObjectById(orderId);
		newOrder.setShipmentId(shipment);
		newOrder.setSenderId(senderId);
		newOrder.setReceipterId(receipterId);
		newOrder.setTransCode(transCode);
		newOrder.setOther(other);

		service.update(newOrder);

		System.out.println("new order id: " + orderId);

		/**
		 * ------------ save history -----------------
		 */
		new LocationHistoryService().setInJapan(newOrder, user);

		/**
		 * ------------ new attachment -----------------
		 */
		for (int i = 0; i < attachs.size(); i++) {
			attachs.get(i).setTransOrderId(orderId);
			attachmentService.add(attachs.get(i));
		}
		
		/**
		 * ------------ Send report to mail ------------
		 */
		String customerEmail = new CustomerService().getObjectById(senderId).getEmail();
		try {
			reportService.reportToMail(orderId, customerEmail);
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		response.sendRedirect(request.getContextPath() + "/userMainPage");
	}
}
