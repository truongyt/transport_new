package com.japo.service.servlet.user.order;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.japo.Util.Constant;
import com.japo.dao.obj.AttachmentObj;
import com.japo.dao.obj.ProductObj;
import com.japo.service.AttachmentService;
import com.japo.service.ProductService;
import com.japo.service.ReportService;
import com.japo.service.TransportOrderService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/userDoNewPreOrder")
public class UserDoNewPreOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ReportService reportService;
	private boolean isMultipart;
	private String filePath;
	private int maxFileSize = 50 * 1024 * 1024;
	private int maxMemSize = 4 * 1024 * 1024;
	private File file;
	private String uploadStat = "";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserDoNewPreOrderServlet() {
		super();
		// TODO Auto-generated constructor stub
		reportService = new ReportService();
	}

	public void init() {
		// Get the file location where it would be stored.
		filePath = getServletContext().getInitParameter("file-upload");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		HttpSession session = request.getSession(true);
		String transCode = "";
		int senderId = 0;
		int receipterId = 0;
		String shipment = "";
		String products = "";
		String other = "";

		List<AttachmentObj> attachs = new ArrayList<>();
		AttachmentService attachmentService = new AttachmentService();

		/**
		 * ------------ new attach -----------------
		 */

		isMultipart = ServletFileUpload.isMultipartContent(request);

		if (!isMultipart) {
			java.io.PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Servlet upload</title>");
			out.println("</head>");
			out.println("<body>");
			out.println("<p>No file uploaded</p>");
			out.println("</body>");
			out.println("</html>");
			return;
		}
		DiskFileItemFactory factory = new DiskFileItemFactory();
		// maximum size that will be stored in memory
		factory.setSizeThreshold(maxMemSize);

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		// maximum file size to be uploaded.
		upload.setSizeMax(maxFileSize);

		try {
			// Parse the request to get file items.
			List fileItems = upload.parseRequest(request);

			// Process the uploaded file items
			Iterator i = fileItems.iterator();

			while (i.hasNext()) {
				FileItem fi = (FileItem) i.next();
				System.out.println("fi.getFieldName() " + fi.getFieldName());

				if (!fi.isFormField()) {
					// Get the uploaded file parameters
					String fieldName = fi.getFieldName();
					String fileName = fi.getName();
					String contentType = fi.getContentType();
					boolean isInMemory = fi.isInMemory();
					long sizeInBytes = fi.getSize();
					System.out.println("file name: " + fileName);
					// Write the file
					if (fileName.lastIndexOf("\\") >= 0) {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
					} else {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
					}
					if (file.exists()) {
						System.out.println("File Exist: " + file.exists());
					}

					fi.write(file);
					AttachmentObj o = new AttachmentObj(fileName);
					attachs.add(o);

				}
				if (fi.isFormField()) {
					if (fi.getFieldName().equals("sender")) {
						senderId = Integer.parseInt(fi.getString("UTF-8").trim());
					}
					if (fi.getFieldName().equals("receipter")) {
						receipterId = Integer.parseInt(fi.getString("UTF-8").trim());
					}
					if (fi.getFieldName().equals("transCode")) {
						transCode = fi.getString("UTF-8").trim();
					}
					if (fi.getFieldName().equals("shipment")) {
						shipment = fi.getString("UTF-8").trim();
					}
					if (fi.getFieldName().equals("other")) {
						other = fi.getString("UTF-8").trim();
					}
				}
			}
		} catch (Exception ex) {
			System.out.println(ex);
		}

		/**
		 * ------------ new order -----------------
		 */
		TransportOrderService service = new TransportOrderService();
		String id = service.newOrder(2, shipment, senderId, receipterId, transCode,
				session.getAttribute("userEmail").toString(), other);
		System.out.println("new order id: " + id);

		/**
		 * ------------ new attachment -----------------
		 */
		for (int i = 0; i < attachs.size(); i++) {
			attachs.get(i).setTransOrderId(id);
			attachmentService.add(attachs.get(i));
		}

		response.sendRedirect(request.getContextPath() + "/userMainPage");
	}
}
