package com.japo.service.servlet.user.order;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.japo.Util.Cache;
import com.japo.dao.obj.CustomerObj;
import com.japo.dao.obj.CustomerTypeObj;
import com.japo.dao.obj.ProductTypeObj;
import com.japo.dao.obj.ShipmentObj;
import com.japo.service.CustomerService;
import com.japo.service.CustomerTypeService;
import com.japo.service.ProductTypeService;
import com.japo.service.ShipmentService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = { "/userNewPreOrder" })
public class UserNewPreOrderPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserNewPreOrderPageServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		List<ProductTypeObj> products = new ProductTypeService().getList();
		request.setAttribute("products", products);

		List<CustomerObj> customers = new CustomerService().getList();
		request.setAttribute("customers", customers);

		List<CustomerTypeObj> types = new CustomerTypeService().getList();
		request.setAttribute("types", types);

		List<ShipmentObj> shipments = new ShipmentService().getList();
		request.setAttribute("shipments", shipments);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/Jsp/user/userNewPreOrder.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
