package com.japo.service.servlet.user.shipment;

import java.io.IOException;
import java.sql.Date;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.japo.dao.obj.CustomerObj;
import com.japo.dao.obj.LocationObj;
import com.japo.dao.obj.ShipmentObj;
import com.japo.service.ShipmentService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/userEditShipment")
public class UserEditShipmentServler extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserEditShipmentServler() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("editId");
		String departString = request.getParameter("editDepart");
		String deliveryString = request.getParameter("editDelivery");
		Date depart = Date.valueOf(departString);
		Date delivery = Date.valueOf(deliveryString);

		ShipmentObj ob = new ShipmentService().getObjectById(id);
		ob.setDepartTime(depart);
		ob.setDeliveryTime(delivery);

		boolean edit = new ShipmentService().edit(ob);
		System.out.println(edit);

		response.sendRedirect(request.getContextPath() + "/userShipmentPage");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
