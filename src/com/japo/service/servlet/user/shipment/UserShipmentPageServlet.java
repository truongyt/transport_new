package com.japo.service.servlet.user.shipment;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.japo.dao.obj.LocationObj;
import com.japo.dao.obj.ShipmentObj;
import com.japo.dao.obj.UserObj;
import com.japo.service.LocationService;
import com.japo.service.ShipmentService;
import com.japo.service.UserService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/userShipmentPage")
public class UserShipmentPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserShipmentPageServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		HttpSession session = request.getSession(true);
		int userId = Integer.parseInt(session.getAttribute("userId").toString());

		List<ShipmentObj> shipments = new ShipmentService().getListByUser(userId);
		request.setAttribute("shipments", shipments);
		
		List<LocationObj> locations = new LocationService().getList();
		request.setAttribute("locations", locations);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/Jsp/user/userShipment.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
