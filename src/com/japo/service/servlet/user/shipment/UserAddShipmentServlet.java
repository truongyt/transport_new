package com.japo.service.servlet.user.shipment;

import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.util.Random;

import javax.mail.Session;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.japo.dao.obj.ShipmentObj;
import com.japo.service.ShipmentService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/userAddShipment")
public class UserAddShipmentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserAddShipmentServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession(true);

		String departString = request.getParameter("newDepart");
		String deliveryString = request.getParameter("newDelivery");
		Date depart = Date.valueOf(departString);
		Date delivery = Date.valueOf(deliveryString);

		ShipmentObj ob = new ShipmentObj();
		ob.setDepartTime(depart);
		ob.setDeliveryTime(delivery);
		// ob.setShipmentId(Integer.toString(new Random().nextInt(20)));
		ob.setUserId(Integer.parseInt(session.getAttribute("userId").toString()));
		String add = new ShipmentService().add(ob);
		System.out.println(add);

		response.sendRedirect(request.getContextPath() + "/userShipmentPage");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
