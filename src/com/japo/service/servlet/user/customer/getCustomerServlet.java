package com.japo.service.servlet.user.customer;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.japo.dao.obj.CustomerObj;
import com.japo.service.CustomerService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/getCustomer")
public class getCustomerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CustomerService service;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public getCustomerServlet() {
		super();
		// TODO Auto-generated constructor stub
		service = new CustomerService();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");
		CustomerObj obj = service.getObjectById(Integer.parseInt(id));
		String result = null;
		Map<String, String> mapResult = new LinkedHashMap<String, String>();
		mapResult.put("id", Integer.toString(obj.getCustomerId()));
		mapResult.put("name", obj.getName());
		mapResult.put("phone", obj.getPhone());
		mapResult.put("email", obj.getEmail());
		mapResult.put("address", obj.getAdress());
		result = new Gson().toJson(mapResult);

		System.out.println("getCustomer" + result);

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
