package com.japo.service.servlet.user.customer;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.JsonObject;
import com.japo.dao.obj.CustomerHistoryObj;
import com.japo.dao.obj.CustomerObj;
import com.japo.dao.obj.UserObj;
import com.japo.service.CustomerService;
import com.japo.service.UserService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = { "/userAddCustomer" })
public class UserAddCustomerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserAddCustomerServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession(true);
		String userId = session.getAttribute("userId").toString();

		String email = request.getParameter("email");
		String name = request.getParameter("name");
		String typeString = request.getParameter("type");
		int type = Integer.parseInt(typeString);
		String phone = request.getParameter("phone");
		String address = request.getParameter("address");

		CustomerObj ob = new CustomerObj();
		ob.setEmail(email);
		ob.setName(name);
		ob.setType(type);
		ob.setPhone(phone);
		ob.setAdress(address);
		String id = new CustomerService().add(ob);
		System.out.println(Integer.parseInt(id));

		CustomerHistoryObj historyObj = new CustomerHistoryObj();
		historyObj.setCustomerId(Integer.parseInt(id));
		historyObj.setUserId(Integer.parseInt(userId));

		List<JsonObject> customers = new CustomerService().getJsonList();

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(customers.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
