package com.japo.service.servlet.user.product;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.japo.dao.obj.ProductObj;
import com.japo.dao.obj.ShipmentObj;
import com.japo.dao.obj.TransportOrderObj;
import com.japo.service.ProductService;
import com.japo.service.ProductTypeService;
import com.japo.service.ShipmentService;
import com.japo.service.SubProductService;
import com.japo.service.TransportOrderService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/userProductPage")
public class UserProductPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserProductPageServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(true);
		int userId = Integer.parseInt(session.getAttribute("userId").toString());

		ShipmentService shipmentService = new ShipmentService();
		TransportOrderService transportOrderService = new TransportOrderService();
		// ProductTypeService productTypeService = new ProductTypeService();
		// ProductService productService = new ProductService();
		// SubProductService subProductService = new SubProductService();

		List<ShipmentObj> shipmentObjs = shipmentService.getList();
		ShipmentObj newestShipment = shipmentObjs.get(0);
		List<TransportOrderObj> transportOrderObjs = transportOrderService.getListBy(null, null, null,
				newestShipment.getShipmentId());

		request.setAttribute("shipments", shipmentObjs);
		request.setAttribute("orders", transportOrderObjs);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/Jsp/user/userProduct.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
