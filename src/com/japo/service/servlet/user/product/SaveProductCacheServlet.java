package com.japo.service.servlet.user.product;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonObject;
import com.japo.dao.obj.ProductObj;
import com.japo.dao.obj.ProductTypeObj;
import com.japo.service.ProductTypeService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/saveProductCache")
public class SaveProductCacheServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SaveProductCacheServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		int type = Integer.parseInt(request.getParameter("type"));
		float quantity = Float.parseFloat(request.getParameter("quantity"));
		String name = request.getParameter("name");
		String attach = request.getParameter("attach");

		ProductTypeObj productTypeObj = new ProductTypeService().getObjectById(type);

		JsonObject result = new JsonObject();
		result.addProperty("name", name);
		result.addProperty("quantity", quantity);
		result.addProperty("unit", productTypeObj.getUnit());
		float price = quantity * productTypeObj.getPrice();
		result.addProperty("price", price);

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(result.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
