package com.japo.service.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonObject;
import com.japo.Util.Constant;
import com.japo.dao.obj.ProductObj;
import com.japo.dao.obj.ProductTypeObj;
import com.japo.dao.obj.SubProductObj;
import com.japo.service.LocationService;
import com.japo.service.ProductService;
import com.japo.service.ProductTypeService;
import com.japo.service.SubProductService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/getSubProducts")
public class GetSubProductsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetSubProductsServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String orderId = request.getParameter("id");
		List<JsonObject> result = new ArrayList<>();

		List<SubProductObj> objs = new SubProductService().getListByOrderId(orderId);
		for (int i = 0; i < objs.size(); i++) {
			JsonObject o = new JsonObject();
			SubProductObj obj = objs.get(i);

			o.addProperty("id", obj.getSubProductId());
			o.addProperty("fId", obj.getSubProductId());
			o.addProperty("location", new LocationService().getObjectById(obj.getLocationId()).getLocationName());

			result.add(o);
		}

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(result.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
