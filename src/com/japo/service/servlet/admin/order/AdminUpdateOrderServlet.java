package com.japo.service.servlet.admin.order;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.JsonObject;
import com.japo.dao.obj.LocationHistoryObj;
import com.japo.dao.obj.LocationObj;
import com.japo.dao.obj.ProviderObj;
import com.japo.dao.obj.StatusHistoryObj;
import com.japo.dao.obj.StatusObj;
import com.japo.dao.obj.TransportOrderObj;
import com.japo.service.CustomerService;
import com.japo.service.LocationHistoryService;
import com.japo.service.LocationService;
import com.japo.service.ProviderService;
import com.japo.service.StatusHistoryService;
import com.japo.service.StatusService;
import com.japo.service.TransportOrderService;
import com.japo.service.UserService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/adminUpdateOrder")
public class AdminUpdateOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TransportOrderService transportOrderService;
	private ProviderService providerService;
	private UserService userService;
	private StatusHistoryService statusHistoryService;
	private LocationHistoryService locationHistoryService;
	private StatusService statusService;
	private LocationService locationService;
	private CustomerService customerService;

	private JsonObject history;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminUpdateOrderServlet() {
		super();
		// TODO Auto-generated constructor stub
		transportOrderService = new TransportOrderService();
		providerService = new ProviderService();
		userService = new UserService();
		statusService = new StatusService();
		locationService = new LocationService();
		statusHistoryService = new StatusHistoryService();
		locationHistoryService = new LocationHistoryService();
		customerService = new CustomerService();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");

		HttpSession session = request.getSession(true);
		int userId = (Integer) session.getAttribute("userId");
		String orderId = request.getParameter("update-id");

		TransportOrderObj obj = transportOrderService.getObjectById(orderId);

		String status = request.getParameter("status");
		String location = request.getParameter("location");
		String provider = request.getParameter("provider");

		if (!obj.getStatusId().equals(status)) {
			history = new JsonObject();
			history.addProperty("orderId", orderId);
			history.addProperty("provider", providerService.getObjectById(obj.getProviderId()).getProviderName());
			history.addProperty("status", statusService.getObjectById(status).getStatusName());
			history.addProperty("location", locationService.getObjectById(location).getLocationName());
			history.addProperty("user", userService.getObjectById(userId).getEmail());
			history.addProperty("sender", customerService.getObjectById(obj.getSenderId()).getName());
			history.addProperty("receipter", customerService.getObjectById(obj.getReceipterId()).getName());
			history.addProperty("other", obj.getOther());

			StatusHistoryObj newStatusHistory = new StatusHistoryObj(orderId, status, userId, history.toString());
			statusHistoryService.add(newStatusHistory);

			obj.setStatusId(status);
		}

		if (!obj.getLocationId().equals(location)) {
			history = new JsonObject();
			history.addProperty("orderId", orderId);
			history.addProperty("provider", providerService.getObjectById(obj.getProviderId()).getProviderName());
			history.addProperty("status", statusService.getObjectById(status).getStatusName());
			history.addProperty("location", locationService.getObjectById(location).getLocationName());
			history.addProperty("user", userService.getObjectById(userId).getEmail());
			history.addProperty("sender", customerService.getObjectById(obj.getSenderId()).getName());
			history.addProperty("receipter", customerService.getObjectById(obj.getReceipterId()).getName());
			history.addProperty("other", obj.getOther());

			LocationHistoryObj newLocationHistory = new LocationHistoryObj(orderId, location, userId,
					history.toString());
			locationHistoryService.add(newLocationHistory);

			obj.setLocationId(location);
		}

		obj.setProviderId(provider);

		transportOrderService.edit(obj);

		response.sendRedirect(request.getContextPath() + "/adminMainPage");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
