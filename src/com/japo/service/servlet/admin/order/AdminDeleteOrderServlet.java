package com.japo.service.servlet.admin.order;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.japo.Util.Constant;
import com.japo.dao.obj.AttachmentObj;
import com.japo.dao.obj.LocationHistoryObj;
import com.japo.dao.obj.LocationObj;
import com.japo.dao.obj.ProductObj;
import com.japo.dao.obj.StatusHistoryObj;
import com.japo.dao.obj.StatusObj;
import com.japo.dao.obj.SubProductObj;
import com.japo.dao.obj.TransportOrderObj;
import com.japo.service.AttachmentService;
import com.japo.service.LocationHistoryService;
import com.japo.service.ProductService;
import com.japo.service.ProviderService;
import com.japo.service.StatusHistoryService;
import com.japo.service.SubProductService;
import com.japo.service.TransportOrderService;
import com.japo.service.UserService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/adminDeleteOrder")
public class AdminDeleteOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private TransportOrderService transportOrderService;
	private StatusHistoryService statusHistoryService;
	private LocationHistoryService locationHistoryService;
	private ProductService productService;
	private SubProductService subProductService;
	private AttachmentService attachmentService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminDeleteOrderServlet() {
		super();
		// TODO Auto-generated constructor stub
		transportOrderService = new TransportOrderService();
		locationHistoryService = new LocationHistoryService();
		statusHistoryService = new StatusHistoryService();
		productService = new ProductService();
		subProductService = new SubProductService();
		attachmentService = new AttachmentService();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");

		TransportOrderObj order = transportOrderService.getObjectById(id);

		List<LocationHistoryObj> locationHistoryObjs = locationHistoryService
				.getListByTransOrderId(order.getTransportOrderId());
		for (LocationHistoryObj locationHistoryObj : locationHistoryObjs) {
			locationHistoryService.delete(locationHistoryObj);
		}

		List<StatusHistoryObj> statusHistoryObjs = statusHistoryService
				.getListByTransOrderId(order.getTransportOrderId());
		for (StatusHistoryObj statusHistoryObj : statusHistoryObjs) {
			statusHistoryService.delete(statusHistoryObj);
		}

		List<ProductObj> productObjs = productService.getListByOrderId(order.getTransportOrderId());
		for (ProductObj productObj : productObjs) {
			productService.delete(productObj);
		}

		List<SubProductObj> subProductObjs = subProductService.getListByOrderId(order.getTransportOrderId());
		for (SubProductObj subProductObj : subProductObjs) {
			subProductService.delete(subProductObj);
		}

		List<AttachmentObj> attachmentObjs = attachmentService.getListByTransOrderId(order.getTransportOrderId());
		for (AttachmentObj attachmentObj : attachmentObjs) {
			attachmentService.delete(attachmentObj);
		}

		boolean delete = transportOrderService.delete(order);
		System.out.println("Order deleted: " + delete);

		response.sendRedirect(request.getContextPath() + "/adminMainPage");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
