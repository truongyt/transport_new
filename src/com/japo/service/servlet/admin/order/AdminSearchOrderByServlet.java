package com.japo.service.servlet.admin.order;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.japo.dao.obj.LocationObj;
import com.japo.dao.obj.ProviderObj;
import com.japo.dao.obj.ShipmentObj;
import com.japo.dao.obj.StatusObj;
import com.japo.dao.obj.TransportOrderObj;
import com.japo.dao.obj.UserObj;
import com.japo.service.LocationService;
import com.japo.service.ProviderService;
import com.japo.service.ShipmentService;
import com.japo.service.StatusService;
import com.japo.service.TransportOrderService;
import com.japo.service.UserService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/adminSearchOrderBy")
public class AdminSearchOrderByServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminSearchOrderByServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String location = request.getParameter("searchLocation");
		String status = request.getParameter("searchStatus");
		String shipment = request.getParameter("searchShipment");

		if (shipment == "") {
			shipment = null;
		}

		if (location == "") {
			location = null;
		}

		if (status == "") {
			status = null;
		}

		List<TransportOrderObj> list = new TransportOrderService().getListBy(null, status, location, shipment);
		request.setAttribute("orders", list);

		List<ShipmentObj> shipments = new ShipmentService().getList();
		request.setAttribute("listShipment", shipments);

		List<StatusObj> statuss = new StatusService().getList();
		request.setAttribute("listStatus", statuss);

		List<LocationObj> locations = new LocationService().getList();
		request.setAttribute("listLocation", locations);

		List<UserObj> listUser = new UserService().getList();
		request.setAttribute("listUser", listUser);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/Jsp/admin/adminMain.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
