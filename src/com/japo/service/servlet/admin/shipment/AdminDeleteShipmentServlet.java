package com.japo.service.servlet.admin.shipment;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.japo.dao.obj.ShipmentObj;
import com.japo.service.ShipmentService;


/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/adminDeleteShipment")
public class AdminDeleteShipmentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminDeleteShipmentServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		ShipmentService service = new ShipmentService();

		String id = request.getParameter("id");
		ShipmentObj obj = service.getObjectById(id);

		Boolean delete = service.delete(obj);
		System.out.println(delete);

		response.sendRedirect(request.getContextPath() + "/adminShipmentPage");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
