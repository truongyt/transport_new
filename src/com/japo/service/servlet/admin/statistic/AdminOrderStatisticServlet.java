package com.japo.service.servlet.admin.statistic;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonObject;
import com.japo.Util.Constant;
import com.japo.dao.obj.UserObj;
import com.japo.service.StatisticService;
import com.japo.service.StatusHistoryService;
import com.japo.service.UserService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = { "/adminOrderStatisticPage" })
public class AdminOrderStatisticServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminOrderStatisticServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		// int month = Integer.parseInt((new SimpleDateFormat("MM")).format(new
		// Date()));
		// int year = Integer.parseInt((new SimpleDateFormat("yyyy")).format(new
		// Date()));
		//
		// request.setAttribute("month", month);
		// request.setAttribute("year", month);

		request.setAttribute("months", Constant.months);
		request.setAttribute("years", Constant.years);

		RequestDispatcher dispatcher = this.getServletContext()
				.getRequestDispatcher("/Jsp/admin/adminOrderStatistic.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
