package com.japo.service.servlet.admin.customer;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.japo.dao.obj.CustomerObj;
import com.japo.service.CustomerService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/adminEditCustomer")
public class AdminEditCustomerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminEditCustomerServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		
		String idString = request.getParameter("editCustomerId");
		int id = Integer.parseInt(idString);
		String email = request.getParameter("editCustomerEmail");
		String name = request.getParameter("editCustomerName");
		String typeString = request.getParameter("editCustomerType");
		int type = Integer.parseInt(typeString);
		String phone = request.getParameter("editCustomerPhone");
		String address = request.getParameter("editCustomerAddress");

		CustomerService service = new CustomerService();

		CustomerObj ob = service.getObjectById(id);
		ob.setEmail(email);
		ob.setName(name);
		ob.setType(type);
		ob.setPhone(phone);
		ob.setAdress(address);
		boolean edit = service.edit(ob);
		System.out.println(edit);

		response.sendRedirect(request.getContextPath() + "/adminCustomerPage");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
