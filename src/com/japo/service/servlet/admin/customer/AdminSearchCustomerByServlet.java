package com.japo.service.servlet.admin.customer;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.japo.dao.obj.CustomerObj;
import com.japo.dao.obj.CustomerTypeObj;
import com.japo.dao.obj.UserObj;
import com.japo.service.CustomerService;
import com.japo.service.CustomerTypeService;
import com.japo.service.UserService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = { "/adminSearchCustomerBy" })
public class AdminSearchCustomerByServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminSearchCustomerByServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		
		String typeString = request.getParameter("customerType");

		Integer type = null;
		if (typeString != null) {
			type = Integer.parseInt(typeString);
		}

		List<CustomerObj> list = new CustomerService().getListBy(type);
		request.setAttribute("customers", list);

		List<CustomerTypeObj> types = new CustomerTypeService().getList();
		request.setAttribute("types", types);

		RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/Jsp/admin/adminCustomer.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
