package com.japo.service.servlet.admin.customer;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.japo.dao.obj.CustomerObj;
import com.japo.dao.obj.UserObj;
import com.japo.service.CustomerService;
import com.japo.service.UserService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = { "/adminAddCustomer" })
public class AdminAddCustomerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminAddCustomerServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String email = request.getParameter("newCustomerEmail");
		String name = request.getParameter("newCustomerName");
		String typeString = request.getParameter("newCustomerType");
		int type = Integer.parseInt(typeString);
		String phone = request.getParameter("newCustomerPhone");
		String address = request.getParameter("newCustomerAddress");

		CustomerObj ob = new CustomerObj();
		ob.setEmail(email);
		ob.setName(name);
		ob.setType(type);
		ob.setPhone(phone);
		ob.setAdress(address);
		String add = new CustomerService().add(ob);
		System.out.println(add);

		response.sendRedirect(request.getContextPath() + "/adminCustomerPage");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
