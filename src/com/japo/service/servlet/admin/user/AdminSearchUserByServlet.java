package com.japo.service.servlet.admin.user;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.japo.dao.obj.UserObj;
import com.japo.service.UserService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = { "/adminSearchUserBy" })
public class AdminSearchUserByServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminSearchUserByServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String statusString = request.getParameter("userStatus");
		String typeString = request.getParameter("userType");

		Integer status = null;
		if (statusString != null) {
			status = Integer.parseInt(statusString);
		}
		Integer type = null;
		if (typeString != null) {
			type = Integer.parseInt(typeString);
		}

		List<UserObj> list = new UserService().getListBy(null, type, status);
		request.setAttribute("listUser", list);
		List<UserObj> listFull = new UserService().getList();
		request.setAttribute("listUserFull", listFull);

		RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/Jsp/admin/adminUser.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
