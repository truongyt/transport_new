package com.japo.service.servlet.admin.user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.japo.dao.obj.UserObj;
import com.japo.service.UserService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/adminEditUser")
public class AdminEditUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminEditUserServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		
		String userIdString = request.getParameter("editUserId");
		String userEmail = request.getParameter("editUserEmail");
		String userName = request.getParameter("editUserName");
		String typeString = request.getParameter("editUserType");
		String statusString = request.getParameter("editUserStatus");
		int userId = Integer.parseInt(userIdString);
		int type = Integer.parseInt(typeString);
		int status = Integer.parseInt(statusString);

		UserService service = new UserService();

		UserObj user = service.getObjectById(userId);
		user.setEmail(userEmail);
		user.setName(userName);
		user.setType(type);
		user.setStatus(status);
		boolean edit = new UserService().edit(user);
		System.out.println(edit);

		response.sendRedirect(request.getContextPath() + "/adminUserPage");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
