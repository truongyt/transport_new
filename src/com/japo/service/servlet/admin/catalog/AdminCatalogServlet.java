package com.japo.service.servlet.admin.catalog;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.japo.dao.obj.LocationObj;
import com.japo.dao.obj.ProductTypeObj;
import com.japo.dao.obj.ProviderObj;
import com.japo.dao.obj.StatusObj;
import com.japo.service.LocationService;
import com.japo.service.ProductTypeService;
import com.japo.service.ProviderService;
import com.japo.service.StatusService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/adminCatalogPage")
public class AdminCatalogServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminCatalogServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		List<ProductTypeObj> products = new ProductTypeService().getList();
		request.setAttribute("products", products);
		List<ProviderObj> providers = new ProviderService().getList();
		request.setAttribute("providers", providers);
		List<StatusObj> statuss = new StatusService().getList();
		request.setAttribute("statuss", statuss);
		List<LocationObj> locations = new LocationService().getList();
		request.setAttribute("locations", locations);

		RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/Jsp/admin/adminCatalog.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
