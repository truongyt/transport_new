package com.japo.service.servlet.admin.catalog;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.japo.dao.obj.ProductTypeObj;
import com.japo.service.ProductTypeService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/adminEditProductType")
public class AdminEditProductTypeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminEditProductTypeServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		
		int id = Integer.parseInt(request.getParameter("editProductId"));
		String name = request.getParameter("editProductName");
		int price = Integer.parseInt(request.getParameter("editProductPrice"));
		String unit = request.getParameter("editUnit");

		ProductTypeService service = new ProductTypeService();

		ProductTypeObj ob = service.getObjectById(id);
		ob.setTypeName(name);
		ob.setPrice(price);
		ob.setUnit(unit);

		boolean edit = service.edit(ob);
		System.out.println(edit);

		response.sendRedirect(request.getContextPath() + "/adminCatalogPage");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
