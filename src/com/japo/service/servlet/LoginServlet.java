package com.japo.service.servlet;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.interfaces.RSAPrivateKey;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.japo.Util.Constant;
import com.japo.Util.RSAUtils;
import com.japo.Util.Util;
import com.japo.dao.CommonDao;
import com.japo.dao.obj.UserObj;
import com.japo.service.UserService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = { "/doLogin" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {

		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init() {
		String hbn = this.getServletContext().getRealPath(Constant.hibernateConfig);
		CommonDao.setFactory(hbn);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		String email = request.getParameter("userName");
		String rsaPass = request.getParameter("password");
		String pass = "";

		// Decrypt Rsa Password
		String privKeyPath = this.getServletContext().getRealPath(Constant.privateKey);
		RSAPrivateKey privkey;
		try {
			privkey = (RSAPrivateKey) RSAUtils.getPrivateKey(privKeyPath);
			pass = RSAUtils.decrypt(rsaPass, privkey);
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// pass = rsaPass;

		// Encrypt password to hash256
		String password = Util.hash256(pass);
		UserObj user = new UserService().login(email, password);
		if ((user != null) && (user.getStatus() == 0)) {
			// set userName attribute to current session
			HttpSession session = request.getSession(true);
			RequestDispatcher dispatcher = null;
			session.setAttribute("userId", user.getUserId());
			session.setAttribute("userEmail", email);
			session.setAttribute("userName", user.getName());
			int type = user.getType();

			if ((Util.hash256(Constant.defaultPassword)).equals(user.getPassword())) {
				dispatcher = this.getServletContext().getRequestDispatcher("/Jsp/changePassword.jsp");
				dispatcher.forward(request, response);
			} else {
				switch (type) {
				case 0:
				case 1:
					session.setAttribute("type", "admin");
					response.sendRedirect(request.getContextPath() + "/adminMainPage");
					break;
				case 2:
					session.setAttribute("type", "user");
					response.sendRedirect(request.getContextPath() + "/userMainPage");
				}
			}

		} else {
			response.getWriter().print("User or pass incorrect!");
			response.flushBuffer();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
