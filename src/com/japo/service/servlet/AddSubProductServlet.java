package com.japo.service.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.japo.Util.Cache;
import com.japo.Util.Constant;
import com.japo.dao.obj.SubProductObj;
import com.japo.service.SubProductService;

/**
 * @author PhucNT
 */
@WebServlet(urlPatterns = "/addSubProduct")
public class AddSubProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddSubProductServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		SubProductService service = new SubProductService();

		String parentOrderId = request.getParameter("orderId");
		String sNumber = request.getParameter("number");

		int number = Integer.parseInt(sNumber);

		for (int i = 0; i < number; i++) {
			SubProductObj o = new SubProductObj();
			o.setSubProductId( number);
			o.setTransportOrderId(parentOrderId);
			service.add(o);
		}

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write("");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
