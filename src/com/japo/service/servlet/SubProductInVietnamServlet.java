package com.japo.service.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.japo.Util.Constant;
import com.japo.dao.obj.SubProductObj;
import com.japo.dao.obj.TransportOrderObj;
import com.japo.dao.obj.UserObj;
import com.japo.service.SubProductService;
import com.japo.service.TransportOrderService;
import com.japo.service.UserService;
import com.japo.service.api.AndroidApi;

/**
 * @author phucnt
 */
@WebServlet(urlPatterns = { "/subProductInVietnam" })
public class SubProductInVietnamServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SubProductInVietnamServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		SubProductService subProductService = new SubProductService();
		TransportOrderService transportOrderService = new TransportOrderService();
		UserService userService = new UserService();
		
		HttpSession session = request.getSession(true);
		String userEmail = (String) session.getAttribute("userEmail");
		UserObj user = userService.getObjectByEmail(userEmail);
		
		String subProductId = request.getParameter("id");
		SubProductObj subProductObj = subProductService.getObjectById(subProductId);
		subProductObj.setLocationId(Constant.inVietnamLocation);
		boolean update = subProductService.update(subProductObj);
		System.out.println("Sub product " + subProductId + " in viet nam: " + update);
		
		TransportOrderObj parentOrder = transportOrderService.getObjectById(subProductObj.getTransportOrderId());
		String nowParentOrderLocation = parentOrder.getLocationId();
		AndroidApi androidApi = new AndroidApi();
		androidApi.checkAndUpdateParentOrder(parentOrder, nowParentOrderLocation, user);

		response.sendRedirect(request.getContextPath() + "/userMainPage");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
