package com.japo.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.japo.Util.Constant;

public class ReportService {

	private Properties mailServerProperties;
	private Session getMailSession;
	private MimeMessage generateMailMessage;

	public void reportToFile(String title, String content) {
		String path = "/Users/Apple/Documents/report/" + title + ".txt";
		File file = new File(path);

		try {
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void reportToMail(String orderId, String email) throws AddressException, MessagingException {
		String content = "Cám ơn bạn đã sử dụng dịch vự vận chuyển của chúng tôi" + "\n\nMã đơn hàng của bạn là: "
				+ orderId + "\n\nBạn có thể sử dụng mã đơn hàng để kiểm tra vị trí món hàng tại: "
				+ Constant.checkOrderLocationUrl + "\n\nCông ty cổ phần YTAsia";

		// Step1
		System.out.println("\n 1st ===> setup Mail Server Properties..");
		mailServerProperties = System.getProperties();
		mailServerProperties.put("mail.smtp.port", "587");
		mailServerProperties.put("mail.smtp.auth", "true");
		mailServerProperties.put("mail.smtp.starttls.enable", "true");
		System.out.println("Mail Server Properties have been setup successfully..");

		// Step2
		System.out.println("\n\n 2nd ===> get Mail Session..");
		getMailSession = Session.getDefaultInstance(mailServerProperties, null);
		generateMailMessage = new MimeMessage(getMailSession);
		generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
		// generateMailMessage.addRecipient(Message.RecipientType.CC, new
		// InternetAddress("phucnt@ytasia.co.jp"));
		generateMailMessage.setSubject("(YTAsia) Cám ơn đã sử dụng dịch vụ của YTAsia");
		generateMailMessage.setContent(content, "text/plain; charset=utf-8");
		System.out.println("Mail Session has been created successfully..");

		// Step3
		System.out.println("\n\n 3rd ===> Get Session and Send mail");
		Transport transport = getMailSession.getTransport("smtp");

		// Enter your correct gmail UserID and Password
		// if you have 2FA enabled then provide App Specific Password
		transport.connect("smtp.gmail.com", Constant.reportMailAddress, Constant.reportMailPassword);
		transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
		transport.close();
	}
}
