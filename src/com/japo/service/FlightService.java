package com.japo.service;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;

import com.japo.Util.Constant;
import com.japo.Util.Util;
import com.japo.dao.CommonDao;
import com.japo.dao.obj.FlightObj;
import com.japo.dao.obj.FlightObj;

public class FlightService extends CommonService {
	public List<FlightObj> getList() {
		List ls = super.getList(FlightObj.class);
		return (List<FlightObj>) ls;
	}

	public String add(FlightObj o) {
		return super.add(o);
	}

	public boolean edit(FlightObj o) {
		return super.update(o);
	}

	public boolean delete(FlightObj o) {
		return super.delete(o);
	}

	

	public FlightObj getObjectById(int id) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(FlightObj.class);
			cr.add(Restrictions.eq("flightId", id));
			FlightObj result = (FlightObj) cr.uniqueResult();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public List<FlightObj> getObjectByStartTime(Timestamp startTime) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(FlightObj.class);
			cr.add(Restrictions.eq("thoigiandi", startTime));
			List<FlightObj> result =  cr.list();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	/*public List<FlightObj> getListBy(Integer userId, Integer type, Integer status) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(FlightObj.class);
			if (userId != null) {
				cr.add(Restrictions.eq("userId", userId));
			}
			if (type != null) {
				cr.add(Restrictions.eq("type", type));
			}
			if (status != null) {
				cr.add(Restrictions.eq("status", status));
			}
			List result = cr.list();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}*/
}
