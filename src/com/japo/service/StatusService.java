package com.japo.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.google.gson.JsonObject;
import com.japo.dao.CommonDao;
import com.japo.dao.obj.ProductTypeObj;
import com.japo.dao.obj.StatusObj;

/**
 * 
 * @author PhucNT
 *
 */
public class StatusService extends CommonService {
	public List<StatusObj> getList() {
		List ls = super.getList(StatusObj.class);
		return (List<StatusObj>) ls;
	}

	public String add(StatusObj o) {
		return super.add(o);
	}

	public boolean edit(StatusObj o) {
		return super.update(o);
	}

	public boolean delete(StatusObj o) {
		return super.delete(o);
	}

	public StatusObj getObjectById(String id) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(StatusObj.class);
			cr.add(Restrictions.eq("statusId", id));
			StatusObj result = (StatusObj) cr.uniqueResult();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public JsonObject getJson(String id) {
		StatusObj obj = getObjectById(id);
		JsonObject json = new JsonObject();

		json.addProperty("id", obj.getStatusId());
		json.addProperty("name", obj.getStatusName());

		return json;
	}
}
