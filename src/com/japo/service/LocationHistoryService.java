package com.japo.service;

import java.sql.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.google.gson.JsonObject;
import com.japo.Util.Constant;
import com.japo.dao.CommonDao;
import com.japo.dao.obj.LocationHistoryObj;
import com.japo.dao.obj.TransportOrderObj;
import com.japo.dao.obj.UserObj;

/**
 * 
 * @author PhucNT
 *
 */
public class LocationHistoryService extends CommonService {
	public List<LocationHistoryObj> getList() {
		List ls = super.getList(LocationHistoryObj.class);
		return (List<LocationHistoryObj>) ls;
	}

	public String add(LocationHistoryObj o) {
		return super.add(o);
	}

	public boolean edit(LocationHistoryObj o) {
		return super.update(o);
	}

	public boolean delete(LocationHistoryObj o) {
		return super.delete(o);
	}

	public List<LocationHistoryObj> getListByTransOrderId(String id) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(LocationHistoryObj.class);
			cr.add(Restrictions.eq("transOrderId", id));
			cr.addOrder(Order.asc("time"));
			List result = cr.list();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public List<LocationHistoryObj> getListBetween(Date from, Date to) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(LocationHistoryObj.class);
			cr.add(Restrictions.eq("locationId", Constant.inJapanLocation));
			cr.add(Restrictions.between("time", from, to));
			cr.addOrder(Order.asc("time"));
			List result = cr.list();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public LocationHistoryObj getObjectById(String id) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(LocationHistoryObj.class);
			cr.add(Restrictions.eq("id", Integer.parseInt(id)));
			LocationHistoryObj result = (LocationHistoryObj) cr.uniqueResult();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public String setInJapan(TransportOrderObj order, UserObj user) {
		LocationHistoryObj locationHistoryObj = new LocationHistoryObj();

		JsonObject detail = new JsonObject();

		locationHistoryObj.setLocationId(Constant.inJapanLocation);

		locationHistoryObj.setTransOrderId(order.getTransportOrderId());
		locationHistoryObj.setUserId(user.getUserId());

		detail.addProperty("orderId", order.getTransportOrderId());
		detail.addProperty("provider", new ProviderService().getObjectById(order.getProviderId()).getProviderName());
		detail.addProperty("status", new StatusService().getObjectById(order.getStatusId()).getStatusName());
		detail.addProperty("user", user.getEmail());
		detail.addProperty("sender", new CustomerService().getObjectById(order.getSenderId()).getName());
		detail.addProperty("receipter", new CustomerService().getObjectById(order.getReceipterId()).getName());
		detail.addProperty("other", order.getOther());
		detail.addProperty("location",
				new LocationService().getObjectById(locationHistoryObj.getLocationId()).getLocationName());

		locationHistoryObj.setDetail(detail.toString());

		String add = new LocationHistoryService().add(locationHistoryObj);
		return add;
	}
}
