package com.japo.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.hibernate.criterion.Order;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.google.gson.JsonObject;
import com.japo.Util.Constant;
import com.japo.Util.Util;
import com.japo.dao.CommonDao;
import com.japo.dao.obj.LocationHistoryObj;
import com.japo.dao.obj.TransportOrderObj;
import com.japo.dao.obj.UserObj;

public class TransportOrderService extends CommonService {
	public List<TransportOrderObj> getAll() {
		List ls = super.getList(TransportOrderObj.class);
		return (List<TransportOrderObj>) ls;
	}

	public String add(TransportOrderObj o) {
		return super.add(o);
	}

	public boolean edit(TransportOrderObj o) {
		return super.update(o);
	}

	public boolean delete(TransportOrderObj o) {
		return super.delete(o);
	}

	public List<TransportOrderObj> getAllAvailable() {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(TransportOrderObj.class);
			cr.add(Restrictions.ne("statusId", Constant.deleteStatus));
			cr.addOrder(Order.desc("id"));
			List result = cr.list();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public TransportOrderObj getObjectById(String id) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(TransportOrderObj.class);
			cr.add(Restrictions.eq("transportOrderId", id));
			TransportOrderObj result = (TransportOrderObj) cr.uniqueResult();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public TransportOrderObj getObjectByTransCode(String id) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(TransportOrderObj.class);
			cr.add(Restrictions.eq("transCode", id));
			TransportOrderObj result = (TransportOrderObj) cr.uniqueResult();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public List<TransportOrderObj> getListBy(String orderId, String status, String location, String shipment) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(TransportOrderObj.class);
			if (orderId != null) {
				cr.add(Restrictions.eq("transportOrderId", orderId));
			}
			if (status != null) {
				cr.add(Restrictions.eq("statusId", status));
			}
			if (location != null) {
				cr.add(Restrictions.eq("locationId", location));
			}
			if (shipment != null) {
				cr.add(Restrictions.eq("shipmentId", shipment));
			}
			List<TransportOrderObj> result = cr.list();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	/**
	 * Initial order by web user
	 * 
	 */
	public String newOrder(int addType, String shipmentId, int senderId, int receipterId, String transCode,
			String userEmail, String other) {
		ProviderService providerService = new ProviderService();
		StatusService statusService = new StatusService();
		LocationService locationService = new LocationService();
		CustomerService customerService = new CustomerService();

		String result = null;

		TransportOrderObj transportOrderObj = new TransportOrderObj();
		String newOrderId = transportOrderObj.getTransportOrderId();
		transportOrderObj.setTransportOrderId(newOrderId);
		if (shipmentId != null)
			transportOrderObj.setShipmentId(shipmentId);
		transportOrderObj.setProviderId("YTA");
		transportOrderObj.setProviderOrderId(null);
		transportOrderObj.setAuthenCode(Integer.toString(new Random().nextInt(50)));
		transportOrderObj.setIsShip(0);
		if (receipterId > -1)
			transportOrderObj.setReceipterId(receipterId);
		if (senderId > -1)
			transportOrderObj.setSenderId(senderId);
		transportOrderObj.setStatusId("AVA");
		transportOrderObj.setPaidStatus(0);

		if (transCode != null)
			transportOrderObj.setTransCode(transCode);

		if (addType == 1) {
			transportOrderObj.setLocationId(Constant.inJapanLocation);
		} else if (addType == 2) {
			transportOrderObj.setLocationId(Constant.waitingLocation);
		}
		transportOrderObj.setOther(other);

		String r3 = add(transportOrderObj);
		System.out.println("return value of add order: " + r3);

		if (r3 == null)
			System.out.println("order false");

		if (r3 != null) {
			result = newOrderId;
		} else {
			result = null;
		}
		return result;
	}
}
