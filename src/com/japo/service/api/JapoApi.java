package com.japo.service.api;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.japo.Util.Constant;
import com.japo.dao.obj.CustomerObj;
import com.japo.dao.obj.LocationHistoryObj;
import com.japo.dao.obj.ProductObj;
import com.japo.dao.obj.ShipmentObj;
import com.japo.dao.obj.TransportOrderObj;
import com.japo.dao.obj.UserObj;
import com.japo.service.CustomerService;
import com.japo.service.LocationHistoryService;
import com.japo.service.LocationService;
import com.japo.service.ProductService;
import com.japo.service.ProviderService;
import com.japo.service.ShipmentService;
import com.japo.service.StatusHistoryService;
import com.japo.service.StatusService;
import com.japo.service.TransportOrderService;
import com.japo.service.UserService;

@Path("/JapoApi")
public class JapoApi {
	private TransportOrderService transportOrderService;
	private ProviderService providerService;
	private CustomerService customerService;
	private StatusService statusService;
	private LocationService locationService;
	private StatusHistoryService statusHistoryService;
	private LocationHistoryService locationHistoryService;
	private UserService userService;
	private ShipmentService shipmentService;
	private ProductService productService;

	public JapoApi() {
		// TODO Auto-generated constructor stub
		transportOrderService = new TransportOrderService();
		providerService = new ProviderService();
		customerService = new CustomerService();
		statusService = new StatusService();
		locationService = new LocationService();
		statusHistoryService = new StatusHistoryService();
		locationHistoryService = new LocationHistoryService();
		userService = new UserService();
		shipmentService = new ShipmentService();
		productService = new ProductService();
	}

	/**
	 * (API) Initial order by request from JAPO
	 * 
	 * @param receipterName
	 * @param receipterEmail
	 * @param receipterAddress
	 * @param receipterPhone
	 * @param orderId
	 * @param providerId
	 * @param providerOrderId
	 * @param detail
	 * @param authenCode
	 * @param isShip
	 * @return
	 */
	@POST
	@Path("/initOrder")
	@Produces(MediaType.TEXT_PLAIN)
	public String initOrder(@QueryParam("customerEmail") String receipterEmail,
			@QueryParam("customerPhone") String receipterPhone, @QueryParam("productData") String productData) {

		String result = null;

		CustomerObj receipter = customerService.getObjectByJapo(receipterEmail, receipterPhone);

		List<ShipmentObj> shipments = shipmentService.getList();
		System.out.println("shipments size:" + shipments.size());

		String orderId = transportOrderService.newOrder(2, shipments.get(0).getShipmentId(), 1,
				receipter.getCustomerId(), null, null, null);
		System.out.println("new order id: " + orderId);

		JSONParser jsonParser = new JSONParser();
		JSONArray prdData;
		try {
			prdData = (JSONArray) jsonParser.parse(productData);
			for (int i = 0; i < prdData.size(); i++) {
				JSONObject json = (JSONObject) prdData.get(i);
				ProductObj product = new ProductObj();
				product.setName(json.get("name").toString());
				product.setQuantity(Integer.parseInt(json.get("quantity").toString()));
				product.setOther(json.get("price").toString());
				product.setTypeId(99);
				product.setTransportOrderId(orderId);

				productService.add(product);
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		LocationHistoryObj locationHistoryObj = new LocationHistoryObj();
		locationHistoryObj.setLocationId(Constant.waitingLocation);
		locationHistoryObj.setTransOrderId(orderId);
		locationHistoryObj.setUserId(1);
		locationHistoryObj.setTime(new Timestamp(new Date().getTime()));

		String r3 = locationHistoryService.add(locationHistoryObj);

		if (orderId != null && r3 != null) {
			result = orderId;
		} else {
			result = "fail";
		}
		return result;
	}

	/**
	 * (API) Update order's transport code from JAPO
	 * 
	 * @param orderId
	 * @param transCode
	 * @return
	 */
	@POST
	@Path("/updateTransportCode")
	@Produces(MediaType.TEXT_PLAIN)
	public String updateTransportCode(@QueryParam("id") String orderId, @QueryParam("code") String transCode) {

		TransportOrderObj o = transportOrderService.getObjectById(orderId);
		o.setTransCode(transCode);
		boolean r = transportOrderService.update(o);
		if (r == true)
			return "success";
		else
			return "fail";
	}

	/**
	 * (API) Add new customer from JAPO
	 * 
	 * @param name
	 * @param email
	 * @return success/fail
	 */
	@POST
	@Path("/addCustomer")
	@Produces(MediaType.APPLICATION_JSON)
	public String addCustomer(@QueryParam("name") String name, @QueryParam("email") String email) {

		CustomerObj obj = new CustomerObj();
		obj.setName(name);
		obj.setEmail(email);
		obj.setType(1);

		String add = customerService.add(obj);

		if (add != null)
			return "success";
		else
			return "fail";
	}

	/**
	 * (API) Update customer from JAPO
	 * 
	 * @param phone
	 * @param address
	 * @return success/fail
	 */
	@POST
	@Path("/updateCustomer")
	@Produces(MediaType.APPLICATION_JSON)
	public String updateCustomer(@QueryParam("email") String email, @QueryParam("phone") String phone,
			@QueryParam("address") String address) {
		String r = "success";

		List<CustomerObj> customers = customerService.getAllObjectByEmail(email);
		for (CustomerObj customer : customers) {
			customer.setPhone(phone);
			customer.setAdress(address);
			boolean update = customerService.update(customer);
			if(!update){
				r = "fail";
			}
		}

		return r;
	}

	// @POST
	// @Path("/addCustomer")
	// @Produces(MediaType.APPLICATION_JSON)
	// public String addCustomer(@QueryParam("name") String name,
	// @QueryParam("email") String email,
	// @QueryParam("phone") String phone, @QueryParam("address") String address)
	// {
	//
	// CustomerObj obj = new CustomerObj();
	// obj.setName(name);
	// obj.setEmail(email);
	// obj.setPhone(phone);
	// obj.setAdress(address);
	// obj.setType(1);
	//
	// String add = customerService.add(obj);
	//
	// if (add != null)
	// return "success";
	// else
	// return "fail";
	// }

	/**
	 * (API) Get all shipment of YTAsia from JAPO
	 * 
	 * @param code
	 * @return
	 */
	@POST
	@Path("/getShipments")
	@Produces(MediaType.APPLICATION_JSON)
	public String getShipments() {

		List<JsonObject> result = new ArrayList<>();
		List<ShipmentObj> shipmentObjs = shipmentService.getList();

		for (int i = 0; i < shipmentObjs.size(); i++) {
			ShipmentObj o = shipmentObjs.get(i);

			JsonObject r = new JsonObject();

			r.addProperty("id", o.getShipmentId());
			r.addProperty("depart", o.getDepartTime().toString());
			r.addProperty("delivery", o.getDeliveryTime().toString());
			r.addProperty("detail", o.getDetail());

			result.add(r);
		}

		return result.toString();
	}
}
