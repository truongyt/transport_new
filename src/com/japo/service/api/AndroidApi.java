package com.japo.service.api;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.google.gson.JsonObject;
import com.japo.Util.Constant;
import com.japo.Util.Util;
import com.japo.dao.CommonDao;
import com.japo.dao.obj.CustomerObj;
import com.japo.dao.obj.LocationHistoryObj;
import com.japo.dao.obj.ProductObj;
import com.japo.dao.obj.ProductTypeObj;
import com.japo.dao.obj.ProviderObj;
import com.japo.dao.obj.ShipmentObj;
import com.japo.dao.obj.SubProductObj;
import com.japo.dao.obj.TransportOrderObj;
import com.japo.dao.obj.UserObj;
import com.japo.service.CustomerService;
import com.japo.service.LocationHistoryService;
import com.japo.service.LocationService;
import com.japo.service.ProductService;
import com.japo.service.ProductTypeService;
import com.japo.service.ProviderService;
import com.japo.service.ShipmentService;
import com.japo.service.StatusHistoryService;
import com.japo.service.StatusService;
import com.japo.service.SubProductService;
import com.japo.service.TransportOrderService;
import com.japo.service.UserService;

/**
 * 
 * @author PhucNT
 *
 */
@Path("/AndroidApi")
public class AndroidApi {

	private TransportOrderService transportOrderService;
	private ProviderService providerService;
	private CustomerService customerService;
	private StatusService statusService;
	private LocationService locationService;
	private LocationHistoryService locationHistoryService;
	private UserService userService;
	private SubProductService subProductService;
	private ProductService productService;
	private ShipmentService shipmentService;
	private UserObj userObj;

	public AndroidApi() {
		// TODO Auto-generated constructor stub
		String hbn = (Constant.hibernateConfig);
		CommonDao.setFactory(hbn);

		transportOrderService = new TransportOrderService();
		providerService = new ProviderService();
		customerService = new CustomerService();
		statusService = new StatusService();
		locationService = new LocationService();
		locationHistoryService = new LocationHistoryService();
		userService = new UserService();
		subProductService = new SubProductService();
		productService = new ProductService();
		shipmentService = new ShipmentService();

	}

	/**
	 * (API) Update sub-product's location from android user
	 * 
	 * @param id
	 * @return
	 */
	@POST
	@Path("/updateSubProductLocation")
	@Produces(MediaType.TEXT_PLAIN)
	public String updateSubProductLocation(@QueryParam("id") String id, @QueryParam("user") String userEmail) {

		SubProductObj subProductObj = subProductService.getObjectById(id);
		userObj = userService.getObjectByEmail(userEmail);

		/*
		 * Change location of sub-product
		 */
		String nowLocation = subProductObj.getLocationId();
		switch (nowLocation) {
		// case Constant.waitingLocation:
		// subProductObj.setLocationId(Constant.inJapanLocation);
		// break;
		// case Constant.inJapanLocation:
		// subProductObj.setLocationId(Constant.outJapanLocation);
		// break;
		case Constant.outJapanLocation:
			subProductObj.setLocationId(Constant.inVietnamLocation);
			break;
		// case Constant.inVietnamLocation:
		// subProductObj.setLocationId(Constant.outVietnamLocation);
		// break;
		}

		boolean r = subProductService.update(subProductObj);

		/*
		 * Check and change location of order (parent)
		 */
		TransportOrderObj parentOrder = transportOrderService.getObjectById(subProductObj.getTransportOrderId());
		String nowParentOrderLocation = parentOrder.getLocationId();
		switch (nowParentOrderLocation) {
		// case Constant.waitingLocation:
		// checkAndUpdateParentOrder(parentOrder, Constant.waitingLocation);
		// break;
		// case Constant.inJapanLocation:
		// checkAndUpdateParentOrder(parentOrder, Constant.inJapanLocation);
		// break;
		case Constant.outJapanLocation:
			checkAndUpdateParentOrder(parentOrder, Constant.outJapanLocation, userObj);
			break;
		// case Constant.inVietnamLocation:
		// checkAndUpdateParentOrder(parentOrder, Constant.inVietnamLocation);
		// break;
		}

		if (r == true)
			return "success";
		else
			return "false";
	}

	/**
	 * Check and Update location of TransportOrder parent
	 * 
	 * @param orderObj
	 * @param loc
	 * @return
	 */
	public String checkAndUpdateParentOrder(TransportOrderObj orderObj, String loc, UserObj user) {
		System.out.println("checkAndUpdateParentOrder: order changed");

		List<SubProductObj> subProductObjs = subProductService.getListByOrderId(orderObj.getTransportOrderId());
		List<String> subProductsLocation = new ArrayList<>();
		JsonObject detail = new JsonObject();
		LocationHistoryObj locationHistoryObj = new LocationHistoryObj();

		for (int i = 0; i < subProductObjs.size(); i++) {
			subProductsLocation.add(subProductObjs.get(i).getLocationId());
		}

		/*
		 * Check and change location of order (parent)
		 */
		switch (loc) {
		// case Constant.waitingLocation:
		// if (!(subProductsLocation.contains(Constant.waitingLocation))) {
		// orderObj.setLocationId(Constant.inJapanLocation);
		// locationHistoryObj.setLocationId(Constant.inJapanLocation);
		// }
		// break;
		// case Constant.inJapanLocation:
		// if (!(subProductsLocation.contains(Constant.inJapanLocation))) {
		// orderObj.setLocationId(Constant.outJapanLocation);
		// locationHistoryObj.setLocationId(Constant.outJapanLocation);
		// }
		// break;
		case Constant.outJapanLocation:
			if (!(subProductsLocation.contains(Constant.outJapanLocation))) {
				orderObj.setLocationId(Constant.inVietnamLocation);
				locationHistoryObj.setLocationId(Constant.inVietnamLocation);
			}
			break;
		// case Constant.inVietnamLocation:
		// if (!(subProductsLocation.contains(Constant.inVietnamLocation))) {
		// orderObj.setLocationId(Constant.outVietnamLocation);
		// locationHistoryObj.setLocationId(Constant.outVietnamLocation);
		// }
		// break;
		}

		boolean r = false;

		if (!loc.equals(orderObj.getLocationId())) {
			r = transportOrderService.update(orderObj);

			locationHistoryObj.setTransOrderId(orderObj.getTransportOrderId());
			locationHistoryObj.setTime(new Timestamp(new Date().getTime()));
			locationHistoryObj.setUserId(user.getUserId());

			detail.addProperty("orderId", orderObj.getTransportOrderId());
			detail.addProperty("provider", providerService.getObjectById(orderObj.getProviderId()).getProviderName());
			detail.addProperty("status", statusService.getObjectById(orderObj.getStatusId()).getStatusName());
			detail.addProperty("user", user.getEmail());
			detail.addProperty("sender", customerService.getObjectById(orderObj.getSenderId()).getName());
			detail.addProperty("receipter", customerService.getObjectById(orderObj.getReceipterId()).getName());
			detail.addProperty("other", orderObj.getOther());
			detail.addProperty("location", locationService.getObjectById(orderObj.getLocationId()).getLocationName());
			locationHistoryObj.setDetail(detail.toString());
			locationHistoryService.add(locationHistoryObj);
		}

		if (r == true)
			return "success";
		else
			return "false";
	}

	/**
	 * (API) Check order information by 'transport code' from android user
	 * 
	 * @param code
	 * @return
	 */
	@POST
	@Path("/checkOrderByTransCode")
	@Produces(MediaType.APPLICATION_JSON)
	public String checkOrderByTransCode(@QueryParam("id") String code) {
		String result = null;
		JsonObject jresult = new JsonObject();

		TransportOrderObj transOb = transportOrderService.getObjectByTransCode(code);
		if (transOb != null) {
			ProviderObj providerOb = providerService.getObjectById(transOb.getProviderId());

			CustomerObj receipterOb = customerService.getObjectById(transOb.getReceipterId());

			jresult.addProperty("order_id", transOb.getTransportOrderId());
			jresult.addProperty("provider_name", providerOb.getProviderName());
			jresult.addProperty("receipter_email", receipterOb.getEmail());

			String location = transOb.getLocationId();
			switch (location) {
			case "ODR":
				jresult.addProperty("location", "1");
				break;
			case "JPI":
				jresult.addProperty("location", "2");
				break;
			case "JPO":
				jresult.addProperty("location", "3");
				break;
			case "VNI":
				jresult.addProperty("location", "4");
				break;
			case "VNO":
				jresult.addProperty("location", "5");
				break;
			}

		} else {
			jresult.addProperty("order_id", "notavailable");
		}

		result = jresult.toString();
		return result;
	}

	/**
	 * (API) Get all shipment of YTAsia from android user
	 * 
	 * @param code
	 * @return
	 */
	@POST
	@Path("/getShipments")
	@Produces(MediaType.APPLICATION_JSON)
	public String getShipments() {

		List<JsonObject> result = new ArrayList<>();
		List<ShipmentObj> shipmentObjs = shipmentService.getList();

		for (int i = 0; i < shipmentObjs.size(); i++) {
			ShipmentObj o = shipmentObjs.get(i);
			List<TransportOrderObj> orderObjs = transportOrderService.getListBy(null, null, null, o.getShipmentId());
			UserObj u = userService.getObjectById(o.getUserId());

			JsonObject r = new JsonObject();

			r.addProperty("id", o.getShipmentId());
			r.addProperty("depart", o.getDepartTime().toString());
			r.addProperty("delivery", o.getDeliveryTime().toString());
			r.addProperty("user", u.getEmail());
			r.addProperty("detail", o.getDetail());
			r.addProperty("numOfOrders", orderObjs.size());

			result.add(r);
		}

		return result.toString();
	}

	/**
	 * (API) Get all order by shipment of YTAsia from android user
	 * 
	 * @param code
	 * @return
	 */
	@POST
	@Path("/getOrdersByShipmentId")
	@Produces(MediaType.APPLICATION_JSON)
	public String getOrdersByShipmentId(@QueryParam("id") String code) {

		List<JsonObject> result = new ArrayList<>();
		List<TransportOrderObj> orderObjs = transportOrderService.getListBy(null, null, null, code);

		for (int i = 0; i < orderObjs.size(); i++) {
			TransportOrderObj o = orderObjs.get(i);
			List<SubProductObj> subProductObjs = subProductService.getListByOrderId(o.getTransportOrderId());
			CustomerObj senderObj = customerService.getObjectById(o.getSenderId());
			CustomerObj receipterObj = customerService.getObjectById(o.getReceipterId());

			JsonObject r = new JsonObject();

			r.addProperty("id", o.getTransportOrderId());
			r.addProperty("location", o.getLocationId());
			r.addProperty("sender-name", senderObj.getName());
			r.addProperty("sender-phone", senderObj.getPhone());
			r.addProperty("sender-address", senderObj.getAdress());
			r.addProperty("receipter-name", receipterObj.getName());
			r.addProperty("receipter-phone", receipterObj.getPhone());
			r.addProperty("receipter-address", receipterObj.getAdress());
			r.addProperty("numOfSubProducts", subProductObjs.size());

			result.add(r);
		}

		return result.toString();
	}

	/**
	 * (API) Get all sub-products by order of YTAsia from android user
	 * 
	 * @param code
	 * @return
	 */
	@POST
	@Path("/getSubProductsByOrderId")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSubProductsByProductId(@QueryParam("id") String code) {

		List<JsonObject> result = new ArrayList<>();
		List<SubProductObj> subProductObjs = subProductService.getListByOrderId(code);

		for (int i = 0; i < subProductObjs.size(); i++) {
			SubProductObj o = subProductObjs.get(i);

			JsonObject r = new JsonObject();

			r.addProperty("id", o.getTransportOrderId());
			r.addProperty("location", o.getLocationId());

			result.add(r);
		}

		return result.toString();
	}

	/**
	 * (API) Check order information by 'Order Id' from android user
	 * 
	 * @param code
	 * @return
	 */
	@POST
	@Path("/getOrderByOrderId")
	@Produces(MediaType.APPLICATION_JSON)
	public String getOrderByOrderId(@QueryParam("id") String code) {

		String result = null;
		JsonObject jresult = new JsonObject();

		TransportOrderObj transOb = transportOrderService.getObjectById(code);
		if (transOb != null) {

		} else {
			jresult.addProperty("order_id", "notavailable");
		}

		result = jresult.toString();
		return result;
	}

	/**
	 * (API) Get sub-product information by 'sub-product id' from android user
	 * 
	 * @param code
	 * @return
	 */
	@POST
	@Path("/getSubProductInfo")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSubProductInfo(@QueryParam("id") String code) {

		JsonObject result = new JsonObject();
		SubProductObj subProductObj = subProductService.getObjectById(code);
		TransportOrderObj transportOrderObj = transportOrderService.getObjectById(subProductObj.getTransportOrderId());
		ShipmentObj shipmentObj = shipmentService.getObjectById(transportOrderObj.getShipmentId());
		CustomerObj senderObj = customerService.getObjectById(transportOrderObj.getSenderId());
		CustomerObj receipterObj = customerService.getObjectById(transportOrderObj.getReceipterId());

		if (subProductObj != null) {
			result.addProperty("s-id", code);
			result.addProperty("s-location", subProductObj.getLocationId());
			result.addProperty("o-id", transportOrderObj.getTransportOrderId());
			result.addProperty("o-depart", shipmentObj.getDepartTime().toString());
			result.addProperty("o-delivery", shipmentObj.getDeliveryTime().toString());
			result.addProperty("o-location", transportOrderObj.getLocationId());
			result.addProperty("o-sender-name", senderObj.getName());
			result.addProperty("o-sender-phone", senderObj.getPhone());
			result.addProperty("o-sender-address", senderObj.getAdress());
			result.addProperty("o-receipter-name", receipterObj.getName());
			result.addProperty("o-receipter-phone", receipterObj.getPhone());
			result.addProperty("o-receipter-address", receipterObj.getAdress());
		} else {
			result.addProperty("order_id", "notavailable");
		}

		return result.toString();
	}

	/**
	 * (API) Divide order from Android user
	 * 
	 * @param id
	 * @param number
	 * @return
	 */
	@POST
	@Path("/divideOrder")
	@Produces(MediaType.APPLICATION_JSON)
	public String divideOrder(@QueryParam("id") String id, @QueryParam("number") int number) {
		SubProductService service = new SubProductService();

		for (int i = 0; i < number; i++) {
			SubProductObj o = new SubProductObj();
			o.setSubProductId(number);
			o.setTransportOrderId(id);
			service.add(o);
		}

		return "success";
	}

	/**
	 * (API) Clear all divided order from Android user
	 * 
	 * @param id
	 * @param number
	 * @return
	 */
	@POST
	@Path("/clearSubProducts")
	@Produces(MediaType.APPLICATION_JSON)
	public String clearSubProducts(@QueryParam("id") String id) {
		SubProductService service = new SubProductService();
		List<SubProductObj> subProducts = service.getListByOrderId(id);

		for (int i = 0; i < subProducts.size(); i++) {
			service.delete(subProducts.get(i));
		}

		String shipmentId = new TransportOrderService().getObjectById(id).getShipmentId();

		return shipmentId;
	}

	/**
	 * (API) Get order info from android user
	 * 
	 * @param code
	 * @return
	 */
	@POST
	@Path("/getOrderInfo")
	@Produces(MediaType.APPLICATION_JSON)
	public String getOrderInfo(@QueryParam("by") int by, @QueryParam("id") String id) {
		JsonObject result = new JsonObject();
		TransportOrderObj orderObj = new TransportOrderObj();
		switch (by) {
		case 1:
			orderObj = transportOrderService.getObjectByTransCode(id);
			break;
		case 2:
			orderObj = transportOrderService.getObjectById(id);
			break;
		}
		System.out.println("order is null: ");

		CustomerObj sender = customerService.getObjectById(orderObj.getSenderId());
		CustomerObj receipter = customerService.getObjectById(orderObj.getReceipterId());

		if (orderObj != null) {
			result.addProperty("order_id", orderObj.getTransportOrderId());
			result.addProperty("order_location", orderObj.getLocationId());
			result.addProperty("trans_code", orderObj.getTransCode());
			result.addProperty("sender_name", sender.getName());
			result.addProperty("sender_email", sender.getEmail());
			result.addProperty("sender_phone", sender.getPhone());
			result.addProperty("receipter_name", receipter.getName());
			result.addProperty("receipter_email", receipter.getEmail());
			result.addProperty("receipter_phone", receipter.getPhone());
		} else {
			result.addProperty("order_id", "notavailable");
		}

		return result.toString();
	}

	/**
	 * (API) Authenticate from device
	 * 
	 * @param userEmail
	 * @param password
	 * @return
	 */
	@POST
	@Path("/authenticate")
	@Produces(MediaType.TEXT_PLAIN)
	public String loginFromDevice(@QueryParam("userEmail") String userEmail, @QueryParam("password") String password) {
		String result = null;
		JsonObject jresult = new JsonObject();

		UserObj userObj = new UserService().login(userEmail + "@ytasia.co.jp", Util.hash256(password));
		if ((userObj != null) && (userObj.getStatus() == 0)) {
			jresult.addProperty("userEmail", userObj.getEmail());
			jresult.addProperty("userName", userObj.getName());

		} else {
			jresult.addProperty("userEmail", "notavailable");
		}

		result = jresult.toString();

		return result;
	}

	/**
	 * (API) checkout vietnam order from android user
	 * 
	 * @param id
	 * @return
	 */
	@POST
	@Path("/checkOutVietnam")
	@Produces(MediaType.TEXT_PLAIN)
	public String checkOutVietnam(@QueryParam("id") String id, @QueryParam("user") String userEmail) {

		userObj = userService.getObjectByEmail(userEmail);
		TransportOrderObj order = transportOrderService.getObjectById(id);
		List<SubProductObj> subProducts = subProductService.getListByOrderId(order.getTransportOrderId());
		for (SubProductObj o : subProducts) {
			o.setLocationId(Constant.outVietnamLocation);
			subProductService.update(o);
		}
		order.setLocationId(Constant.outVietnamLocation);
		boolean r = transportOrderService.update(order);

		JsonObject detail = new JsonObject();
		LocationHistoryObj locationHistoryObj = new LocationHistoryObj();

		locationHistoryObj.setTransOrderId(order.getTransportOrderId());
		locationHistoryObj.setTime(new Timestamp(new Date().getTime()));
		locationHistoryObj.setUserId(userObj.getUserId());
		locationHistoryObj.setLocationId(Constant.outVietnamLocation);

		detail.addProperty("orderId", order.getTransportOrderId());
		detail.addProperty("provider", providerService.getObjectById(order.getProviderId()).getProviderName());
		detail.addProperty("status", statusService.getObjectById(order.getStatusId()).getStatusName());
		detail.addProperty("user", userObj.getEmail());
		detail.addProperty("sender", customerService.getObjectById(order.getSenderId()).getName());
		detail.addProperty("receipter", customerService.getObjectById(order.getReceipterId()).getName());
		detail.addProperty("other", order.getOther());
		detail.addProperty("location", locationService.getObjectById(order.getLocationId()).getLocationName());
		locationHistoryObj.setDetail(detail.toString());
		locationHistoryService.add(locationHistoryObj);

		if (r == true)
			return "success";
		else
			return "false";
	}
}
