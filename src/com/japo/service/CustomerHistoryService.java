package com.japo.service;

import java.sql.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.japo.dao.CommonDao;
import com.japo.dao.obj.CustomerHistoryObj;

/**
 * 
 * @author PhucNT
 *
 */
public class CustomerHistoryService extends CommonService {
	public List<CustomerHistoryObj> getList() {
		List ls = super.getList(CustomerHistoryObj.class);
		return (List<CustomerHistoryObj>) ls;
	}

	public String add(CustomerHistoryObj o) {
		return super.add(o);
	}

	public boolean edit(CustomerHistoryObj o) {
		return super.update(o);
	}

	public boolean delete(CustomerHistoryObj o) {
		return super.delete(o);
	}

	public List<CustomerHistoryObj> getListByUserId(int id) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(CustomerHistoryObj.class);
			cr.add(Restrictions.eq("userId", id));
			cr.addOrder(Order.asc("time"));
			List result = cr.list();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public List<CustomerHistoryObj> getListBetween(Date from, Date to) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(CustomerHistoryObj.class);
			cr.add(Restrictions.between("time", from, to));
			cr.addOrder(Order.asc("time"));
			List result = cr.list();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
}
