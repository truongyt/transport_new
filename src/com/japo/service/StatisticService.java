package com.japo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonObject;
import com.japo.Util.Util;
import com.japo.dao.obj.CustomerHistoryObj;
import com.japo.dao.obj.CustomerObj;
import com.japo.dao.obj.LocationHistoryObj;
import com.japo.dao.obj.UserObj;

public class StatisticService {
	private CustomerService customerService;
	private LocationHistoryService locationHistoryService;
	private UserService userService;
	private CustomerHistoryService customerHistoryService;

	public StatisticService() {
		// TODO Auto-generated constructor stub
		customerService = new CustomerService();
		locationHistoryService = new LocationHistoryService();
		userService = new UserService();
		customerHistoryService = new CustomerHistoryService();
	}

	/*
	 * Get data of Orders by month for statistic
	 */
	public String getJsonOrderDataByMonth(int year, int month) {
		List<LocationHistoryObj> ls = locationHistoryService.getListBetween(
				Util.stringToDate(year + "-" + month + "-01"), Util.stringToDate(year + "-" + (month + 1) + "-01"));

		String result = "";
		List<Integer> monthData = new ArrayList<>();
		List<JsonObject> chartData = new ArrayList<>();
		JsonObject detail = new JsonObject();

		for (int i = 0; i < ls.size(); i++) {
			LocationHistoryObj o = ls.get(i);
			int d = Integer.parseInt((new SimpleDateFormat("dd")).format(o.getTime()));
			monthData.add(d);
		}
		detail.addProperty("total", ls.size());
		detail.addProperty("average", (float) ls.size() / 20);

		Map<Integer, Integer> mapData = new HashMap<Integer, Integer>();

		for (int i : monthData) {
			if (mapData.containsKey(i))
				mapData.put(i, mapData.get(i) + 1);
			else
				mapData.put(i, 1);
		}
		
		for (int i = 1; i <= 31; i++) {
			if (!mapData.containsKey(i)) {
				mapData.put(i, 0);
			}
		}

		for (int i : mapData.keySet()) {
			JsonObject r = new JsonObject();
			r.addProperty("day", i);
			r.addProperty("number", mapData.get(i));

			chartData.add(r);
		}

		result = "[" + chartData.toString() + "," + detail.toString() + "]";

		return result;
	}

	/*
	 * Get data of Orders by year for statistic
	 */
	public String getJsonOrderDataByYear(int year) {
		List<LocationHistoryObj> ls = locationHistoryService.getListBetween(Util.stringToDate(year + "-01-01"),
				Util.stringToDate((year + 1) + "-01-01"));

		String result = "";
		List<Integer> yearData = new ArrayList<>();
		List<JsonObject> chartData = new ArrayList<>();
		JsonObject detail = new JsonObject();

		detail.addProperty("total", ls.size());
		detail.addProperty("average", (float) ls.size() / 12);
		detail.addProperty("year", year);

		for (int i = 0; i < ls.size(); i++) {
			LocationHistoryObj o = ls.get(i);
			int m = Integer.parseInt((new SimpleDateFormat("MM")).format(o.getTime()));
			yearData.add(m);
		}

		Map<Integer, Integer> mapData = new HashMap<Integer, Integer>();

		for (int i : yearData) {
			if (mapData.containsKey(i)) {
				mapData.put(i, mapData.get(i) + 1);
			} else {
				mapData.put(i, 1);
			}
		}

		for (int i = 1; i <= 12; i++) {
			if (!mapData.containsKey(i)) {
				mapData.put(i, 0);
			}
		}

		for (int i : mapData.keySet()) {

			JsonObject r = new JsonObject();

			String time = year + "-" + i;
			SimpleDateFormat f = new SimpleDateFormat("yyyy-MM");
			Date d;

			try {
				d = f.parse(time);
				long milliseconds = d.getTime();

				r.addProperty("month", milliseconds);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			r.addProperty("number", mapData.get(i));
			chartData.add(r);
		}

		result = "[" + chartData.toString() + "," + detail.toString() + "]";

		return result;
	}

	/*
	 * Get classify of Customer by month for statistic
	 */
	public String getJsonCustomerClassifyData() {

		String result = "";
		List<JsonObject> chartData = new ArrayList<>();
		JsonObject detail = new JsonObject();

		List<CustomerObj> ls = customerService.getList();
		List<Integer> types = new ArrayList<>();

		detail.addProperty("total", ls.size());

		for (int i = 0; i < ls.size(); i++) {
			types.add(ls.get(i).getType());
		}

		Map<Integer, Integer> mapData = new HashMap<Integer, Integer>();

		for (int i : types) {
			if (mapData.containsKey(i))
				mapData.put(i, mapData.get(i) + 1);
			else
				mapData.put(i, 1);
		}

		for (int i : mapData.keySet()) {
			JsonObject r = new JsonObject();
			r.addProperty("type", i);
			r.addProperty("number", mapData.get(i));

			chartData.add(r);
		}

		result = "[" + chartData.toString() + "," + detail.toString() + "]";

		return result;
	}

	/*
	 * Get data of Customer by year for statistic
	 */
	public String getJsonCustomerDataByYear(int year) {
		List<CustomerHistoryObj> ls = customerHistoryService.getListBetween(Util.stringToDate(year + "-01-01"),
				Util.stringToDate((year + 1) + "-01-01"));

		String result = "";

		List<Integer> yearData = new ArrayList<>();
		List<JsonObject> chartData = new ArrayList<>();

		JsonObject detail = new JsonObject();
		detail.addProperty("year", year);
		detail.addProperty("total", ls.size());
		detail.addProperty("average", (float) ls.size() / 12);

		for (int i = 0; i < ls.size(); i++) {
			CustomerHistoryObj o = ls.get(i);
			int m = Integer.parseInt((new SimpleDateFormat("MM")).format(o.getTime()));
			yearData.add(m);
		}

		Map<Integer, Integer> mapData = new HashMap<Integer, Integer>();

		for (int i : yearData) {
			if (mapData.containsKey(i)) {
				mapData.put(i, mapData.get(i) + 1);
			} else {
				mapData.put(i, 1);
			}
		}

		for (int i = 1; i <= 12; i++) {
			if (!mapData.containsKey(i)) {
				mapData.put(i, 0);
			}
		}

		for (int i : mapData.keySet()) {

			JsonObject r = new JsonObject();

			String time = year + "-" + i;
			SimpleDateFormat f = new SimpleDateFormat("yyyy-MM");
			Date d;

			try {
				d = f.parse(time);
				long milliseconds = d.getTime();
				r.addProperty("month", milliseconds);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			r.addProperty("number", mapData.get(i));
			chartData.add(r);
		}

		result = "[" + chartData.toString() + "," + detail.toString() + "]";

		return result;
	}

	/*
	 * Get classify of Customer by month for statistic
	 */
	public String getJsonUserClassifyData() {
		String result = "";
		List<JsonObject> chartData = new ArrayList<>();
		JsonObject detail = new JsonObject();

		List<UserObj> ls = userService.getList();
		List<Integer> types = new ArrayList<>();

		detail.addProperty("total", ls.size());

		for (int i = 0; i < ls.size(); i++) {
			types.add(ls.get(i).getType());
		}

		Map<Integer, Integer> mapData = new HashMap<Integer, Integer>();

		for (int i : types) {
			if (mapData.containsKey(i))
				mapData.put(i, mapData.get(i) + 1);
			else
				mapData.put(i, 1);
		}

		for (int i = 0; i <= 2; i++) {
			if (!mapData.containsKey(i)) {
				mapData.put(i, 0);
			}
		}

		for (int i : mapData.keySet()) {
			JsonObject r = new JsonObject();
			r.addProperty("type", i);
			r.addProperty("number", mapData.get(i));

			chartData.add(r);
		}

		result = "[" + chartData.toString() + "," + detail.toString() + "]";

		return result;
	}
}
