package com.japo.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.japo.dao.CommonDao;
import com.japo.dao.obj.ProductObj;

public class ProductService extends CommonService {
	public List<ProductObj> getList() {
		List ls = super.getList(ProductObj.class);
		return (List<ProductObj>) ls;
	}

	public String add(ProductObj o) {
		return super.add(o);
	}

	public boolean edit(ProductObj o) {
		return super.update(o);
	}

	public boolean delete(ProductObj o) {
		return super.delete(o);
	}

	public ProductObj getObjectById(String id) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(ProductObj.class);
			cr.add(Restrictions.eq("productId", id));
			ProductObj result = (ProductObj) cr.uniqueResult();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public List<ProductObj> getListByOrderId(String id) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(ProductObj.class);

			cr.add(Restrictions.eq("transportOrderId", id));

			List result = cr.list();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
}
