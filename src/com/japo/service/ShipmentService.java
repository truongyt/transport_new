package com.japo.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.google.gson.JsonObject;
import com.japo.dao.CommonDao;
import com.japo.dao.obj.ShipmentObj;

/**
 * 
 * @author PhucNT
 *
 */
public class ShipmentService extends CommonService {
	public List<ShipmentObj> getList() {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(ShipmentObj.class);
			cr.addOrder(Order.desc("departTime"));
			List<ShipmentObj> result = cr.list();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public String add(ShipmentObj o) {
		return super.add(o);
	}

	public boolean edit(ShipmentObj o) {
		return super.update(o);
	}

	public boolean delete(ShipmentObj o) {
		return super.delete(o);
	}

	public ShipmentObj getObjectById(String id) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(ShipmentObj.class);
			cr.add(Restrictions.eq("shipmentId", id));
			cr.addOrder(Order.asc("departTime"));
			ShipmentObj result = (ShipmentObj) cr.uniqueResult();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

//	public ShipmentObj getNewestObject() {
//		Session session = CommonDao.getSession();
//		Transaction trans = null;
//		try {
//			trans = session.beginTransaction();
//			Criteria cr = session.createCriteria(ShipmentObj.class);
//			cr.addOrder(Order.desc("departTime"));
//			List<ShipmentObj> result = cr.list();
//			trans.commit();
//			return result.get(0);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		} finally {
//			session.close();
//		}
//	}

	public JsonObject getJson(String id) {
		ShipmentObj obj = getObjectById(id);
		JsonObject json = new JsonObject();

		json.addProperty("depart", obj.getDepartTime().toString());
		json.addProperty("delivery", obj.getDeliveryTime().toString());
		json.addProperty("user", obj.getUserId());
		json.addProperty("id", obj.getShipmentId());

		return json;
	}

	public List<ShipmentObj> getListByUser(Integer userId) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(ShipmentObj.class);
			if (userId != null) {
				cr.add(Restrictions.eq("userId", userId));
			}
			List result = cr.list();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
}
