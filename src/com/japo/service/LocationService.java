package com.japo.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.google.gson.JsonObject;
import com.japo.dao.CommonDao;
import com.japo.dao.obj.LocationObj;
import com.japo.dao.obj.StatusObj;

/**
 * 
 * @author PhucNT
 *
 */

public class LocationService extends CommonService {
	public List<LocationObj> getList() {
		List ls = super.getList(LocationObj.class);
		return (List<LocationObj>) ls;
	}

	public String add(LocationObj o) {
		return super.add(o);
	}

	public boolean edit(LocationObj o) {
		return super.update(o);
	}

	public boolean delete(LocationObj o) {
		return super.delete(o);
	}

	public LocationObj getObjectById(String id) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(LocationObj.class);
			cr.add(Restrictions.eq("locationId", id));
			LocationObj result = (LocationObj) cr.uniqueResult();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public JsonObject getJson(String id) {
		LocationObj obj = getObjectById(id);
		JsonObject json = new JsonObject();

		json.addProperty("id", obj.getLocationId());
		json.addProperty("name", obj.getLocationName());

		return json;
	}
}
