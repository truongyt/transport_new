package com.japo.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.japo.dao.CommonDao;
import com.japo.dao.obj.StatusHistoryObj;

/**
 * 
 * @author PhucNT
 *
 */
public class StatusHistoryService extends CommonService {
	public List<StatusHistoryObj> getList() {
		List ls = super.getList(StatusHistoryObj.class);
		return (List<StatusHistoryObj>) ls;
	}

	public String add(StatusHistoryObj o) {
		return super.add(o);
	}

	public boolean edit(StatusHistoryObj o) {
		return super.update(o);
	}

	public boolean delete(StatusHistoryObj o) {
		return super.delete(o);
	}

	public List<StatusHistoryObj> getListByTransOrderId(String id) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(StatusHistoryObj.class);
			cr.add(Restrictions.eq("transOrderId", id));
			cr.addOrder(Order.asc("time"));
			List result = cr.list();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
	
	public StatusHistoryObj getObjectById(String id) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(StatusHistoryObj.class);
			cr.add(Restrictions.eq("id", Integer.parseInt(id)));
			StatusHistoryObj result = (StatusHistoryObj) cr.uniqueResult();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
}
