package com.japo.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.google.gson.JsonObject;
import com.japo.dao.CommonDao;
import com.japo.dao.obj.ProductTypeObj;

public class ProductTypeService extends CommonService {
	public List<ProductTypeObj> getList() {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(ProductTypeObj.class);
			cr.add(Restrictions.ne("typeId", 99));
			List<ProductTypeObj> result = cr.list();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public String add(ProductTypeObj o) {
		return super.add(o);
	}

	public boolean edit(ProductTypeObj o) {
		return super.update(o);
	}

	public boolean delete(ProductTypeObj o) {
		return super.delete(o);
	}

	public ProductTypeObj getObjectById(int id) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(ProductTypeObj.class);
			cr.add(Restrictions.eq("typeId", id));
			ProductTypeObj result = (ProductTypeObj) cr.uniqueResult();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public JsonObject getJson(int id) {
		ProductTypeObj obj = getObjectById(id);
		JsonObject json = new JsonObject();

		json.addProperty("id", id);
		json.addProperty("name", obj.getTypeName());
		json.addProperty("price", obj.getPrice());
		json.addProperty("unit", obj.getUnit());

		return json;
	}
}
