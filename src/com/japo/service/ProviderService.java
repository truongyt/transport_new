package com.japo.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.google.gson.JsonObject;
import com.japo.dao.CommonDao;
import com.japo.dao.obj.ProductTypeObj;
import com.japo.dao.obj.ProviderObj;

public class ProviderService extends CommonService {
	public List<ProviderObj> getList() {
		List ls = super.getList(ProviderObj.class);
		return (List<ProviderObj>) ls;
	}

	public String add(ProviderObj o) {
		return super.add(o);
	}

	public boolean edit(ProviderObj o) {
		return super.update(o);
	}

	public boolean delete(ProviderObj o) {
		return super.delete(o);
	}

	public ProviderObj getObjectById(String id) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(ProviderObj.class);
			cr.add(Restrictions.eq("providerId", id));
			ProviderObj result = (ProviderObj) cr.uniqueResult();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public ProviderObj getObjectByName(String name) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(ProviderObj.class);
			cr.add(Restrictions.eq("providerName", name));
			ProviderObj result = (ProviderObj) cr.uniqueResult();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public JsonObject getJson(String id) {
		ProviderObj obj = getObjectById(id);
		JsonObject json = new JsonObject();

		json.addProperty("id", obj.getProviderId());
		json.addProperty("name", obj.getProviderName());

		return json;
	}
}
