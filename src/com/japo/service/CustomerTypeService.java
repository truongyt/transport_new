package com.japo.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.japo.dao.CommonDao;
import com.japo.dao.obj.CustomerTypeObj;

public class CustomerTypeService extends CommonService {
	public List<CustomerTypeObj> getList() {
		List ls = super.getList(CustomerTypeObj.class);
		return (List<CustomerTypeObj>) ls;
	}

	public String add(CustomerTypeObj o) {
		return super.add(o);
	}

	public boolean edit(CustomerTypeObj o) {
		return super.update(o);
	}

	public boolean delete(CustomerTypeObj o) {
		return super.delete(o);
	}

	public CustomerTypeObj getObjectById(int id) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(CustomerTypeObj.class);
			cr.add(Restrictions.eq("id", id));
			CustomerTypeObj result = (CustomerTypeObj) cr.uniqueResult();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
}
