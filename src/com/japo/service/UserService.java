package com.japo.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;

import com.japo.Util.Constant;
import com.japo.Util.Util;
import com.japo.dao.CommonDao;
import com.japo.dao.obj.UserObj;

/**
 * 
 * @author PhucNT
 *
 */

public class UserService extends CommonService {
	public List<UserObj> getList() {
		List ls = super.getList(UserObj.class);
		return (List<UserObj>) ls;
	}

	public String add(UserObj o) {
		return super.add(o);
	}

	public boolean edit(UserObj o) {
		return super.update(o);
	}

	public boolean delete(UserObj o) {
		return super.delete(o);
	}

	public boolean resetPass(int id) {
		UserObj obj = getObjectById(id);
		obj.setPassword(Util.hash256(Constant.defaultPassword));
		return edit(obj);
	}

	public UserObj getObjectById(int id) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(UserObj.class);
			cr.add(Restrictions.eq("userId", id));
			UserObj result = (UserObj) cr.uniqueResult();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public UserObj getObjectByEmail(String mail) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(UserObj.class);
			cr.add(Restrictions.eq("email", mail));
			UserObj result = (UserObj) cr.uniqueResult();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public UserObj login(String email, String hashPwd) {
		Session session = CommonDao.getSession();
		Transaction tx = session.beginTransaction();
		try {
			session = CommonDao.getSession();
			Criteria criteria = session.createCriteria(UserObj.class);
			SimpleExpression cr0 = Restrictions.eq("password", hashPwd);
			SimpleExpression cr1 = Restrictions.eq("email", email);
			criteria.add(Restrictions.and(cr0, cr1));
			Object uo = criteria.uniqueResult();
			if (uo == null)
				return null;
			else {
				Criteria cr2 = session.createCriteria(UserObj.class);
				SimpleExpression cr3 = Restrictions.eq("email", email);

				cr2.add(cr3);
				UserObj user = (UserObj) cr2.uniqueResult();
				tx.commit();
				return user;
			}
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
			return null;
		} finally {
			if (session != null)
				session.close();
		}
	}

	public boolean changePass(String email, String newPass) {
		Session session = CommonDao.getSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			Criteria criteria = session.createCriteria(UserObj.class);
			criteria.add(Restrictions.eq("email", email));
			UserObj e = (UserObj) criteria.uniqueResult();
			if (e != null) {
				e.setPassword(Util.hash256(newPass));
				session.update(e);// saveOrUpdate(e);
				tx.commit();
				return true;
			}
			return false;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			// Log.err("", "add", e.getMessage());
			return false;

		} finally {
			session.close();
		}
	}

	public List<UserObj> getListBy(Integer userId, Integer type, Integer status) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(UserObj.class);
			if (userId != null) {
				cr.add(Restrictions.eq("userId", userId));
			}
			if (type != null) {
				cr.add(Restrictions.eq("type", type));
			}
			if (status != null) {
				cr.add(Restrictions.eq("status", status));
			}
			List result = cr.list();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
}
