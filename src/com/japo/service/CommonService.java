package com.japo.service;

import java.util.List;

import com.japo.dao.CommonDao;
import com.japo.dao.obj.AbstractObj;

public abstract class CommonService {

	protected List<AbstractObj> getList(Class<? extends AbstractObj> T) {
		CommonDao cd = new CommonDao();
		return cd.getList(T);
	}

	protected String add(AbstractObj ob) {
		CommonDao cd = new CommonDao();
		return cd.add(ob);
	}

	protected AbstractObj add2(AbstractObj ob) {
		CommonDao cd = new CommonDao();
		return cd.add2(ob);
	}

	public boolean update(AbstractObj obj) {
		CommonDao cd = new CommonDao();
		return cd.addOrUpdate(obj);
	}

	protected boolean delete(AbstractObj obj) {
		CommonDao cd = new CommonDao();
		return cd.delete(obj);
	}

	protected List<AbstractObj> getList(Class<? extends AbstractObj> T, String fieldName, Object value) {
		CommonDao cd = new CommonDao();
		return cd.getListByField(T, fieldName, value);
	}
}
