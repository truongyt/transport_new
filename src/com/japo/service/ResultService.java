package com.japo.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.japo.Util.Constant;
import com.japo.dao.obj.CustomerObj;
import com.japo.dao.obj.LocationHistoryObj;
import com.japo.dao.obj.LocationObj;
import com.japo.dao.obj.OrderResultObj;
import com.japo.dao.obj.ProductObj;
import com.japo.dao.obj.ProductTypeObj;
import com.japo.dao.obj.ProviderObj;
import com.japo.dao.obj.StatusHistoryObj;
import com.japo.dao.obj.StatusObj;
import com.japo.dao.obj.TransportOrderObj;
import com.japo.dao.obj.UserObj;

/**
 * 
 * @author PhucNT
 *
 */
public class ResultService {
	private TransportOrderService transportOrderService;
	private ProviderService providerService;
	private CustomerService customerService;
	private StatusService statusService;
	private LocationService locationService;
	private StatusHistoryService statusHistoryService;
	private LocationHistoryService locationHistoryService;
	private UserService userService;
	private SimpleDateFormat timeFormat;
	private ProductService productService;
	private ProductTypeService productTypeService;

	public ResultService() {
		// TODO Auto-generated constructor stub
		transportOrderService = new TransportOrderService();
		providerService = new ProviderService();
		customerService = new CustomerService();
		statusService = new StatusService();
		locationService = new LocationService();
		statusHistoryService = new StatusHistoryService();
		locationHistoryService = new LocationHistoryService();
		userService = new UserService();
		timeFormat = new SimpleDateFormat("yyyy/MM/dd");
		productService = new ProductService();
		productTypeService = new ProductTypeService();
	}

	public String getJsonOrderDetailById(String idString) {
		JsonObject detail = new JsonObject();

		TransportOrderObj obj = transportOrderService.getObjectById(idString);

		StatusObj statusObj = statusService.getObjectById(obj.getStatusId());
		LocationObj locationObj = locationService.getObjectById(obj.getLocationId());
		ProviderObj providerObj = providerService.getObjectById(obj.getProviderId());
		CustomerObj senderObj = customerService.getObjectById(obj.getSenderId());
		CustomerObj receipterObj = customerService.getObjectById(obj.getReceipterId());
		List<LocationHistoryObj> locationHistoryObjs = locationHistoryService.getListByTransOrderId(idString);

		if (!locationHistoryObjs.isEmpty()) {
			detail.addProperty("time", timeFormat.format(locationHistoryObjs.get(0).getTime()));
			detail.addProperty("user", userService.getObjectById(locationHistoryObjs.get(0).getUserId()).getName());
		}

		detail.addProperty("id", obj.getTransportOrderId());
		detail.addProperty("fId", obj.getTransportOrderId());
		detail.addProperty("providerId", obj.getProviderId());
		detail.addProperty("provider", providerObj.getProviderName());
		detail.addProperty("statusId", obj.getStatusId());
		detail.addProperty("status", statusObj.getStatusName());
		detail.addProperty("locationId", obj.getLocationId());
		detail.addProperty("location", locationObj.getLocationName());
		detail.addProperty("senderId", obj.getSenderId());
		detail.addProperty("senderName", senderObj.getName());
		detail.addProperty("senderEmail", senderObj.getEmail());
		detail.addProperty("senderAddress", senderObj.getAdress());
		detail.addProperty("senderPhone", senderObj.getPhone());
		detail.addProperty("receipterId", obj.getReceipterId());
		detail.addProperty("receipterName", receipterObj.getName());
		detail.addProperty("receipterEmail", receipterObj.getEmail());
		detail.addProperty("receipterAddress", receipterObj.getAdress());
		detail.addProperty("receipterPhone", receipterObj.getPhone());
		detail.addProperty("providerOrderId", obj.getProviderOrderId());
		detail.addProperty("transCode", obj.getTransCode());
		detail.addProperty("paidStatus", obj.getPaidStatus());
		detail.addProperty("other", obj.getOther());

		String products = getJsonProductsDetail(idString);

		String result = "[" + detail.toString() + "," + products + "]";
		return result;
	}

	public String getJsonProductsDetail(String id) {
		List<ProductObj> list = productService.getListByOrderId(id);
		List<JsonObject> result = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			ProductObj o = list.get(i);
			JsonObject product = new JsonObject();

			if (o.getTypeId() == 99) {
				product.addProperty("type", "n/a");
				product.addProperty("name", o.getName());
				product.addProperty("quantity", o.getQuantity());
				product.addProperty("unitPrice", "n/a");
				product.addProperty("price", Integer.parseInt(o.getOther()));
			} else {
				ProductTypeObj p = productTypeService.getObjectById(o.getTypeId());

				product.addProperty("type", p.getTypeName());
				product.addProperty("name", o.getName());
				product.addProperty("quantity", o.getQuantity());
				product.addProperty("unitPrice", p.getPrice());
				product.addProperty("price", o.getQuantity() * p.getPrice());
			}
			result.add(product);
		}

		return result.toString();
	}

	public List<JsonObject> getListJsonStatusHistory(String id) {
		List<JsonObject> result = new ArrayList<>();

		List<StatusHistoryObj> list = statusHistoryService.getListByTransOrderId(id);
		for (int i = 0; i < list.size(); i++) {
			StatusHistoryObj obj = list.get(i);
			StatusObj statusObj = statusService.getObjectById(obj.getStatusId());
			UserObj userObj = userService.getObjectById(obj.getUserId());
			JsonObject json = new JsonObject();

			json.addProperty("time", timeFormat.format(obj.getTime()));
			json.addProperty("status", statusObj.getStatusName());
			json.addProperty("userId", obj.getUserId());
			json.addProperty("user", userObj.getName());
			json.addProperty("detail", obj.getDetail());

			result.add(json);
		}

		return result;
	}

	public List<JsonObject> getListJsonLocationHistory(String id) {
		List<JsonObject> result = new ArrayList<>();

		List<LocationHistoryObj> list = locationHistoryService.getListByTransOrderId(id);
		for (int i = 0; i < list.size(); i++) {
			LocationHistoryObj obj = list.get(i);
			LocationObj statusObj = locationService.getObjectById(obj.getLocationId());
			UserObj userObj = userService.getObjectById(obj.getUserId());
			JsonObject json = new JsonObject();

			json.addProperty("time", timeFormat.format(obj.getTime()));
			json.addProperty("location", statusObj.getLocationName());
			json.addProperty("locationId", statusObj.getLocationId());
			json.addProperty("userId", obj.getUserId());
			json.addProperty("user", userObj.getName());
			json.addProperty("detail", obj.getDetail());

			result.add(json);
		}

		return result;
	}

	public JsonObject getJsonUserById(String idString) {
		JsonObject result = new JsonObject();

		int id = Integer.parseInt(idString);
		UserObj obj = userService.getObjectById(id);

		result.addProperty("id", obj.getUserId());
		result.addProperty("email", obj.getEmail());
		result.addProperty("name", obj.getName());
		result.addProperty("type", obj.getType());
		result.addProperty("status", obj.getStatus());

		return result;
	}
}
