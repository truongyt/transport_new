package com.japo.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.japo.dao.CommonDao;
import com.japo.dao.obj.AttachmentObj;

/**
 * 
 * @author PhucNT
 *
 */
public class AttachmentService extends CommonService {
	public List<AttachmentObj> getList() {
		List ls = super.getList(AttachmentObj.class);
		return (List<AttachmentObj>) ls;
	}

	public String add(AttachmentObj o) {
		return super.add(o);
	}

	public boolean edit(AttachmentObj o) {
		return super.update(o);
	}

	public boolean delete(AttachmentObj o) {
		return super.delete(o);
	}

	public List<AttachmentObj> getListByTransOrderId(String id) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(AttachmentObj.class);
			cr.add(Restrictions.eq("transOrderId", id));
			List result = cr.list();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
}
