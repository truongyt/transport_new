package com.japo.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.google.gson.JsonObject;
import com.japo.dao.CommonDao;
import com.japo.dao.obj.CustomerObj;
import com.japo.dao.obj.CustomerTypeObj;
import com.japo.dao.obj.LocationHistoryObj;
import com.japo.dao.obj.LocationObj;
import com.japo.dao.obj.UserObj;

public class CustomerService extends CommonService {
	public List<CustomerObj> getList() {
		List ls = super.getList(CustomerObj.class);
		return (List<CustomerObj>) ls;
	}

	public String add(CustomerObj o) {
		return super.add(o);
	}

	public boolean edit(CustomerObj o) {
		return super.update(o);
	}

	public boolean delete(CustomerObj o) {
		return super.delete(o);
	}

	public CustomerObj getObjectById(int id) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(CustomerObj.class);
			cr.add(Restrictions.eq("customerId", id));
			CustomerObj result = (CustomerObj) cr.uniqueResult();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public CustomerObj getObjectByEmail(String mail) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(CustomerObj.class);
			cr.add(Restrictions.eq("email", mail));
			List<CustomerObj> result = (List<CustomerObj>) cr.list();
			trans.commit();
			if (result.size() < 1) {
				return null;
			} else {
				return result.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
	
	public List<CustomerObj> getAllObjectByEmail(String mail) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(CustomerObj.class);
			cr.add(Restrictions.eq("email", mail));
			List<CustomerObj> result = (List<CustomerObj>) cr.list();
			trans.commit();
			if (result.size() < 1) {
				return null;
			} else {
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public CustomerObj getObjectByJapo(String email, String phone) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(CustomerObj.class);
			cr.add(Restrictions.eq("email", email));
			cr.add(Restrictions.eq("phone", phone));
			List<CustomerObj> result = (List<CustomerObj>) cr.list();
			trans.commit();
			if (result.size() < 1) {
				return null;
			} else {
				return result.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public List<JsonObject> getJsonList() {
		CustomerTypeService customerTypeService = new CustomerTypeService();

		List<JsonObject> result = new ArrayList<>();
		List<CustomerObj> list = getList();

		for (int i = 0; i < list.size(); i++) {
			CustomerObj obj = list.get(i);
			CustomerTypeObj customerTypeObj = customerTypeService.getObjectById(obj.getType());
			JsonObject json = new JsonObject();

			json.addProperty("id", obj.getCustomerId());
			json.addProperty("name", obj.getName());
			json.addProperty("email", obj.getEmail());
			json.addProperty("address", obj.getAdress());
			json.addProperty("phone", obj.getPhone());
			json.addProperty("type_id", obj.getType());
			json.addProperty("type", customerTypeObj.getType());

			result.add(json);
		}

		return result;
	}

	public JsonObject getJson(int id) {
		CustomerTypeService customerTypeService = new CustomerTypeService();
		CustomerObj obj = getObjectById(id);
		CustomerTypeObj customerTypeObj = customerTypeService.getObjectById(obj.getType());
		JsonObject json = new JsonObject();

		json.addProperty("id", obj.getCustomerId());
		json.addProperty("name", obj.getName());
		json.addProperty("email", obj.getEmail());
		json.addProperty("address", obj.getAdress());
		json.addProperty("phone", obj.getPhone());
		json.addProperty("type_id", obj.getType());
		json.addProperty("type", customerTypeObj.getType());

		return json;
	}

	public List<CustomerObj> getListBy(Integer type) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(CustomerObj.class);
			if (type != null) {
				cr.add(Restrictions.eq("type", type));
			}
			List result = cr.list();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
}
