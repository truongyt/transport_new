package com.japo.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.japo.dao.CommonDao;
import com.japo.dao.obj.ProductObj;
import com.japo.dao.obj.SubProductObj;

public class SubProductService extends CommonService {
	public List<SubProductObj> getList() {
		List ls = super.getList(SubProductObj.class);
		return (List<SubProductObj>) ls;
	}

	public String add(SubProductObj o) {
		return super.add(o);
	}

	public boolean edit(SubProductObj o) {
		return super.update(o);
	}

	public boolean delete(SubProductObj o) {
		return super.delete(o);
	}

	public SubProductObj getObjectById(String id) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(SubProductObj.class);
			cr.add(Restrictions.eq("subProductId", id));
			SubProductObj result = (SubProductObj) cr.uniqueResult();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

//	public List<SubProductObj> getListByProductId(String id) {
//		Session session = CommonDao.getSession();
//		Transaction trans = null;
//		try {
//			trans = session.beginTransaction();
//			Criteria cr = session.createCriteria(SubProductObj.class);
//			cr.add(Restrictions.eq("productId", id));
//			List result = cr.list();
//			trans.commit();
//			return result;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		} finally {
//			session.close();
//		}
//	}
	
	public List<SubProductObj> getListByOrderId(String id) {
		Session session = CommonDao.getSession();
		Transaction trans = null;
		try {
			trans = session.beginTransaction();
			Criteria cr = session.createCriteria(SubProductObj.class);

			cr.add(Restrictions.eq("transportOrderId", id));

			List result = cr.list();
			trans.commit();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
}
