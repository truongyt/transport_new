package com.japo.Util;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.japo.dao.obj.UserObj;
import com.japo.service.UserService;

/**
 * @author PhucNT
 */
@WebFilter(urlPatterns = { "/adminMainPage", "/adminAddUser", "/adminDeactiveUser", "/adminDeleteOrder",
		"/adminEditUser", "/adminGetUserInfo", "/adminResetPassword", "/adminSearchOrderBy", "/adminSearchUserBy",
		"/adminUpdateOrder", "/adminUserPage" })
public class AdminAccessFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public AdminAccessFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		HttpSession session = req.getSession();
		String userEmail = (String) session.getAttribute("userEmail");
		UserObj userObj = new UserService().getObjectByEmail(userEmail);
		Boolean ok = true;

		if (userObj.getType() == 2) {
			ok = false;
		}
		;
		String path = req.getRequestURI();
		if (!ok) {
			res.getWriter().println("Permission Denided!");
			response.flushBuffer();
		} else
			chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
