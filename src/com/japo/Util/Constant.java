package com.japo.Util;

import java.util.Arrays;
import java.util.List;

public class Constant {
	public final static String hibernateConfig = "/Users/truongnguyen/ichiba/workspace/japo_transport_server/WebContent/WEB-INF/config/hibernate.cfg.xm";
			//"/WEB-INF/config/hibernate.cfg.xml";
	//public final static String hibernateConfig = "/WEB-INF/config/hibernate.cfg.test.xml";
	public final static String privateKey = "/WEB-INF/config/rsa_1024_priv.pem";
	public final static String defaultPassword = "ytasia@123";

	public final static String checkOrderLocationUrl = "http://transport.japo.com.vn/check";

	public final static String reportMailAddress = "test.ytasia@gmail.com";
	public final static String reportMailPassword = "ytasia@123";

	public final static String availableStatus = "AVA";
	public final static String notAvailableStatus = "NOT";
	public final static String wrongStatus = "WRO";
	public final static String deleteStatus = "DET";

	public final static String waitingLocation = "WAI";
	public final static String inJapanLocation = "JPI";
	public final static String outJapanLocation = "JPO";
	public final static String inVietnamLocation = "VNI";
	public final static String outVietnamLocation = "VNO";

	public final static List<String> months = Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11",
			"12");
	public final static List<String> years = Arrays.asList("2016", "2017", "2018", "2019", "2020", "2021", "2022",
			"2023", "2024", "2025");
	
	//------------------------------//
	public final static List<String> airportList = Arrays.asList("Narita", "Haneda", "Hanoi", "Danang", "Hochiminh");
}
