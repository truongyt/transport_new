package com.japo.Util;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.bind.DatatypeConverter;

import org.apache.tomcat.util.buf.HexUtils;

import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.engines.RSAEngine;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.util.encoders.Base64Encoder;

import com.mysql.jdbc.util.Base64Decoder;

import org.apache.commons.codec.binary.Base64;

public class Util {
	private static long dateOriginal;
	static {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2016);
		cal.set(Calendar.MONTH, Calendar.JANUARY);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		dateOriginal = cal.getTime().getTime();
	}

	public static synchronized String generateId() {
		String reverse="";
		for(int i=1;i<5;i++){
	    
		
		Calendar cal = Calendar.getInstance();
		
		Long l = System.currentTimeMillis();// - dateOriginal;
		cal.setTimeInMillis(l);
		Calendar cal2 = Calendar.getInstance();
		Date date=new Date(cal.getTime().getYear(), cal.getTime().getMonth(), cal.getTime().getDay());
		cal2.setTime(date);
		System.out.println("System.currentTimeMillis():"+i+":::"+l+" - "+cal2.getTimeInMillis());
		System.out.println("Lech:"+(l-cal2.getTimeInMillis()));
		
		 reverse = new StringBuffer(String.valueOf(l)).reverse().toString();
		reverse = Long.toHexString(Long.parseLong(reverse));
		reverse = new StringBuffer(reverse).insert(5, "-").toString();
		}
		return reverse;
	}

	public static String hash256(String s) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(s.getBytes()); // or UTF-16 if needed
			byte[] digest = md.digest();
			return HexUtils.toHexString(digest);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public static Date stringToDate(String stringDate) {
		if (stringDate != "") {
			java.util.Date d = new java.util.Date();
			SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd", Locale.JAPAN);
			try {
				d = f.parse(stringDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Date date = new Date(d.getTime());

			return date;
		} else {
			return null;
		}
	}

	// Long startDateMilli = new SimpleDateFormat("yyyy/MM/dd
	// hh:mm").parse(time1String).getTime();
	// time1 = new Timestamp(startDateMilli);

	public static String getDecryptedPass(String privateKey, String encryptedData) {
		String outputData = null;

		try {
			PrivateKey key;
			key = loadPrivateKey(privateKey);

			outputData = decrypt(key, DatatypeConverter.parseBase64Binary(encryptedData));
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (

		InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return outputData;
	}

	private static String decrypt(Key decryptionKey, byte[] buffer) {
		try {
			Cipher rsa;
			rsa = Cipher.getInstance("RSA");
			rsa.init(Cipher.DECRYPT_MODE, decryptionKey);
			byte[] utf8 = rsa.doFinal(buffer);
			return new String(utf8, "UTF8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static PrivateKey loadPrivateKey(String key64) throws GeneralSecurityException {
		byte[] clear = DatatypeConverter.parseBase64Binary(key64);
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(clear);
		KeyFactory fact = KeyFactory.getInstance("RSA");
		PrivateKey priv = fact.generatePrivate(keySpec);
		Arrays.fill(clear, (byte) 0);
		return priv;
	}

	public static int digitSum(int n) {
		int sum = 0;
		String i = String.valueOf(n);
		for (int j = 0; j < i.length(); j++) {
			sum = sum + Integer.parseInt(Character.toString(i.charAt(j)));
		}

		return sum;
	}
}
