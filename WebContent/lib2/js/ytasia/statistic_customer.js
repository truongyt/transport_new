/**
 * Customer classify
 */
function setCustomerClassifyData(data) {

	$
			.plot(
					$("#classify_chart"),
					data,
					{
						series : {
							pie : {
								show : true,
								radius : 1,
								label : {
									show : true,
									radius : 2 / 3,
									formatter : function(label, series) {
										return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
												+ label
												+ '<br/>'
												+ Math.round(series.percent)
												+ '%</div>';
									},
									threshold : 0.1
								}
							}
						},
						legend : {
							show : false
						},
						colors : [ "#ee7951", "#6db6ee", "#95c832", "#993eb7",
								"#3ba3aa" ]
					});

}

/**
 * By year statistic
 */
function setByYearData(year, yearData) {
	var data1 = [ {
		label : "khách hàng",
		data : yearData,
		color : '#f1553c'
	// 6fc8dd
	} ];

	$.plot($("#by_year_chart"), data1, {
		xaxis : {
			show : true,
			min : (new Date(year - 1, 12, 1)).getTime(),
			max : (new Date(year, 11, 2)).getTime(),
			mode : "time",
			tickSize : [ 1, "month" ],
			monthNames : [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
					"Aug", "Sep", "Oct", "Nov", "Dec" ],
			tickLength : 1,
			axisLabel : 'Month',
			axisLabelFontSizePixels : 11,
		},
		yaxis : {
			axisLabel : 'Amount',
			axisLabelUseCanvas : true,
			axisLabelFontSizePixels : 11,
			autoscaleMargin : 0.01,
			axisLabelPadding : 5
		},
		series : {
			lines : {
				show : true,
				fill : true,
				fillColor : {
					colors : [ {
						opacity : 0.2
					}, {
						opacity : 0.2
					} ]
				},
				lineWidth : 1.5
			},
			points : {
				show : true,
				radius : 2.5,
				fill : true,
				fillColor : "#ffffff",
				symbol : "circle",
				lineWidth : 1.1
			}
		},
		grid : {
			hoverable : true,
			clickable : true
		},
		legend : {
			show : false
		}
	});

	function showTooltip(x, y, contents) {
		$('<div id="tooltip" class="chart-tooltip">' + contents + '</div>')
				.css({
					position : 'absolute',
					display : 'none',
					top : y - 46,
					left : x - 9,
					'z-index' : '9999',
					opacity : 0.9
				}).appendTo("body").fadeIn(200);
	}

	var previousPoint = null;
	$("#by_year_chart")
			.bind(
					"plothover",
					function(event, pos, item) {
						$("#x").text(pos.x.toFixed(2));
						$("#y").text(pos.y.toFixed(2));

						if ($("#by_year_chart").length > 0) {
							if (item) {
								if (previousPoint != item.dataIndex) {
									previousPoint = item.dataIndex;

									$("#tooltip").remove();
									var x = item.datapoint[0].toFixed(2), y = item.datapoint[1]
											.toFixed(2);

									showTooltip(item.pageX, item.pageY,
											"<strong>" + y + "</strong>" + " "
													+ item.series.label);
								}
							} else {
								$("#tooltip").remove();
								previousPoint = null;
							}
						}
					});

	$("#by_year_chart").bind(
			"plotclick",
			function(event, pos, item) {
				if (item) {
					$("#clickdata").text(
							"You clicked point " + item.dataIndex + " in "
									+ item.series.label + ".");
					plot.highlight(item.series, item.datapoint);
				}
			});
}
