function drawGraphic(result) {
	for (i = 0; i < result.length; i++) {

		switch (result[i].locationId) {
		case 'WAI':
			if (i == (result.length - 1)) {
				$('#locations')
						.append(
								'<li class="bg-info">'
										+ ' <div class="top-info">'
										+ ' <a href="#">'
										+ result[i].location
										+ '</a>'
										+ '</div>'
										+ '<a href="#"><i class="icon-checkmark3"></i></a>'
										+ '<span class="bottom-info bg-primary">'
										+ result[i].time + '</span>' + '</li>');
			} else {
				$('#locations')
						.append(
								'<li class="bg-primary">'
										+ ' <div class="top-info">'
										+ ' <a href="#">'
										+ result[i].location
										+ '</a>'
										+ '</div>'
										+ '<a href="#"><i class="icon-checkmark3"></i></a>'
										+ '<span class="bottom-info bg-primary">'
										+ result[i].time + '</span>' + '</li>');
			}
			break;
		case 'JPI':
			if (i == (result.length - 1)) {
				$('#locations').append(
						'<li class="bg-info">' + ' <div class="top-info">'
								+ ' <a href="#">' + result[i].location + '</a>'
								+ '</div>'
								+ '<a href="#"><i class="icon-truck"></i></a>'
								+ '<span class="bottom-info bg-primary">'
								+ result[i].time + '</span>' + '</li>');
			} else {
				$('#locations').append(
						'<li class="bg-primary">' + ' <div class="top-info">'
								+ ' <a href="#">' + result[i].location + '</a>'
								+ '</div>'
								+ '<a href="#"><i class="icon-truck"></i></a>'
								+ '<span class="bottom-info bg-primary">'
								+ result[i].time + '</span>' + '</li>');
			}
			break;
		case 'JPO':
			if (i == (result.length - 1)) {
				$('#locations')
						.append(
								'<li class="bg-info">'
										+ ' <div class="top-info">'
										+ ' <a href="#">'
										+ result[i].location
										+ '</a>'
										+ '</div>'
										+ '<a href="#"><i class="icon-airplane2"></i></a>'
										+ '<span class="bottom-info bg-primary">'
										+ result[i].time + '</span>' + '</li>');
			} else {
				$('#locations')
						.append(
								'<li class="bg-primary">'
										+ ' <div class="top-info">'
										+ ' <a href="#">'
										+ result[i].location
										+ '</a>'
										+ '</div>'
										+ '<a href="#"><i class="icon-airplane2"></i></a>'
										+ '<span class="bottom-info bg-primary">'
										+ result[i].time + '</span>' + '</li>');
			}
			break;
		case 'VNI':
			if (i == (result.length - 1)) {
				$('#locations').append(
						'<li class="bg-info">' + ' <div class="top-info">'
								+ ' <a href="#">' + result[i].location + '</a>'
								+ '</div>'
								+ '<a href="#"><i class="icon-truck"></i></a>'
								+ '<span class="bottom-info bg-primary">'
								+ result[i].time + '</span>' + '</li>');
			} else {
				$('#locations').append(
						'<li class="bg-primary">' + ' <div class="top-info">'
								+ ' <a href="#">' + result[i].location + '</a>'
								+ '</div>'
								+ '<a href="#"><i class="icon-truck"></i></a>'
								+ '<span class="bottom-info bg-primary">'
								+ result[i].time + '</span>' + '</li>');
			}
			break;
		case 'VNO':
			if (i == (result.length - 1)) {
				$('#locations').append(
						'<li class="bg-info">' + ' <div class="top-info">'
								+ ' <a href="#">' + result[i].location + '</a>'
								+ '</div>'
								+ '<a href="#"><i class="icon-exit3"></i></a>'
								+ '<span class="bottom-info bg-primary">'
								+ result[i].time + '</span>' + '</li>');
			} else {
				$('#locations').append(
						'<li class="bg-primary">' + ' <div class="top-info">'
								+ ' <a href="#">' + result[i].location + '</a>'
								+ '</div>'
								+ '<a href="#"><i class="icon-exit3"></i></a>'
								+ '<span class="bottom-info bg-primary">'
								+ result[i].time + '</span>' + '</li>');
			}
			break;
		}
	}
}

function drawTable(result) {
	for (i = 0; i < result.length; i++) {
		switch (result[i].locationId) {
		case 'JPI':
			$('#s-table').append(
					'<tr><td>' + '<span class="text-info">' + result[i].time
							+ '</span>' + '</td><td>'
							+ "Đã về đến kho của YTAsia tại Nhật Bản"
							+ '</td></tr>');
			break;
		case 'JPO':
			$('#s-table').append(
					'<tr><td>' + '<span class="text-info">' + result[i].time
							+ '</span>' + '</td><td>'
							+ "Đã đưa lên máy bay về Việt Nam" + '</td></tr>');
			break;
		case 'VNI':
			$('#s-table')
					.append(
							'<tr><td>'
									+ '<span class="text-info">'
									+ result[i].time
									+ '</span>'
									+ '</td><td>'
									+ 'Đã đến kho của YTAsia tại Việt Nam <span class="text-info">( Có thể đến lấy hàng )</span>'
									+ '</td></tr>');
			break;
		case 'VNO':
			$('#s-table').append(
					'<tr><td>' + '<span class="text-info">' + result[i].time
							+ '</span>' + '</td><td>' + '<span class="text-danger">Đã trả hàng</span>'
							+ '</td></tr>');
			break;
		}
	}
}
