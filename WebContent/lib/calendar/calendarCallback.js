function createEvent(start, end, url, currentUser) {
	end = moment(end).subtract(1, "day");
	var start1 = start.format("YYYY-MM-DD");
	var end1 = end.format("YYYY-MM-DD");
	
	$("#allday").prop("checked", false);
	if ($("#allday").prop("checked")) {
		$("#startTime").hide();
		$("#endTime").hide();
	} else {
		$("#startTime").show();
		$("#endTime").show();
	};
	$("#startDate").val(start1);
	$("#endDate").val(end1);
	$("#startTime").val("09:00");
	$("#endTime").val("10:00");
	$("#title").val("New Event");
	$("#content").val("");
	$("#member").multiselect('deselectAll', false);
	$("#member").multiselect('updateButtonText');
	$("#creator").val(currentUser);
	$("#deleteDialog").hide();
	$("#submit").show();
	
	$("#dialog").dialog("open");
	
	$("#allday").click(function (e) {
		if ($("#allday").prop("checked")) {
			$("#startTime").hide();
			$("#endTime").hide();
		} else {
			$("#startTime").show();
			$("#endTime").show();
			//end1 = start1;
		}
	});
	$("#submit").click(function(e) {
		var title = $("#title").val();
		var content = $("#content").val();
		var location = $("#location").val();
		var member = $("#member").val();
		if (member != null) {
			member = member.toString();
		} else member = "";
		var allday = $("#allday").prop("checked");
		var start, end;
		if (allday) {
			start = $("#startDate").val() + " 09:00:00";
			end = $("#endDate").val() + " 10:00:00";
		} else {
			start = $("#startDate").val() + " " + $("#startTime").val() + ":00";
			end = $("#endDate").val() + " " + $("#endTime").val() + ":00";
		}
		var startDate = new Date(start);
		var endDate = new Date(end);
		if (startDate.getTime() > endDate.getTime()) {
			$.notify("不正な入力です。開始時間より大きく正しい終了時間を入力して下さい！", {
				position: "bottom center",
				className: "error"
			});
			return null;
		}
		if (title != "") {
			eventData = {
				id : "0",
				title : title,
				start : startDate,
				end : endDate,
			};
			$.ajax({
	    		url: url,
	    		data: 'type=new&title='+title+'&eventId='+"0"+'&startdate='+start+'&enddate='+end+'&content='+content+'&member='+member+'&location='+location+'&allday='+allday,
	    		type: 'POST',
	    		async: false,
	    		dataType: 'json',
	    		success: function(response){
	    			eventData.id = response.eventid;
	    			eventData.start = response.start;
	    			eventData.allDay = response.allDay;
	    			eventData.end = response.end;	
	    			eventData.color = response.color;
	    			$('#calendar').fullCalendar('renderEvent',
	    					eventData, true);
	    		},
	    		error: function(e){
	    			console.log("error:" + e.responseText);
	    		}
	    	});
		}
		$.notify("イベントを追加できました！", {
			position: "bottom center",
			className: "info"
		});
		$("#submit").unbind("click");
		$("#dialog").dialog("close");
	});
	$("#cancelDialog").click(function(e) {
		$("#dialog").dialog("close");
	});
	
	$('#calendar').fullCalendar('unselect');
}

/* When an event is clicked, open a dialog, update the event
 */
function editEvent(event, url) {
	var eventId = event.id
	var json_events;
	
	$("#allday").click(function (e) {
		if ($("#allday").prop("checked")) {
			$("#startTime").hide();
			$("#endTime").hide();
		} else {
			$("#startTime").show();
			$("#endTime").show();
		}
	});
	
	$.ajax({
		url : url,
		type : 'POST', // Send post data
		data : 'type=getbyid&title=' + '' + '&eventId=' + eventId
				+ '&startdate=' + '' + '&enddate=' + '',
		dataType : 'json',
		async : false,
		success : function(s) {
			json_events = s;
		}
	});
	var allday = event.allDay;
	var startDate, endDate, startTime, endTime;
	/*if (allday) {
		//$("#allday").prop("checked", true);
		startDate = moment(json_events.start).format("YYYY-MM-DD");
		endDate = moment(json_events.end).format("YYYY-MM-DD");
		$("#startTime").hide();startTime = "09:00"
		$("#endTime").hide();endTime = "10:00";
	} else {*/
		//$("#allday").prop("checked", false);
		startDate = moment(json_events.start).format("YYYY-MM-DD");
		endDate = moment(json_events.end).format("YYYY-MM-DD");
		startTime = moment(json_events.start).format("HH:mm"); 
		endTime = moment(json_events.end).format("HH:mm");
		$("#startTime").show();
		$("#endTime").show();
	//}
	
	var title = event.title;
	$("#startDate").val(startDate);
	$("#endDate").val(endDate);
	$("#startTime").val(startTime);
	$("#endTime").val(endTime);
	
	$("#title").val(title);
	$("#ghichu").val(json_events.ghichu);
	$("#tenhanhkhach").val(json_events.tenhanhkhach);
	$("#thongtinhanhkhach").val(json_events.thongtinhanhkhach);
	$("#member").multiselect('deselectAll', false);
	var memberArr = json_events.member.split(',');
	$("#member").multiselect('select', memberArr);
	$("#deleteDialog").show();
	$("#submit").show();
	
	// if the current user is not the owner of the event, hide OK and Delete buttons
	var isOwner = json_events.isOwner;
	if (!isOwner) {
		$("#submit").hide();
		$("#deleteDialog").hide();
	}
	
	$("#dialog").dialog("open");
	$("#submit").click(function(e) {
		var title = $("#title").val();
		var content = $("#ghichu").val();
		var location = $("#thongtinhanhkhach").val();
		var member = $("#tenhanhkhach").val();
		
		//var allday = $("#allday").prop("checked");
		var start, end;
		/*if (allday) {
			start = $("#startDate").val() + " 09:00:00";
			end = $("#endDate").val() + " 10:00:00";
		} else {*/
			start = $("#startDate").val() + " " + $("#startTime").val() + ":00";
			end = $("#endDate").val() + " " + $("#endTime").val() + ":00";
		//}
		if (new Date(start).getTime() > new Date(end).getTime()) {
			$.notify("thông tin nhập không đúng！ Vui lòng kiểm tra lại", {
				position: "bottom center",
				className: "error"
			});
			return null;
		}
		
		if (title != "") {
			console.log("AJAX REQUEST");
			$.ajax({
	    		url: url,
	    		data: 'type=update&title='+title+'&eventId='+eventId+'&startdate='+start+'&enddate='+end+
	    				'&ghichu='+content+'&thongtinhanhkhach='+location+'&tenhanhkhach='+member,//+'&allday='+allday,
	    		type: 'POST',
	    		async: false,
	    		dataType: 'json',
	    		success: function(response){
	    			event.id = response.eventid;
	    			event.title = title;
	    			event.start = moment(response.start);
	    			if (response.allDay) event.end = moment(response.end).add(1, "day");
	    			else event.end = moment(response.end);
	    			event.allDay = response.allDay;
	    			event.color = response.color;
	    			
	    			$('#calendar').fullCalendar('updateEvent',
	    					event, true);
	    			
	    		},
	    		error: function(e){
	    			console.log("error:" + e.responseText);
	    		}
	    	});
		}
		$("#submit").unbind("click");
		$('#calendar').fullCalendar('unselect');
		
		$("#dialog").dialog("close");
		$.notify("Thông tin chuyến bay đã được thay đổi！", {
			position: "bottom center",
			className: "info"
		});
	});
	
	$("#deleteDialog").click(function(e) {
		var check = confirm("Bạn muốn xoá？");
		if (check) {
			console.log("AJAX REQUEST");
			$.ajax({
	    		url: url,
	    		data: 'type=delete&eventId='+eventId,
	    		type: 'POST',
	    		dataType: 'json',
	    		success: function(events){
	    			$('#calendar').fullCalendar('removeEvents', eventId);
	                $('#calendar').fullCalendar('rerenderEvents' );
	    		},
	    		error: function(e){
	    			console.log("error:" + e.responseText);
	    		}
	    	});
		}
		$("#deleteDialog").unbind("click");
		$('#calendar').fullCalendar('unselect');
		$("#dialog").dialog("close");
		$.notify("Thông tin chuyến bay đã bị xoá！", {
			position: "bottom center",
			className: "info"
		});
	});
	
	$("#cancelDialog").click(function(e) {
		$("#dialog").dialog("close");
		$("#cancelDialog").unbind("click");
	});
}

function getJPVNFlight(url) {
	selectable = true;
	$.ajax({
		url : url,
		type : 'POST', // Send post data
		data : 'type=fetchJV&title=' + '' + '&eventId=' + "0"
				+ '&startdate=' + '' + '&enddate=' + '',
		dataType : 'json',
		async : false,
		success : function(events) {
			 $('#calendar').fullCalendar('removeEvents');
             $('#calendar').fullCalendar('addEventSource', events);         
             $('#calendar').fullCalendar('rerenderEvents' );
		}
	});
}
function getVNJPFlight(url) {
	selectable = false;
	$.ajax({
		url : url,
		type : 'POST', // Send post data
		data : 'type=fetchVJ&title=' + '' + '&eventId=' + "0"
				+ '&startdate=' + '' + '&enddate=' + '',
		dataType : 'json',
		async : false,
		success : function(events) {
			 $('#calendar').fullCalendar('removeEvents');
             $('#calendar').fullCalendar('addEventSource', events);         
             $('#calendar').fullCalendar('rerenderEvents' );
		}
	});
}

function getIndividualSchedule(url, name) {
	selectable = false;
	$.ajax({
		url : url,
		type : 'POST', // Send post data
		data : 'type=fetchIndi&name=' + name,
		dataType : 'json',
		async : false,
		success : function(events) {
			 $('#calendar').fullCalendar('removeEvents');
             $('#calendar').fullCalendar('addEventSource', events);         
             $('#calendar').fullCalendar('rerenderEvents' );
		}
	});
}
