/**
 * By month statistic
 */
function setByMonthData(monthData) {

	var previousPoint;

	var ds = new Array();

	ds.push({
		data : monthData,
		bars : {
			show : true,
			barWidth : 0.4,
			order : 1,
		}
	});

	// tooltip function
	function showTooltip(x, y, contents, areAbsoluteXY) {
		var rootElt = 'body';

		$('<div id="tooltip" class="chart-tooltip">' + contents + '</div>')
				.css({
					top : y - 50,
					left : x - 6,
					opacity : 0.9
				}).prependTo(rootElt).show();
	}
	;

	// Display graph
	$.plot($("#by_month_chart"), ds, {
		colors : [ "#ee7951", "#6db6ee", "#95c832", "#993eb7", "#3ba3aa" ],
		grid : {
			hoverable : true
		}
	});

	// add tooltip event
	$("#by_month_chart")
			.bind(
					"plothover",
					function(event, pos, item) {
						if (item) {
							if (previousPoint != item.datapoint) {
								previousPoint = item.datapoint;

								// delete de prГ©cГ©dente
								// tooltip
								$('.chart-tooltip').remove();

								var x = item.datapoint[0];

								// All the bars concerning a
								// same x value must display
								// a tooltip
								// with this value and not
								// the shifted value
								if (item.series.bars.order) {
									for (var i = 0; i < item.series.data.length; i++) {
										if (item.series.data[i][3] == item.datapoint[0])
											x = item.series.data[i][0];
									}
								}

								var y = item.datapoint[1];

								showTooltip(item.pageX + 5, item.pageY + 5,
										"(ngày " + x + ") " + y + " đơn hàng");

							}
						} else {
							$('.chart-tooltip').remove();
							previousPoint = null;
						}

					});
}

/**
 * By year statistic
 */
function setByYearData(year, yearData) {
	var data1 = [ {
		label : "đơn hàng",
		data : yearData,
		color : '#f1553c'
	// 6fc8dd
	} ];

	$.plot($("#by_year_chart"), data1, {
		xaxis : {
			show : true,
			min : (new Date(year - 1, 12, 1)).getTime(),
			max : (new Date(year, 11, 2)).getTime(),
			mode : "time",
			tickSize : [ 1, "month" ],
			monthNames : [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
					"Aug", "Sep", "Oct", "Nov", "Dec" ],
			tickLength : 1,
			axisLabel : 'Month',
			axisLabelFontSizePixels : 11,
		},
		yaxis : {
			axisLabel : 'Amount',
			axisLabelUseCanvas : true,
			axisLabelFontSizePixels : 11,
			autoscaleMargin : 0.01,
			axisLabelPadding : 5
		},
		series : {
			lines : {
				show : true,
				fill : true,
				fillColor : {
					colors : [ {
						opacity : 0.2
					}, {
						opacity : 0.2
					} ]
				},
				lineWidth : 1.5
			},
			points : {
				show : true,
				radius : 2.5,
				fill : true,
				fillColor : "#ffffff",
				symbol : "circle",
				lineWidth : 1.1
			}
		},
		grid : {
			hoverable : true,
			clickable : true
		},
		legend : {
			show : false
		}
	});

	function showTooltip(x, y, contents) {
		$('<div id="tooltip" class="chart-tooltip">' + contents + '</div>')
				.css({
					position : 'absolute',
					display : 'none',
					top : y - 46,
					left : x - 9,
					'z-index' : '9999',
					opacity : 0.9
				}).appendTo("body").fadeIn(200);
	}

	var previousPoint = null;
	$("#by_year_chart")
			.bind(
					"plothover",
					function(event, pos, item) {
						$("#x").text(pos.x.toFixed(2));
						$("#y").text(pos.y.toFixed(2));

						if ($("#by_year_chart").length > 0) {
							if (item) {
								if (previousPoint != item.dataIndex) {
									previousPoint = item.dataIndex;

									$("#tooltip").remove();
									var x = item.datapoint[0].toFixed(2), y = item.datapoint[1]
											.toFixed(2);

									showTooltip(item.pageX, item.pageY,
											"<strong>" + y + "</strong>" + " "
													+ item.series.label);
								}
							} else {
								$("#tooltip").remove();
								previousPoint = null;
							}
						}
					});

	$("#by_year_chart").bind(
			"plotclick",
			function(event, pos, item) {
				if (item) {
					$("#clickdata").text(
							"You clicked point " + item.dataIndex + " in "
									+ item.series.label + ".");
					plot.highlight(item.series, item.datapoint);
				}
			});
}
