package com.japo.test;

import java.lang.reflect.Array;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.persistence.criteria.CriteriaBuilder.In;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.japo.Util.Constant;
import com.japo.Util.Util;
import com.japo.dao.CommonDao;
import com.japo.dao.obj.CustomerHistoryObj;
import com.japo.dao.obj.CustomerObj;
import com.japo.dao.obj.LocationHistoryObj;
import com.japo.dao.obj.LocationObj;
import com.japo.dao.obj.OrderResultObj;
import com.japo.dao.obj.ProductObj;
import com.japo.dao.obj.ProductTypeObj;
import com.japo.dao.obj.ShipmentObj;
import com.japo.dao.obj.StatusHistoryObj;
import com.japo.dao.obj.StatusObj;
import com.japo.dao.obj.SubProductObj;
import com.japo.dao.obj.TransportOrderObj;
import com.japo.dao.obj.UserObj;
import com.japo.service.CustomerHistoryService;
import com.japo.service.CustomerService;
import com.japo.service.LocationHistoryService;
import com.japo.service.LocationService;
import com.japo.service.ProductService;
import com.japo.service.ProductTypeService;
import com.japo.service.ProviderService;
import com.japo.service.ReportService;
import com.japo.service.ResultService;
import com.japo.service.ShipmentService;
import com.japo.service.StatisticService;
import com.japo.service.StatusHistoryService;
import com.japo.service.StatusService;
import com.japo.service.SubProductService;
import com.japo.service.TransportOrderService;
import com.japo.service.UserService;

public class Test {
	public static void main(String[] args) {
		CommonDao.setFactory("/Users/Apple/workspace/transport-server/resources/hibernate.cfg.xml");
		TransportOrderService transportOrderService = new TransportOrderService();
		ProviderService providerService = new ProviderService();
		UserService userService = new UserService();
		CustomerService customerService = new CustomerService();
		ResultService resultService = new ResultService();
		StatusService statusService = new StatusService();
		LocationService locationService = new LocationService();
		StatusHistoryService statusHistoryService = new StatusHistoryService();
		LocationHistoryService locationHistoryService = new LocationHistoryService();
		ProductService productService = new ProductService();
		ProductTypeService productTypeService = new ProductTypeService();
		SubProductService subProductService = new SubProductService();
		ShipmentService shipmentService = new ShipmentService();
		StatisticService statisticService = new StatisticService();
		CustomerHistoryService customerHistoryService = new CustomerHistoryService();

		// LocationObj locationObj =
		// locationService.getObjectById(o.getLocationId());
		// StatusObj statusObj = statusService.getObjectById(o.getStatusId());
		//
		// System.out.println("" + locationObj.getLocationName() + " " +
		// o.getOrderDetail() + " "
		// + statusObj.getStatusName() + " " + o.getReceipterId());
		//
		// List<LocationHistoryObj> list =
		// locationHistoryService.getListByTransOrderId(o.getTransportOrderId());
		// for (int i = 0; i < list.size(); i++) {
		// LocationObj location =
		// locationService.getObjectById(list.get(i).getLocationId());
		// UserObj user = userService.getObjectById(list.get(i).getUserId());
		// System.out.println(list.get(i).getTime() + " " +
		// location.getLocationName() + " " + user.getName());
		// }
		//
		// List<StatusHistoryObj> list2 =
		// statusHistoryService.getListByTransOrderId(o.getTransportOrderId());
		// for (int i = 0; i < list2.size(); i++) {
		// StatusObj status =
		// statusService.getObjectById(list2.get(i).getStatusId());
		// UserObj user = userService.getObjectById(list2.get(i).getUserId());
		// System.out.println(list2.get(i).getTime() + " " +
		// status.getStatusName() + " " + user.getName());
		// }

		// System.out.println(resultService.getJsonOrderDetailById("0000001"));
		// System.out.println(resultService.getListJsonLocationHistory("0000001"));

		// System.out.println(statisticService.getJsonCustomerClassifyData().toString());

		// List<CustomerHistoryObj> l1 = customerHistoryService.getList();
		// List<CustomerHistoryObj> l2 =
		// customerHistoryService.getListByUserId(1);
		// List<CustomerHistoryObj> l3 =
		// customerHistoryService.getListBetween(Util.stringToDate("2016-4-1"),
		// Util.stringToDate("2016-8-1"));
		//
		// for (int i = 0; i < l1.size(); i++) {
		// System.out.println("full: " + l1.get(i).getTime());
		// }
		//
		// for (int i = 0; i < l2.size(); i++) {
		// System.out.println("by Phucnt: " + l2.get(i).getTime());
		// }
		// for (int i = 0; i < l3.size(); i++) {
		// System.out.println("April: " + l3.get(i).getTime());
		// }

		// System.out.println(statisticService.getJsonCustomerDataByYear(2016));

		// System.out.println(statisticService.getJsonUserClassifyData());

		// int month = Integer.parseInt((new SimpleDateFormat("MM")).format(new
		// Date()));
		// int year = Integer.parseInt((new SimpleDateFormat("yyyy")).format(new
		// Date()));
		//
		// System.out.println(year + " " + month);

		String webs[] = { "http://codefights.com", "https://uk.domainmaster.com",
				"https://uk.domainmaster.com/websites/website-builder", "https://in.domainmaster.com" };

		System.out.println(domainAuction(webs).toString());

	}

	public static ArrayList<Integer> domainAuction(String[] websites) {
		ArrayList<String> domains = new ArrayList<>();
		HashMap<String, Integer> domainCount = new HashMap<>();
		for (String website : websites) {
			int i = 0;
			while (!website.substring(i, i + 3).equals("://")) {
				i++;
			}
			System.out.println("website: "+website);
			i += 3;
			while (i < website.length() && website.charAt(i) != '/') {
				i++;
			}
			int j = i - 1;
			int cnt = 0;
			while (website.charAt(j) != '/') {
				cnt += website.charAt(j) == '/' ? 1 : 0;
				if (cnt == 2) {
					break;
				}
				j--;
			}
			String domain = website.substring(j + 1, i);
			domains.add(domain);
			if (!domainCount.containsKey(domain)) {
				domainCount.put(domain, 0);
			}
			domainCount.put(domain, domainCount.get(domain) + 1);
		}
		ArrayList<Integer> counts = new ArrayList<>();
		for (String domain : domains) {
			counts.add(domainCount.get(domain));
		}
		return counts;
	}

}
