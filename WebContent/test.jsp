<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/html2canvasjs"></script>

<script>
	$('.print').on('click', function() {
		html2canvas(document.body, {
			onrendered : function(canvas) {

				$("#page").hide();
				document.body.appendChild(canvas);
				window.print();
				$('canvas').remove();
				$("#page").show();
			}
		});

	});
</script>
</head>
<body>
<body>
	<!-- This will print your page exactly how it looks in the browser -->
	<button class='print'>Print</button>
</body>
</body>
</html>