<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	/**
	*@author PhucNT
	*/
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>YT-Schedule</title>
<link
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/lib/CSS/style.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/lib/calendar/fullcalendar.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/lib/CSS/jquery-ui.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath}/lib/calendar/fullcalendar.print.css"
	rel="stylesheet" media="print" />
<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.11.3.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/calendar/moment.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/calendar/fullcalendar.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/calendar/lang-all.js"></script>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/calendar/calendarCallback.js"></script>
	<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/Js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/lib/Js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/lib/CSS/bootstrap-multiselect.css" type="text/css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/lib/CSS/jquery-ui-timepicker-addon.css" type="text/css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/lib/Js/notify.js"></script>
<!--  <script type="text/javascript" src="${pageContext.request.contextPath}/lib/Js/notify.min.js"></script>-->
<script>
	$(document).ready(
			function() {
				
				selectable = true;
				 //$('#example-multiple-selected').multiselect();
				// list-group control
				$('.list-group a').click(function(e) {
			        e.preventDefault()
			        $that = $(this);
			        $that.parent().find('a').removeClass('active');
			        $that.addClass('active');
			        $('.dropdown-select').find('.dropdown-toggle').html('YTメンバー' + ' <span class="caret"></span>');
			        $('.dropdown-select').find('.dropdown-toggle').removeClass('btn-primary');
			        $('.dropdown-select').find('.dropdown-toggle').addClass('btn-default');
			    });
				// user dropdown control
				$('.dropdown-select').on( 'click', '.dropdown-menu li a', function() { 
					   var target = $(this).html();
					   //Adds active class to selected item
					   $(this).parents('.dropdown-menu').find('li').removeClass('active');
					   $(this).parent('li').addClass('active');

					   //Displays selected text on dropdown-toggle button
					   $(this).parents('.dropdown-select').find('.dropdown-toggle').html(target + ' <span class="caret"></span>');
					   $(this).parents('.dropdown-select').find('.dropdown-toggle').removeClass('btn-default');
					   $(this).parents('.dropdown-select').find('.dropdown-toggle').addClass('btn-primary');
					   $('.list-group a').removeClass('active');
					});
				
				// dialog for inputing event info
				$("#dialog").dialog({
					autoOpen : false,
					modal : true,
					width : 500,
					close : function(event, ui) {
						$(this).dialog("close");
						$("#cancelDialog").unbind("click");
						$("#submit").unbind("click");
						$("#deleteDialog").unbind("click");
					}
				});
				
				$('#startDate').datepicker({
					dateFormat : "yy-mm-dd",
				});
				$('#endDate').datepicker({
					dateFormat : "yy-mm-dd",
				});
				$('#startTime').timepicker({
					controlType: 'select',
					stepMinute: 15,
					oneLine: true,
					onSelect:function(){
					    $('#endTime').val($('#startTime').val());
					  }
				});

				$('#endTime').timepicker({
					controlType: 'select',
					stepMinute: 15,
					oneLine: true,
				});
				
				$('#member').multiselect({
					buttonWidth: '100%',
					includeSelectAllOption: true
				});
				
				var zone = "+09:00";
				var json_events;
				// fetching data
				$.ajax({
					url : '${pageContext.request.contextPath}/scheduleProcess',
					type : 'POST', // Send post data
					data : 'type=fetchAll&title=' + '' + '&eventId=' + "0"
							+ '&startdate=' + '' + '&enddate=' + '' + '&zone='
							+ zone,
					dataType : 'json',
					async : false,
					success : function(s) {
						json_events = s;
					}
				});
				$('#calendar').fullCalendar({
					eventDataTransform: function(event) {
						var copy = $.extend({}, event);
						if (copy.allDay) {
							copy.end = moment(copy.end).add(1, "day");
							//console.log("end:" + copy.end.format("YYYY-MM-DD"));
						}
						return copy;
					},
					events : json_events,
					//events: [{"id":"14","title":"New Event","start":"2017-01-01 T16:00:00+04:00","allDay":true}],
					timeFormat: 'HH:mm',
					lang: 'vi',
					utc : true,
					timezone : false,
					header : {
						left : 'prev,next today',
						center : 'title',
						right : 'month'
					},
					nextDayThreshold: "00:00:00",
					forceEventDuration: true,
					editable : true,
					droppable : true,
					slotDuration : '00:30:00',
					selectable : true,
					select : function(start, end, allDay) {
						// call js createEvent fucntion
						if (selectable) 
						createEvent(start, end, '${pageContext.request.contextPath}/scheduleProcess', "${sessionScope.userName}");
					},
					eventClick : function(event, jsEvent, view) {
						editEvent(event, '${pageContext.request.contextPath}/scheduleProcess');
					}
				})
			});
</script>
<style>
#calendar {
	max-width: 900px;
	margin: 0 auto;
}
</style>
</head>
<body>
	<jsp:include page="_header.jsp"></jsp:include>
	<p />
	<div class="container-fluid" style="width:90%;">
	<div class="row"> 
	<div class="well col-sm-3" style="margin-top:50px;">
		 <div class="list-group">
		  <a onclick="getJPVNFlight('${pageContext.request.contextPath}/scheduleProcess')" href="#" class="list-group-item active" style= "text-align:center;">Chiều Nhật⇨Việt</a>
		  <a onclick="getVNJPFlight('${pageContext.request.contextPath}/scheduleProcess')" href="#" class="list-group-item" style="text-align:center;">Chiều Việt⇨Nhật</a>
		</div>
		<!--  <div class="dropdown dropdown-select" style="margin-bottom:20px;">
		  <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"style="width:100%;height:40px;"> YTメンバー
		  <span class="caret">  </span></button>
		  <ul class="dropdown-menu" style="background-color:white">
		  	<c:forEach items="${listUserFull}" var="user">
				<li><a onclick="getIndividualSchedule('${pageContext.request.contextPath}/scheduleProcess', this.innerHTML)" href="#">${user.email}</a></li>
			</c:forEach>
		  </ul>
		</div>-->
	</div>
	<div class="col-sm-9">
		<div id='calendar'></div>
		<div style="" id="dialog" title="Thông tin chuyến bay">
			<div>
			<div class="form-horizontal">
				<div class="form-group">
				    <label class="control-label col-sm-3">Chiều bay:</label>
					<div class="col-sm-8">
						
					</div>
				</div>
				
				<hr style="margin:0px;padding:0px;">
				<!--  <div class="form-group">
					<label class="control-label col-sm-3">Xuất phát:</label>
						<div class="col-sm-8 checkbox">
					  		<label><input type="checkbox" id="allday" value=""></label>
						</div>
				</div>-->
				<div class="form-group">
					<label class="control-label col-sm-3">Xuất phát:</label>
					<div class="col-sm-4">
						<input type="text" id="startDate" name="ngayxuatphat" class="form-control input-sm">
					</div>
					<div class="col-sm-3">
						<input type="text" id="startTime" name="thoigianxuatphat" class="form-control input-sm">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-3">Đến:</label>
					<div class="col-sm-4">
						<input type="text" id="endDate" name="ngayden" class="form-control input-sm">
					</div>
					<div class="col-sm-3">
						<input type="text" id="endTime" name="thoigianden" class="form-control input-sm">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-3">Ghi chú:</label>
					<div class="col-sm-8">
						<input type="text" id="ghichu" name="ghichu" class="form-control input-sm" required>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-3">Tên hành khách:</label>
					<div class="col-sm-8">
						<input type="text" id="tenhanhkhach" name="tenhanhkhach" class="form-control input-sm">
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-sm-3">SĐT-Fb hành khách:</label>
					<div class="col-sm-8">
						<input type="text" id="thongtinhanhkhach" name="thongtinhanhkhach" class="form-control input-sm">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-8">
						<button id="submit" type="submit" class="btn btn-primary" style="width:80px;">OK</button>
						<button id="cancelDialog" type="submit" class="btn btn-default" style="float:right;">Cancel</button>
						<button id="deleteDialog" type="submit" class="btn btn-danger" style="float:right;margin-right:10px;">Delete</button>
					</div>
				</div>
			</div>
		</div>
		</div>
		</div>
		</div>
	</div>
	<jsp:include page="_footer.jsp"></jsp:include>
</body>
</html>