<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	/**
	*@author PhucNT
	*/
%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>Báo giá</title>

<link
	href="${pageContext.request.contextPath}/lib2/css/bootstrap.min.css"
	rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.contextPath}/lib2/css/londinium-theme.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/styles.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/icons.css"
	rel="stylesheet" type="text/css">


<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">

<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/sparkline.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uniform.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/select2.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputmask.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/autosize.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputlimit.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/listbox.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/multiselect.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/validate.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/tags.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/switch.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.full.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.queue.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/toolbar.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/daterangepicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fancybox.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/moment.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/jgrowl.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/datatables.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/tabletools.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/colorpicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fullcalendar.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/timepicker.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/application.js"></script>


<!-- OnPage script -->
<script>
	$(function() {

	});
</script>
<!-- /onPage script -->

</head>

<body class="full-width">
<jsp:include page="_header.jsp"></jsp:include>
	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id))
				return;
			js = d.createElement(s);
			js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7&appId=1365573416883375";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<!-- Page container -->
	<div class="page-container">

		<!-- Navbar -->
		<div class="navbar navbar-inverse" role="navigation">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"><img src="lib2/images/logo.png"
					alt="Japo Transport"></a>
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#navbar-icons">
					<span class="sr-only">Toggle navbar</span> <i class="icon-grid3"></i>
				</button>
				<button type="button" class="navbar-toggle offcanvas">
					<span class="sr-only">Toggle navigation</span> <i
						class="icon-paragraph-justify2"></i>
				</button>
			</div>

			<ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">

			</ul>
		</div>
		<!-- /navbar -->

		<!-- Page content -->
		<div class="page-content">

			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3>
						Bảng báo giá <small><strong>10/07/2016</strong></small><small>Ngày
							chuyển hàng có thể xem trên <a
							href="https://www.facebook.com/japo.ytasia">Fanpage Facebook</a>
						</small>
					</h3>
				</div>
			</div>
			<!-- /page header -->

			<!-- Quotation -->
			<div class="col-sm-6">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h6 class="panel-title">
							<i class="icon-coin"></i> Bảng báo giá
						</h6>
					</div>
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>#</th>
									<th>Tên sản phẩm</th>
									<th>Đơn vị</th>
									<th>Giá (¥)</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${products}" var="product">
									<tr>
										<td>${product.getTypeId()}</td>
										<td>${product.getTypeName()}</td>
										<td>${product.getUnit()}</td>
										<td><strong>${product.getPrice()}</strong></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- /quotation -->

			<!-- Other -->
			<div class="col-sm-6">
				<div class="panel panel-warning">
					<div class="panel-heading">
						<h6 class="panel-title">
							<i class="icon-marker"></i> Ghi chú
						</h6>
					</div>
					<div class="panel-body">
						<h5>
							<span class="text-warning">Giá bên trái dành cho các kiện
								hàng dưới 28 kg</span>
						</h5>
						<p>Với các hàng hóa giá trị khác vui lòng liên hệ bộ phận chăm
							sóc khách hàng</p>
						<div class="well">
							<p class="text-warning">
								Nhân viên chăm sóc khách hàng: <strong>Nguyễn Văn
									Trường</strong>
							</p>
							<p class="text-warning">Số điện thoại: 03-5332-5214</p>
							<p class="text-warning">
								Fanpage: <a href="https://www.facebook.com/japo.ytasia">facebook.com/japo.ytasia</a>
							</p>
						</div>
						<br>
						<ul class="square">
							<li><h6 class="text-danger">Phân loại loại hàng cồng kềnh:</h6>
								<ul>
									<li><span class="text-danger">Loại 1:</span> <strong>65</strong>cm
										< MAX(dài, rộng, cao) < <strong>100</strong>cm</span></li>
									<li><span class="text-danger">Loại 2:</span> MAX(dài, cao,
										rộng) <= <strong>65</strong>cm</span></li>
								</ul></li>
						</ul>
						<br>
						<div class="well">
							<h6 class="text-danger">Công thức quy đổi Thể Tích -> Khối
								Lượng:</h6>
							<h5 style="color: gray">
								Khối lượng <span class="text-danger">(kg)</span> = [dài x rộng x
								cao] <span class="text-danger">(cm)</span> / 5000
							</h5>
						</div>
						<br>
						<ul class="square">
							<li><h6 class="text-danger">Chú ý:</h6>
								<ul>
									<li>Việc thanh toán/giao dịch tính trên đơn vị tiền tệ
										JPY.</li>
									<li>Với các khách hàng trả bằng tiền Việt, sẽ được quy đổi
										ra số tiền VND tương ứng từ tiền JPY theo tỷ giá bán ra của
										ngân hàng VCB thời điểm ngày thực hiện giao dịch thanh toán.</li>
								</ul></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- /other -->

			<div class="col-sm-6">
				<div class="fb-comments" data-href="http://transport.japo.com.vn/"
					data-numposts="10"></div>
			</div>

			<!-- /page content -->
		</div>
	</div>
<jsp:include page="_footer.jsp"></jsp:include>
</body>
</html>