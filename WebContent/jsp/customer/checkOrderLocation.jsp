<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	/**
	*@author PhucNT
	*/
%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>New Order</title>

<link
	href="${pageContext.request.contextPath}/lib2/css/bootstrap.min.css"
	rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.contextPath}/lib2/css/londinium-theme.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/styles.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/icons.css"
	rel="stylesheet" type="text/css">


<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">

<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/sparkline.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uniform.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/select2.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputmask.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/autosize.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputlimit.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/listbox.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/multiselect.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/validate.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/tags.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/switch.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.full.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.queue.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/toolbar.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/daterangepicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fancybox.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/moment.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/jgrowl.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/datatables.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/tabletools.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/colorpicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fullcalendar.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/timepicker.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/application.js"></script>


<!-- YTAsia script -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/ytasia/check_location.js"></script>
<!-- /YTAsia script -->


<!-- OnPage script -->
<script>
	$(function() {
		/**
		 * Show locations of order
		 */
		$("button#check")
				.click(
						function() {
							clear();
							var id = $('#order-id').val();
							$
									.ajax({
										url : '${pageContext.request.contextPath}/getOrderLocationInfo',
										type : "post",
										dataType : "json",
										data : {
											id : id
										},
										success : function(result) {
											$('#s-table').text("");
											//drawGraphic(result);
											if (result.length < 1) {
												$("#message").text("Please enter the valid order id");
											} else {
												$("#message").text("");
												drawTable(result);
											}
										}
									});
						});

		function clear() {
			$('#locations').text('');
		}
	});
</script>
<!-- /onPage script -->

</head>

<body class="full-width">
	<!-- Page container -->
	<div class="page-container">

		<!-- Navbar -->
		<div class="navbar navbar-inverse" role="navigation">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"><img src="lib2/images/logo.png"
					alt="Londinium"></a>
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#navbar-icons">
					<span class="sr-only">Toggle navbar</span> <i class="icon-grid3"></i>
				</button>
				<button type="button" class="navbar-toggle offcanvas">
					<span class="sr-only">Toggle navigation</span> <i
						class="icon-paragraph-justify2"></i>
				</button>
			</div>

			<ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">

			</ul>
		</div>
		<!-- /navbar -->

		<!-- Page content -->
		<div class="page-content">

			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3>
						Check order location <small>Check your order location
							create by Transport Order by YTAsia</small>
					</h3>
				</div>
			</div>
			<!-- /page header -->

			<!-- Id check box -->
			<div class="col-sm-5">
				<div class="panel panel-info">
					<div class="panel-heading">
						<h6 class="panel-title">Enter your order id</h6>
						<h6 id="panel-order-below" class="panel-title text-danger"></h6>
					</div>
					<div class="panel-body">
						<div class="col-sm-6">
							<input type="text" id="order-id" class="form-control input-sm"
								placeholder="your order id" required>
						</div>
						<div class="col-sm-6">
							<!-- <button type="button" id="check" class="btn btn-success btn-xs"
								data-toggle="modal" href="#location-popup">Check</button> -->
							<button type="button" id="check" class="btn btn-success btn-xs">Check</button>
						</div>
						<div class="col-sm-6">
							<span class="text-danger" id="message"></span>
						</div>
					</div>
				</div>
			</div>
			<!-- /id check box -->

			<!-- Location result -->
			<div class="col-sm-7">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h6 class="panel-title">Order's location result</h6>
						<h6 id="panel-order-below" class="panel-title text-danger"></h6>
					</div>
					<div class="table-responsive">
						<table class="table table-striped ">
							<thead>
								<tr>
									<th>Time</th>
									<th>Location</th>
								</tr>
							</thead>
							<tbody id="s-table">

							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- /location result -->

			<!-- /page content -->
		</div>
	</div>


	<!-- Location info (graphic design)-->
	<!-- <div id="location-popup" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Your order location's status</h4>
				</div>

				<div class="panel">
					<div class="panel-body">
						<ul class="info-blocks" id="locations">
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div> -->
	<!-- /location info -->
</body>
</html>