<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
	/**
	*@author PhucNT
	*/
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<!-- Navbar -->
	<div class="navbar navbar-inverse" role="navigation">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#navbar-icons">
				<span class="sr-only">Toggle right icons</span> <i class="icon-grid"></i>
			</button>
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#navbar-menu">
				<span class="sr-only">Toggle menu</span> <i
					class="icon-paragraph-justify2"></i>
			</button>
			<a class="navbar-brand" href="#"><img src="lib2/images/logo.png"
				alt="Londinium"></a>

		</div>

		<ul class="nav navbar-nav collapse" id="navbar-menu">
			<li><a href="${pageContext.request.contextPath}/adminMainPage"><span>Order</span>
					<i class="icon-database"></i></a></li>
			<li><a href="${pageContext.request.contextPath}/adminUserPage"><span>User</span>
					<i class="icon-user"></i></a></li>
			<li><a
				href="${pageContext.request.contextPath}/adminCustomerPage"><span>Customer</span>
					<i class="icon-people"></i></a></li>
			<li><a
				href="${pageContext.request.contextPath}/adminCatalogPage"><span>Catalog</span>
					<i class="icon-tags2"></i></a></li>
			<li class="dropdown"><a href="#" class="dropdown-toggle"
				data-toggle="dropdown"><i class="icon-history"></i> <span>History</span>
					<b class="caret"></b></a>
				<ul class="dropdown-menu dropdown-menu-right icons-right">
					<li><a
						href="${pageContext.request.contextPath}/adminStatusHistory">Status
							History<i class="icon-bubble-notification2"></i>
					</a></li>
					<li><a
						href="${pageContext.request.contextPath}/adminLocationHistory">Location
							History<i class="icon-location2"></i>
					</a></li>
				</ul></li>
			<li><a
				href="${pageContext.request.contextPath}/adminShipmentPage"><span>Shipment</span>
					<i class="icon-airplane2"></i></a></li>
			<li class="dropdown"><a href="#" class="dropdown-toggle"
				data-toggle="dropdown"><i class="icon-pie2"></i> <span>Statistic</span>
					<b class="caret"></b></a>
				<ul class="dropdown-menu dropdown-menu-right icons-right">
					<li><a
						href="${pageContext.request.contextPath}/adminOrderStatisticPage">Order<i
							class="icon-database"></i></a></li>
					<li><a
						href="${pageContext.request.contextPath}/adminCustomerStatisticPage">Customer<i
							class="icon-people"></i></a></li>
					<li><a
						href="${pageContext.request.contextPath}/adminEmployeeStatisticPage">Employee<i
							class="icon-user"></i></a></li>
				</ul></li>
		</ul>

		<ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">
			<li class="user dropdown"><a class="dropdown-toggle"
				data-toggle="dropdown"> <span>${sessionScope.userName}</span> <i
					class="caret"></i>
			</a>
				<ul class="dropdown-menu dropdown-menu-right icons-right">
					<li><a href="#"><i class="icon-user"></i> Profile</a></li>
					<li><a href="${pageContext.request.contextPath}/userMainPage"><i
							class="icon-smiley"></i> User Mode</a></li>
					<li><a href="${pageContext.request.contextPath}/logout"><i
							class="icon-exit"></i> Logout</a></li>
				</ul></li>
		</ul>
	</div>
	<!-- /navbar -->
</body>
</html>