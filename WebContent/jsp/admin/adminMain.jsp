<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	/**
	*@author PhucNT
	*/
%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>Admin Main</title>

<link
	href="${pageContext.request.contextPath}/lib2/css/bootstrap.min.css"
	rel="stylesheet" media="all" type="text/css">
<link
	href="${pageContext.request.contextPath}/lib2/css/londinium-theme.css"
	rel="stylesheet" media="all" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/styles.css"
	rel="stylesheet" media="all" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/icons.css"
	rel="stylesheet" media="all" type="text/css">
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext"
	rel="stylesheet" media="all" type="text/css">

<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/sparkline.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uniform.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/select2.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputmask.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/autosize.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputlimit.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/listbox.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/multiselect.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/validate.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/tags.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/switch.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.full.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.queue.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/toolbar.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/daterangepicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fancybox.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/moment.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/jgrowl.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/datatables.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/tabletools.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/colorpicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fullcalendar.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/timepicker.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/application.js"></script>


<!-- OnPage script -->
<script>
	$(function() {
		$('#sort').click();

		//--------------- PRINT ----------------//

		/**
		 * Button print (invoice) on click 
		 */
		$("#btnPrint").click(function() {
			printElement(document.getElementById("printThis"));

			window.print();
		});

		/**
		 * Button print (invoice) function 
		 */
		function printElement(elem) {
			var domClone = elem.cloneNode(true);

			var $printSection = document.getElementById("printSection");

			if (!$printSection) {
				var $printSection = document.createElement("div");
				$printSection.id = "printSection";
				document.body.appendChild($printSection);
			}

			$printSection.innerHTML = "";

			$printSection.appendChild(domClone);
		}

		//--------------- VIEW INVOICE ----------------//

		/**
		 * Show invoice-popup when OrderId clicked
		 */
		$(document).on('click', 'a#o-invoice', function() {
			clearData();

			var $id = $(this).data("id");
			$.ajax({
				url : '${pageContext.request.contextPath}/viewInvoice',
				type : "post",
				dataType : "json",
				data : {
					orderId : $id
				},
				success : function(result) {
					var detail = result[0], products = result[1];
					setDetail(detail);
					setProducts(products)
				}
			});
		});

		/**
		 * Set data for detail-popup when popup showed
		 */
		function setDetail(detail) {
			$('#d-sender-name').text(detail.senderName);
			$('#d-sender-email').text(detail.senderEmail);
			$('#d-sender-address').text(detail.senderAddress);
			$('#d-sender-phone').text(detail.senderPhone);

			$('#d-receipter-name').text(detail.receipterName);
			$('#d-receipter-email').text(detail.receipterEmail);
			$('#d-receipter-address').text(detail.receipterAddress);
			$('#d-receipter-phone').text(detail.receipterPhone);

			$('#d-order-id').text(detail.fId);
			$('#d-order-time').text(detail.time);
			$('#d-responsible').text(detail.user);
			$('#d-other').text(detail.other);

			switch (detail.paidStatus) {
			case 0:
				$('#d-paid-status').text('Unpaid');
				$("#d-paid-status").attr('class',
						'label label-danger pull-right');
				break;

			case 1:
				$('#d-paid-status').text('Paid');
				$("#d-paid-status").attr('class',
						'label label-success pull-right');
				break;
			}
		}

		/**
		 * Set data for invoice-popup when popup showed
		 */
		function setProducts(products) {
			var subtotal = 0;
			var tax = 0;
			var total = 0;

			for (i = 0; i < products.length; i++) {
				$('#d-product-table').append(
						'<tr><td>' + products[i].type + '</td><td>'
								+ products[i].name + '</td><td>'
								+ products[i].unitPrice + '</td><td>'
								+ products[i].quantity + '</td><td><strong>'
								+ products[i].price + '</strong></td></tr>');
				subtotal = subtotal + products[i].price;
			}
			total = subtotal + (subtotal / 100 * tax);
			$('#d-subtotal').text(subtotal + ' jpy');
			$('#d-tax').text(tax + '%');
			$('#d-total').text(total + ' jpy');
		}

		/**
		 * Clear data for detail-popup when popup closed
		 */
		function clearData() {
			$('#d-sender-name').text("");
			$('#d-sender-email').text("");
			$('#d-sender-address').text("");
			$('#d-sender-phone').text("");

			$('#d-receipter-name').text("");
			$('#d-receipter-email').text("");
			$('#d-receipter-address').text("");
			$('#d-receipter-phone').text("");

			$('#d-order-id').text("");
			$('#d-order-time').text("");
			$('#d-responsible').text("");
			$('#d-paid-status').text("");
			$('#d-other').text("");

			$('#d-product-table').text('');
		}

		//--------------- CONFIRMATION ----------------//

		/**
		 * Confirmation when delete button clicked
		 */

		$('.confirmation').on('click', function() {
			console.log("delete");
			return confirm('Are you sure to delete this order?');
		})

		//--------------- PAY (CHANGE PAID STATUS) ----------------//

		/**
		 * Pay
		 */
		$(document).on(
				'click',
				'button#btnPay',
				function() {
					id = $('#d-order-id').text();
					var status = $("#" + id + " td:nth-child(5)").text();

					$.ajax({
						url : '${pageContext.request.contextPath}/payOrder',
						type : "post",
						dateType : "json",
						data : {
							id : id
						},
						success : function(result) {
							$('#d-paid-status').text('Paid');
							$("#d-paid-status").attr('class',
									'label label-success pull-right');
							$("#" + id + " td:nth-child(5)").text("");
							$("#" + id + " td:nth-child(5)").append(
									"<strong>Paid</strong>");
							$("#" + id + " td:nth-child(5)").attr('class',
									'text-success');
						}
					});

				});

	})
</script>
<!-- /onPage script -->

</head>

<body class="full-width">
	<!-- Page container -->
	<div class="page-container">

		<!-- Navbar -->
		<jsp:include page="_adminHeader.jsp"></jsp:include>


		<!-- Page content -->
		<div class="page-content">

			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3>
						Orders table <small>All Transport orders of YTAsia</small>
					</h3>
				</div>
			</div>
			<!-- /page header -->

			<!-- Page body -->
			<!-- Orders -->
			<div class="col-sm-7">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h6 class="panel-title">
							<i class="icon-paragraph-justify"></i> Orders
						</h6>

						<ul class="breadcrumb-buttons collapse">
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown"><i class="icon-search3"></i> <span>Search</span>
									<b class="caret"></b></a>
								<div class="popup dropdown-menu dropdown-menu-right">
									<form
										action="${pageContext.request.contextPath}/adminSearchOrderBy"
										method="post" class="breadcrumb-search">
										<div class="row">
											<div class="col-xs-2">
												<select data-placeholder="Shipment" class="clear-results"
													name="searchShipment" tabindex="2">
													<option value=""></option>
													<c:forEach items="${listShipment}" var="shipment">
														<option value="${shipment.getShipmentId()}">${shipment.getDepartTime()}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-2">
												<select data-placeholder="Status" class="clear-results"
													tabindex="2" name="searchStatus">
													<option value=""></option>
													<c:forEach items="${listStatus}" var="status">
														<option value="${status.getStatusId()}">${status.getStatusName()}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<input type="submit" class="btn btn-block btn-success"
											value="Search">
									</form>
								</div></li>
							<li class="dropdown"><a
								href="${pageContext.request.contextPath}/userMainPage" id="all"
								style="float: left">All</a></li>
						</ul>
					</div>

					<div class="datatable">
						<table class="table table-striped">
							<thead>
								<tr>
									<th id="sort">#</th>
									<th>Order ID</th>
									<th>Status</th>
									<th>Location</th>
									<th>Paid</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${orders}" var="order">
									<tr id="${order.getTransportOrderId()}">
										<td>${order.getId()}</td>
										<td>${order.getTransportOrderId()}</td>
										<td>${order.getStatusId()}</td>
										<td>${order.getLocationId()}</td>
										<td><c:if test="${order.getPaidStatus() == '0'}">
												<span class="text-danger"><strong>Unpaid</strong></span>
											</c:if> <c:if test="${order.getPaidStatus() == '1'}">
												<span class="text-success"><strong>Paid</strong></span>
											</c:if></td>
										<td><a id="o-invoice"
											data-id="${order.getTransportOrderId()}" data-toggle="modal"
											href="#invoice-popup"
											class="btn btn-success btn-xs icon-coin"></a> <c:if
												test="${order.getStatusId()== 'DET'}">
												<a
													href="${pageContext.request.contextPath}/userPutBackOrder?&id=${order.getTransportOrderId()}"
													class="btn btn-success btn-xs">Hồi sinh</a>
											</c:if> <a
											href="${pageContext.request.contextPath}/adminDeleteOrder?id=${order.getTransportOrderId()}"
											class="btn btn-danger btn-xs confirmation icon-remove3"></a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- /orders -->

			<!-- /page body -->

		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->

	<!-- Invoice popup -->
	<div id="invoice-popup" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Modal title</h4>
				</div>

				<!-- New invoice template -->
				<div id="printThis" class="panel">
					<div class="panel-body">

						<div class="row invoice-header">
							<div class="col-sm-6">
								<h3>YTAsia</h3>
								<span>Best Japan - VietNam transport service ever !</span>
							</div>

							<div class="col-sm-6">
								<ul class="invoice-details">
									<li><h6>
											Invoice # <strong id="d-order-id" class="text-danger"></strong>
										</h6></li>
									<li>Date of Invoice: <strong id="d-order-time"></strong></li>
								</ul>
							</div>
						</div>


						<div class="row">
							<div class="col-sm-3">
								<h6>Sender:</h6>
								<ul>
									<li>Name:<strong id="d-sender-name" class="pull-right"></strong></li>
									<li>Email:<strong id="d-sender-email" class="pull-right"></strong></li>
									<li>Address:<strong id="d-sender-address"
										class="pull-right"></strong></li>
									<li>Phone:<strong id="d-sender-phone" class="pull-right"></strong></li>
								</ul>
							</div>

							<div class="col-sm-1"></div>

							<div class="col-sm-3">
								<h6>Recipient:</h6>
								<ul>
									<li>Name:<strong id="d-receipter-name" class="pull-right"></strong></li>
									<li>Email:<strong id="d-receipter-email"
										class="pull-right"></strong></li>
									<li>Address:<strong id="d-receipter-address"
										class="pull-right"></strong></li>
									<li>Phone:<strong id="d-receipter-phone"
										class="pull-right"></strong></li>
								</ul>
							</div>

							<div class="col-sm-1"></div>

							<div class="col-sm-4">
								<h6>Invoice Details:</h6>
								<ul>
									<li>Responsible: <a href="#" class="pull-right"
										id="d-responsible"></a></li>
									<li class="invoice-status"><strong>Current
											status: <span id="d-paid-status"
											class="label label-danger pull-right">Unpaid</span>
									</strong></li>
								</ul>
							</div>
						</div>
					</div>

					<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>Type</th>
									<th>Description</th>
									<th>Unit price</th>
									<th>Quantity</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody id="d-product-table">

							</tbody>
						</table>
					</div>

					<div class="panel-body">
						<div class="col-sm-8">
							<h6>Detail:</h6>
							<textarea id="d-other" style="border: none; min-height: 200px"
								class="form-control"></textarea>
						</div>

						<div class="col-sm-4">
							<h6>Total:</h6>
							<table class="table">
								<tbody>
									<tr>
										<th>Subtotal:</th>
										<td id="d-subtotal" class="text-right"></td>
									</tr>
									<tr>
										<th>Tax:</th>
										<td id="d-tax" class="text-right"></td>
									</tr>
									<tr>
										<th>Total:</th>
										<td id="d-total" class="text-right text-danger"><h6></h6></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- /new invoice template -->
			</div>
			<div class="btn-group pull-left">
				<button type="button" id="btnPay" class="btn btn-success">
					<i class="icon-checkbox-partial"></i> Confirm payment
				</button>
			</div>
			<div class="btn-group pull-right">
				<button type="button" id="btnPrint" class="btn btn-info">
					<i class="icon-print2"></i> Print
				</button>
			</div>
		</div>
	</div>
	<!-- /invoice popup -->

</body>
</html>