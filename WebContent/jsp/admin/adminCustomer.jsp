<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	/**
	*@author PhucNT
	*/
%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>Customer</title>

<link
	href="${pageContext.request.contextPath}/lib2/css/bootstrap.min.css"
	rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.contextPath}/lib2/css/londinium-theme.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/styles.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/icons.css"
	rel="stylesheet" type="text/css">
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">

<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/sparkline.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.orderbars.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.pie.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.time.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.animator.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/excanvas.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.resize.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uniform.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/select2.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputmask.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/autosize.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputlimit.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/listbox.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/multiselect.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/validate.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/tags.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/switch.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.full.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.queue.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/toolbar.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/daterangepicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fancybox.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/moment.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/jgrowl.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/datatables.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/tabletools.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/colorpicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fullcalendar.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/timepicker.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/application.js"></script>


<!-- YTAsia script -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/ytasia/statistic_customer.js"></script>
<!-- /YTAsia script -->


<!-- OnPage script -->
<script type="text/javascript">
	$(function() {
		$('#sort').click();

		/**
		 * Customer classify
		 */
		$.ajax({
			url : '${pageContext.request.contextPath}/getClassifyCustomer',
			type : "post",
			dataType : "json",
			data : {},
			success : function(result) {
				var data = result[0], detail = result[1];
				var classifyData = [];

				$('#c-total').text(detail.total);

				for (i = 0; i < data.length; i++) {
					var type = data[i].type;
					var number = data[i].number;

					switch (type) {
					case 1:
						classifyData[i] = {
							label : 'Khách thường',
							data : number
						}
						$('#c-normal').text(number);
						break;
					case 2:
						classifyData[i] = {
							label : 'Khách quen 1',
							data : number
						}
						$('#c-discount').text(number);
						break;
					case 3:
						classifyData[i] = {
							label : 'Khách quen 2',
							data : number
						}
						$('#c-loyal').text(number);
						break;
					}
				}

				setCustomerClassifyData(classifyData)
			}
		});

		/**
		 * Confirmation when delete button clicked
		 */
		$('.confirmation').on('click', function() {
			console.log("delete");
			return confirm('Are you sure to delete this customer?');
		})

		/**
		 * Show new customer popup
		 */
		$("#new-popup").dialog({
			autoOpen : false,
			open : function(event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
				$(".ui-dialog-titlebar").hide();
			},
			modal : true,
			width : 600
		});

		/**
		 * Show edit customer popup
		 */
		$("#edit-popup").dialog({
			autoOpen : false,
			open : function(event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
				$(".ui-dialog-titlebar").hide();
			},
			modal : true,
			width : 600
		});

		/* $("a#new-customer").click(function(e) {
			e.preventDefault();
			$("#user-radio").prop("checked", true);
			$("#new-popup").dialog("open");
		}); */

		/**
		 * Close new-popup when 'Cancel button' clicked
		 */
		$("button#n-cancel").click(function(e) {
			e.preventDefault();
			$("#new-popup").dialog("close");
		});

		/**
		 * Close edit-popup when 'Cancel button' clicked
		 */
		$("button#e-cancel").click(function(e) {
			e.preventDefault();
			$("#edit-popup").dialog("close");
		});

		/**
		 * Open edit user popup
		 */
		$(document)
				.on(
						'click',
						"button#edit",
						function() {
							var id = $(this).val();

							clearData();

							$
									.ajax({
										url : '${pageContext.request.contextPath}/adminGetCustomerInfo',
										type : "post",
										dataType : "json",
										data : {
											editId : id
										},
										success : function(result) {
											setData(result)
										}
									});
						});

		/**
		 * Set data for edit-popup when popup is show
		 */
		function setData(result) {
			$('#edit-bt').val(result.id);
			$('#e-email').val(result.email);
			$('#e-name').val(result.name);
			$('#e-address').val(result.address);
			$('#e-phone').val(result.phone);
			$('#e-type').val(result.type_id);
		}

		/**
		 * Clear data for edit-popup when popup is close
		 */
		function clearData() {
			$('#e-email').val('');
			$('#e-name').val('');
			$('#e-address').val('');
			$('#e-phone').val('');
			$('#e-type').val('');
		}

		function test(id) {
			$('#e-email').val('adfadfsdf');
		}
	});
</script>
<!-- /onPage script -->


</head>

<body class="full-width">
	<!-- Page container -->
	<div class="page-container">

		<!-- Navbar -->
		<jsp:include page="_adminHeader.jsp"></jsp:include>


		<!-- Page content -->
		<div class="page-content">

			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3 id="h3">
						Customer <small>Manage customer of YTAsia</small>
					</h3>
				</div>
			</div>
			<!-- /page header -->

			<!-- Page body -->
			<div class="col-xs-8">
				<div class="panel panel-default">
					<!-- Table header -->
					<div class="panel-heading">
						<ul class="breadcrumb-buttons collapse">
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown"><i class="icon-search3"></i> <span>Search</span>
									<b class="caret"></b></a>
								<div class="popup dropdown-menu dropdown-menu-right">
									<div class="popup-header">
										<span>Quick search</span>
									</div>
									<form method="POST"
										action="${pageContext.request.contextPath}/adminSearchCustomerBy"
										class="breadcrumb-search">

										<div class="row">
											<label class="control-label col-sm-2">Type:</label>
											<div class="col-sm-9">
												<select class="form-control input-sm " name="customerType"
													tabindex="5">
													<option value=""></option>
													<c:forEach items="${types}" var="type">
														<option value="${type.getId()}">${type.getType()}</option>
													</c:forEach>
												</select>
											</div>
										</div>

										<input type="submit" class="btn btn-block btn-success"
											value="Search">
									</form>
								</div></li>
							<li class="dropdown"><a
								href="${pageContext.request.contextPath}/adminCustomerPage"
								id="all" style="float: left">All</a></li>
							<li class="dropdown"><a data-toggle="modal" role="button"
								href="#new_customer_popup" id="new-customer"><i
									class="icon-user-plus3"></i>New</a></li>
						</ul>
					</div>
					<!-- /table header -->

					<!-- Data Table -->
					<div class="datatable">
						<table class="table table-striped">
							<thead>
								<tr>
									<th id="sort">#id</th>
									<th>Name</th>
									<th>Email</th>
									<th>Address</th>
									<th>Phone</th>
									<th>Type</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${customers}" var="customer">
									<tr>
										<td><a href="#">${customer.getCustomerId()}</a></td>
										<td>${customer.getName()}</td>
										<td>${customer.getEmail()}</td>
										<td>${customer.getAdress()}</td>
										<td>${customer.getPhone()}</td>
										<td>${customer.getType()}</td>
										<td><button data-toggle="modal"
												href="#edit_customer_popup" id="edit"
												class="btn btn-xs btn-info"
												value="${customer.getCustomerId()}">Edit</button></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<!-- /data table -->
				</div>
			</div>

			<div class="col-md-4">

				<!-- Customer classify -->
				<div class="panel panel-default">
					<div class="panel-heading">
						<h6 class="panel-title">
							<i class="icon-calendar2"></i> Customer classify
						</h6>
					</div>
					<div class="panel-body">
						<div class="graph-standard" id="classify_chart"></div>
					</div>
				</div>
				<!-- /customer classify -->

				<!-- By year statistics detail -->
				<h6>Details:</h6>
				<div class="col-md-6">

					<ul>
						<li>Total: <strong id="c-total" class="">379</strong> người
						</li>
						<li>Khách thường: <strong id="c-normal" class="">379</strong>
							người
						</li>
						<li>Khách quen 1: <strong id="c-discount" class="">379</strong>
							người
						</li>
						<li>Khách quen 2: <strong id="c-loyal" class="">379</strong>
							người
						</li>
					</ul>
				</div>
				<!-- /by year statistics detail -->

			</div>

			<!-- /page body -->
		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->


	<!-- Edit popup -->
	<div id="edit_customer_popup" class="modal fade" tabindex="-1"
		role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit Customer</h4>
				</div>
				<form class="form-horizontal" method="post"
					action="${pageContext.request.contextPath}/adminEditCustomer">
					<div class="modal-body with-padding">

						<div class="form-group row">
							<div class="col-sm-6">
								<label>Email:</label> <input type="text"
									name="editCustomerEmail" id="e-email"
									class="form-control input-sm">
							</div>
							<div class="col-sm-6">
								<label>Name:</label> <input type="text" name="editCustomerName"
									id="e-name" class="form-control input-sm">
							</div>
						</div>

						<div class="form-group row">
							<div class="col-sm-6">
								<label>Type:</label> <select class="form-control input-sm "
									name="editCustomerType" id="e-type" tabindex="5">
									<c:forEach items="${types}" var="type">
										<option value="${type.getId()}">${type.getType()}</option>
									</c:forEach>
								</select>
							</div>
							<div class="col-sm-6">
								<label>Phone:</label> <input type="text"
									name="editCustomerPhone" id="e-phone"
									class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-sm-12">
								<label>Address:</label> <input type="text"
									name="editCustomerAddress" id="e-address"
									class="form-control input-sm">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary" id="edit-bt"
							name="editCustomerId">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /edit popup -->

	<!-- New popup -->
	<div id="new_customer_popup" class="modal fade" tabindex="-1"
		role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<i class="icon-paragraph-justify2"></i> New Customer
					</h4>
				</div>

				<!-- Form inside modal -->
				<form class="form-horizontal" method="post"
					action="${pageContext.request.contextPath}/adminAddCustomer">

					<div class="modal-body with-padding">

						<div class="form-group row">
							<div class="col-sm-6">
								<label>Email:</label> <input type="text" name="newCustomerEmail"
									id="n-email" class="form-control input-sm">
							</div>
							<div class="col-sm-6">
								<label>Name:</label> <input type="text" name="newCustomerName"
									id="n-name" class="form-control input-sm">
							</div>
						</div>

						<div class="form-group row">
							<div class="col-sm-6">
								<label>Phone:</label> <input type="text" name="newCustomerPhone"
									id="n-phone" class="form-control input-sm">
							</div>
							<div class="col-sm-6">
								<label>Type:</label> <select class="form-control input-sm "
									name="newCustomerType" id="n-type" tabindex="5">
									<c:forEach items="${types}" var="type">
										<option value="${type.getId()}">${type.getType()}</option>
									</c:forEach>
								</select>
							</div>
						</div>

						<div class="form-group row">
							<div class="col-sm-12">
								<label>Address:</label> <input type="text"
									name="newCustomerAddress" id="n-address"
									class="form-control input-sm">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>

				</form>
			</div>
		</div>
	</div>
	<!-- /new popup -->



</body>
</html>