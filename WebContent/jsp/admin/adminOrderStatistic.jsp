<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	/**
	*@author PhucNT
	*/
%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>Admin Statistic</title>

<link
	href="${pageContext.request.contextPath}/lib2/css/bootstrap.min.css"
	rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.contextPath}/lib2/css/londinium-theme.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/styles.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/icons.css"
	rel="stylesheet" type="text/css">

<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">

<link rel="stylesheet"
	href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css">
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/sparkline.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.orderbars.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.pie.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.time.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.animator.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/excanvas.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.resize.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/sparkline.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uniform.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/select2.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputmask.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/autosize.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputlimit.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/listbox.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/multiselect.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/validate.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/tags.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/switch.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.full.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.queue.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/toolbar.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/daterangepicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fancybox.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/moment.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/jgrowl.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/datatables.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/tabletools.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/colorpicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fullcalendar.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/timepicker.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/application.js"></script>


<!-- YTAsia script -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/ytasia/statistic_order.js"></script>
<!-- /YTAsia script -->


<!-- OnPage script -->
<script type="text/javascript">
	$(function() {

		var currentTime = new Date()
		var current_month = currentTime.getMonth() + 1;
		var current_year = currentTime.getFullYear();

		$('#select-month').val(current_month);
		$('#select-year').val(current_year);

		setMonthStatistic(current_month, current_year);
		setYearStatistic(current_year);

		/**
		 * On time change
		 */
		$("#select-month").change(function() {
			var _month = $("#select-month").val();
			var _year = $("#select-year").val();

			setMonthStatistic(_month, _year);
		}).change();

		$("#select-year").change(function() {
			var _month = $("#select-month").val();
			var _year = $("#select-year").val();

			setMonthStatistic(_month, _year);
			setYearStatistic(_year);
		}).change();

		/**
		 * By month statistic
		 */
		function setMonthStatistic(month, year) {
			$.ajax({
				url : '${pageContext.request.contextPath}/getOrderByMonth',
				type : "post",
				dataType : "json",
				data : {
					month : month,
					year : year
				},
				success : function(result) {
					var data = result[0], detail = result[1];

					var byMonthData = [];

					for (i = 0; i < data.length; i++) {
						byMonthData.push([ data[i].day, data[i].number ]);
					}
					setByMonthData(byMonthData);

					$("#s-m-total").text(detail.total);
					$("#s-m-average").text(detail.average);

				}
			});
		}
		/**
		 * By year statistic 
		 */
		function setYearStatistic(year) {
			$.ajax({
				url : '${pageContext.request.contextPath}/getOrderByYear',
				type : "post",
				dataType : "json",
				data : {
					year : year
				},
				success : function(result) {
					var data = result[0], detail = result[1];

					var yearData = [];
					var year = detail.year;

					for (i = 0; i < data.length; i++) {
						yearData.push([ data[i].month, data[i].number ]);
					}
					setByYearData(year, yearData);

					$("#s-y-total").text(detail.total);
					$("#s-y-average").text(detail.average);
				}
			});
		}
	});
</script>
<!-- /onPage script -->

</head>

<body class="full-width">
	<!-- Page container -->
	<div class="page-container">

		<!-- Navbar -->
		<jsp:include page="_adminHeader.jsp"></jsp:include>


		<!-- Page content -->
		<div class="page-content">

			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3>
						Order Statistic <small>All order of YTAsia</small>
					</h3>

				</div>
			</div>
			<!-- /page header -->

			<!-- Breadcrumbs line -->
			<div class="breadcrumb-line">
				<div class="col-md-2"></div>

				<div class="col-md-8">
					<div class="breadcrumb">
						<label>Month </label> <select tabindex="" id="select-month">
							<c:forEach items="${months}" var="month">
								<option value="${month}">${month}</option>
							</c:forEach>
						</select>
					</div>

					<div class="breadcrumb">
						<label>Year </label> <select tabindex="" id="select-year">
							<c:forEach items="${years}" var="year">
								<option value="${year}">${year}</option>
							</c:forEach>
						</select>
					</div>
				</div>

			</div>


			<!-- Page body -->
			<div class="col-md-6">

				<!-- By month statistics -->
				<div class="panel panel-warning">
					<div class="panel-heading">
						<h6 class="panel-title">
							<i class="icon-calendar2"></i> Monthly statistic
						</h6>
					</div>
					<div class="panel-body">
						<div class="graph-standard" id="by_month_chart"></div>
					</div>
				</div>
				<!-- /by year statistics -->

				<!-- By month statistics detail -->
				<h6>Details:</h6>
				<div class="col-md-6">

					<ul>
						<li>Total: <strong id="s-m-total" class="">379</strong>
							orders
						</li>
						<li>Average: <strong id="s-m-average" class="">379</strong>
							orders/day
						</li>
					</ul>
				</div>
				<div class="col-md-0"></div>
				<div class="col-md-6">
					<!-- <ul>
						<li>Max: <strong id="s-m-max" class="">379</strong></li>
						<li>Min: <strong id="s-m-min" class="">379</strong></a></li>
					</ul> -->
				</div>
				<!-- /by month statistics detail -->

			</div>

			<div class="col-md-6">

				<!-- By year statistics -->
				<div class="panel panel-warning">
					<div class="panel-heading">
						<h6 class="panel-title">
							<i class="icon-calendar2"></i> Annually statistic
						</h6>
					</div>
					<div class="panel-body">
						<div class="graph-standard" id="by_year_chart"></div>
					</div>
				</div>
				<!-- /by year statistics -->

				<!-- By year statistics detail -->
				<h6>Details:</h6>
				<div class="col-md-6">

					<ul>
						<li>Total: <strong id="s-y-total" class="">379</strong>
							orders
						</li>
						<li>Average: <strong id="s-y-average" class="">379</strong>
							orders/month
						</li>
					</ul>
				</div>
				<div class="col-md-0"></div>
				<div class="col-md-6">
					<!-- <ul>
						<li>Max: <strong id="s-m-max" class="">379</strong></li>
						<li>Min: <strong id="s-m-min" class="">379</strong></a></li>
					</ul> -->
				</div>
				<!-- /by year statistics detail -->

			</div>
			<!-- /page body -->
		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->

</body>
</html>