<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	/**
	*@author PhucNT
	*/
%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>Admin User</title>

<link
	href="${pageContext.request.contextPath}/lib2/css/bootstrap.min.css"
	rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.contextPath}/lib2/css/londinium-theme.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/styles.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/icons.css"
	rel="stylesheet" type="text/css">
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">

<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/sparkline.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.orderbars.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.pie.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.time.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.animator.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/excanvas.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.resize.min.js"></script>


<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uniform.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/select2.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputmask.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/autosize.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputlimit.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/listbox.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/multiselect.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/validate.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/tags.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/switch.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.full.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.queue.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/toolbar.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/daterangepicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fancybox.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/moment.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/jgrowl.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/datatables.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/tabletools.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/colorpicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fullcalendar.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/timepicker.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/application.js"></script>


<!-- YTAsia script -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/ytasia/statistic_user.js"></script>
<!-- /YTAsia script -->


<!-- OnPage script -->
<script type="text/javascript">
	$(function() {
		/**
		 * Employee classify
		 */
		 $.ajax({
				url : '${pageContext.request.contextPath}/getClassifyUser',
				type : "post",
				dataType : "json",
				data : {},
				success : function(result) {
					var data = result[0], detail = result[1];
					var classifyData = [];

					$('#c-total').text(detail.total);

					for (i = 0; i < data.length; i++) {
						var type = data[i].type;
						var number = data[i].number;

						switch (type) {
						case 0:
							classifyData[i] = {
								label : 'Super Admin',
								data : number
							}
							$('#c-super-admin').text(number);
							break;
						case 1:
							classifyData[i] = {
								label : 'Admin',
								data : number
							}
							$('#c-admin').text(number);
							break;
						case 2:
							classifyData[i] = {
								label : 'Employee',
								data : number
							}
							$('#c-user').text(number);
							break;
						}
					}

					setUserClassifyData(classifyData)
				}
			});

		/**
		 * Confirmation when de-active button clicked
		 */
		$('.confirmation').on('click', function() {
			console.log("delete");
			return confirm('Are you sure to de-active this user?');
		})

		/**
		 * Show new user popup
		 */
		$("#edit-popup").dialog({
			autoOpen : false,
			open : function(event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
				$(".ui-dialog-titlebar").hide();
			},
			modal : true,
			width : 600
		});

		/**
		 * Open edit user popup
		 */
		$(document).on('click', "button#edit", function(e) {
			clearData();

			$("#edit-popup").dialog("open");

			var id = $(this).val();

			$.ajax({
				url : '${pageContext.request.contextPath}/adminGetUserInfo',
				type : "post",
				dataType : "json",
				data : {
					editId : id
				},
				success : function(result) {
					setData(result)
				}
			});

			$('a#e-reset-pass').each(function() {
				var href = $(this).attr('href');
				$(this).attr('href', href + id);
			});
		});

		/**
		 * Set data for edit-popup when popup is show
		 */
		function setData(result) {
			$('#edit-bt').val(result.id);
			$('#e-email').val(result.email);
			$('#e-name').val(result.name);
			switch (result.type) {
			case 0:
				$("#e-superadmin-radio").prop("checked", true);
				break;
			case 1:
				$("#e-admin-radio").prop("checked", true);
				break;
			case 2:
				$("#e-user-radio").prop("checked", true);
				break;
			}
			switch (result.status) {
			case 0:
				$("#e-active-radio").prop("checked", true);
				break;
			case 1:
				$("#e-inactive-radio").prop("checked", true);
				break;
			}
		}

		/**
		 * Clear data for edit-popup when popup is close
		 */
		function clearData() {
			$('#e-email').val('');
			$('#e-name').val('');
			$("#e-superadmin-radio").prop("checked", false);
			$("#e-admin-radio").prop("checked", false);
			$("#e-user-radio").prop("checked", false);
			$("#e-active-radio").prop("checked", false);
			$("#e-inactive-radio").prop("checked", false);
			$('a#e-reset-pass')
					.each(
							function() {
								$(this)
										.attr('href',
												"${pageContext.request.contextPath}/adminResetPassword?id=");
							});
		}
	});
</script>
<!-- /onPage script -->

</head>

<body class="full-width">
	<!-- Page container -->
	<div class="page-container">

		<!-- Navbar -->
		<jsp:include page="_adminHeader.jsp"></jsp:include>


		<!-- Page content -->
		<div class="page-content">

			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3>
						Users table <small>All user of YTAsia</small>
					</h3>
				</div>
			</div>
			<!-- /page header -->

			<!-- Page body -->
			<div class="col-xs-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						<ul class="breadcrumb-buttons collapse">
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown"><i class="icon-search3"></i> <span>Search</span>
									<b class="caret"></b></a>
								<div class="popup dropdown-menu dropdown-menu-right">
									<div class="popup-header">
										<span>Quick search</span>
									</div>
									<form method="POST"
										action="${pageContext.request.contextPath}/adminSearchUserBy"
										class="breadcrumb-search">

										<div class="row">
											<label class="control-label col-sm-2">Status:</label>
											<div class="col-sm-9">
												<label class="radio"><input class="styled"
													type="radio" name="userStatus" value="0">Active</label> <label
													class="radio"><input class="styled" type="radio"
													name="userStatus" value="1">In-active</label>
											</div>
										</div>
										<div class="row">
											<label class="control-label col-sm-2">Type:</label>
											<div class="col-sm-9">
												<label class="radio"><input class="styled"
													type="radio" name="userType" value="0">Super Admin</label>
												<label class="radio"><input class="styled"
													type="radio" name="userType" value="1">Admin</label> <label
													class="radio"><input class="styled" type="radio"
													name="userType" value="2">User</label>
											</div>
										</div>

										<input type="submit" class="btn btn-block btn-success"
											value="Search">
									</form>
								</div></li>
							<li class="dropdown"><a
								href="${pageContext.request.contextPath}/adminUserPage" id="all"
								style="float: left">All</a></li>
							<li class="dropdown"><a data-toggle="modal" role="button"
								href="#new_user_popup" id="new-user"><i
									class="icon-user-plus3"></i>New</a></li>
						</ul>
					</div>

					<div class="datatable">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>User</th>
									<th>Name</th>
									<th>Type</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${listUser}" var="user">
									<tr>
										<td><a href="#">${user.getEmail()}</a></td>
										<td>${user.getName()}</td>
										<td>${user.getType()}</td>
										<td>${user.getStatus()}</td>
										<td>
											<button data-toggle="modal" href="#edit_user_popup" id="edit"
												value="${user.getUserId()}" class="btn btn-xs btn-info">Edit</button>
											<a class="btn btn-xs btn-danger confirmation"
											href="${pageContext.request.contextPath}/adminDeactiveUser?id=${user.userId}">De-active</a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="col-md-4">

				<!-- Employee classify -->
				<div class="panel panel-default">
					<div class="panel-heading">
						<h6 class="panel-title">
							<i class="icon-calendar2"></i> Employee classify
						</h6>
					</div>
					<div class="panel-body">
						<div class="graph-standard" id="classify_chart"></div>
					</div>
				</div>
				<!-- /employee classify -->

				<!-- By year statistics detail -->
				<h6>Details:</h6>
				<div class="col-md-6">

					<ul>
						<li>Total: <strong id="c-total" class="">379</strong>
						</li>
						<li>Super Admin: <strong id="c-super-admin" class="">379</strong>
							người
						</li>
						<li>Admin: <strong id="c-admin" class="">379</strong>
						</li>
						<li>User: <strong id="c-user" class="">379</strong>
						</li>
					</ul>
				</div>
				<!-- /by year statistics detail -->

			</div>
			<!-- /page body -->
		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->

	<!-- Edit popup -->
	<div id="edit_user_popup" class="modal fade" tabindex="-1"
		role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<i class="icon-paragraph-justify2"></i> Edit User
					</h4>
				</div>
				<form class="form-horizontal" method="post"
					action="${pageContext.request.contextPath}/adminEditUser">
					<div class="modal-body with-padding">
						<div class="form-group row">
							<div class="col-sm-6">
								<label>User Email:</label><input type="text"
									name="editUserEmail" id="e-email" class="form-control input-sm">
							</div>
							<div class="col-sm-6">
								<label>User Name:</label><input type="text" name="editUserName"
									id="e-name" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-sm-6">
								<label>Type:</label>
								<div>
									<label class="radio-inline"> <input
										id="e-superadmin-radio" type="radio" name="editUserType"
										value="0">Super Admin
									</label> <label class="radio-inline"> <input id="e-admin-radio"
										type="radio" name="editUserType" value="1">Admin
									</label> <label class="radio-inline"> <input id="e-user-radio"
										type="radio" name="editUserType" value="2">User
									</label>
								</div>
							</div>
							<div class="col-sm-6">
								<label>Status:</label>
								<div>
									<label class="radio-inline"> <input id="e-active-radio"
										type="radio" name="editUserStatus" value="0">Active
									</label> <label class="radio-inline"> <input
										id="e-inactive-radio" type="radio" name="editUserStatus"
										value="1">In-active
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" id="edit-bt" name="editUserId"
							class="btn btn-primary">Edit</button>
						<a
							href="${pageContext.request.contextPath}/adminResetPassword?id="
							id="e-reset-pass" class="btn btn-danger " style="float: left;">Reset
							Password</a>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /edit popup -->

	<!-- New popup -->
	<div id="new_user_popup" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<i class="icon-paragraph-justify2"></i> New User
					</h4>
				</div>
				<form class="form-horizontal" method="post"
					action="${pageContext.request.contextPath}/adminAddUser">
					<div class="modal-body with-padding">
						<div class="form-group row">
							<div class="col-sm-6">
								<label>User Email:</label> <input type="text"
									name="newUserEmail" id="n-user" class="form-control input-sm">
							</div>
							<div class="col-sm-6">
								<label>User Name:</label> <input type="text" name="newUserName"
									id="n-name" class="form-control input-sm">
							</div>
						</div>

						<div class="form-group row">
							<div class="col-sm-6">
								<label>Type:</label>
								<div>
									<label class="radio-inline"> <input
										id="superadmin-radio" type="radio" name="newUserType"
										value="0">Super Admin
									</label> <label class="radio-inline"> <input id="admin-radio"
										type="radio" name="newUserType" value="1">Admin
									</label> <label class="radio-inline"> <input id="user-radio"
										type="radio" name="newUserType" value="2" checked="checked">User
									</label>
								</div>
							</div>
							<div class="col-sm-6"></div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /new popup -->
</body>
</html>