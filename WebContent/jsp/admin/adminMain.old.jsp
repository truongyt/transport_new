<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	/**
	*@author PhucNT
	*/
%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>Admin Main</title>

<link
	href="${pageContext.request.contextPath}/lib2/css/bootstrap.min.css"
	rel="stylesheet" media="all" type="text/css">
<link
	href="${pageContext.request.contextPath}/lib2/css/londinium-theme.css"
	rel="stylesheet" media="all" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/styles.css"
	rel="stylesheet" media="all" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/icons.css"
	rel="stylesheet" media="all" type="text/css">
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext"
	rel="stylesheet" media="all" type="text/css">

<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/sparkline.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uniform.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/select2.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputmask.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/autosize.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputlimit.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/listbox.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/multiselect.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/validate.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/tags.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/switch.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.full.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.queue.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/toolbar.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/daterangepicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fancybox.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/moment.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/jgrowl.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/datatables.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/tabletools.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/colorpicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fullcalendar.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/timepicker.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/application.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/html2canvas.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/html2canvas.min.js"></script>


<!-- OnPage script -->
<script>
	$(function() {

		$("#btnPrint").click(function() {
			printElement(document.getElementById("printThis"));

			window.print();
		});

		function printElement(elem) {
			var domClone = elem.cloneNode(true);

			var $printSection = document.getElementById("printSection");

			if (!$printSection) {
				var $printSection = document.createElement("div");
				$printSection.id = "printSection";
				document.body.appendChild($printSection);
			}

			$printSection.innerHTML = "";

			$printSection.appendChild(domClone);
		}

		/**
		 * Show detail-popup when OrderId clicked
		 */
		$(document).on(
				'click',
				"a#t-edit",
				function() {
					clearData();
					var $id = $(this).data("id");
					$.ajax({
						url : '${pageContext.request.contextPath}/viewOrder',
						type : "post",
						dataType : "json",
						data : {
							orderId : $id
						},
						success : function(result) {
							setData(result[0]);
							var eProducts = result[1];

							for (i = 0; i < eProducts.length; i++) {
								$('#p-product').append(
										'<li>' + eProducts[i].name + ' ('
												+ eProducts[i].quantity + ')'
												+ '</li>');
							}

							viewMode()
						}
					});

					$('button#submit').prop("disabled", true);
					$('button#edit').prop("disabled", false);

					$('a#delete').each(function() {
						var href = $(this).attr('href');
						$(this).attr('href', href + $id);
					});
				});

		/**
		 * Change detail-popup to editable when 'Edit button' clicked
		 */
		$("button#edit").click(function(e) {
			e.preventDefault();
			editMode();
			$(this).prop("disabled", true);
			$('button#submit').prop("disabled", false);
		});

		/**
		 * Set data for detail-popup when popup showed
		 */
		function setData(result) {
			$('#submit').val(result.id);
			$('#p-provider').val(result.providerId);
			$('#p-detail').val(result.detail);
			$('#p-status').val(result.statusId);
			$('#p-location').val(result.locationId);
			$('#p-sender').val(result.senderName);
			$('#p-receipter').val(result.receipterName);
		}

		/**
		 * Clear data for detail-popup when popup closed
		 */
		function clearData() {
			$('#submit').val("");
			$('#p-provider').val("");
			$('#p-product').text("");
			$('#p-status').val("");
			$('#p-location').val("");
			$('#p-sender').val("");
			$('#p-receipter').val("");
			$('a#delete')
					.each(
							function() {
								var href = "${pageContext.request.contextPath}/adminDeleteOrder?orderId=";
								$(this).attr('href', href);
							});
		}

		/**
		 * Change all data of detail-popup to non-editable
		 */
		function viewMode() {
			$("#p-provider").prop("disabled", true);
			$("#p-detail").prop("readonly", true);
			$("#p-status").prop("disabled", true);
			$("#p-location").prop("disabled", true);
			$("#p-sender").prop("readonly", true);
			$("#p-receipter").prop("readonly", true);
		}

		/**
		 * Change all data of detail-popup to editable
		 */
		function editMode() {
			$("#p-provider").prop("disabled", false);
			$("#p-detail").prop("readonly", false);
			$("#p-status").prop("disabled", false);
			$("#p-location").prop("disabled", false);
			//$("#p-sender").prop("readonly", false);
			//$("#p-receipter").prop("readonly", false);
		}

		//--------------- VIEW INVOICE ----------------//

		/**
		 * Show invoice-popup when OrderId clicked
		 */
		$(document).on('click', 'a#t-invoice', function() {
			clearData();

			var $id = $(this).data("id");
			$.ajax({
				url : '${pageContext.request.contextPath}/viewInvoice',
				type : "post",
				dataType : "json",
				data : {
					orderId : $id
				},
				success : function(result) {
					var detail = result[0], products = result[1];
					setDetail(detail);
					setProducts(products)
				}
			});
		});

		/**
		 * Set data for detail-popup when popup showed
		 */
		function setDetail(detail) {
			$('#d-sender-name').text(detail.senderName);
			$('#d-sender-email').text(detail.senderEmail);
			$('#d-sender-address').text(detail.senderAddress);
			$('#d-sender-phone').text(detail.senderPhone);

			$('#d-receipter-name').text(detail.receipterName);
			$('#d-receipter-email').text(detail.receipterEmail);
			$('#d-receipter-address').text(detail.receipterAddress);
			$('#d-receipter-phone').text(detail.receipterPhone);

			$('#d-order-id').text(detail.fId);
			$('#d-order-time').text(detail.time);
			$('#d-responsible').text(detail.user);
			switch (detail.paidStatus) {
			case 0:
				$('#d-paid-status').text('Unpaid');
				$("#d-paid-status").attr('class',
						'label label-danger pull-right');
				break;

			case 1:
				$('#d-paid-status').text('Paid');
				$("#d-paid-status").attr('class',
						'label label-success pull-right');
				break;
			}
		}

		/**
		 * Set data for invoice-popup when popup showed
		 */
		function setProducts(products) {
			var subtotal = 0;
			var tax = 0;
			var total = 0;

			for (i = 0; i < products.length; i++) {
				$('#d-product-table').append(
						'<tr><td>' + products[i].type + '</td><td>'
								+ products[i].name + '</td><td>'
								+ products[i].unitPrice + '</td><td>'
								+ products[i].quantity + '</td><td><strong>'
								+ products[i].price + '</strong></td></tr>');
				subtotal = subtotal + products[i].price;
			}
			total = subtotal + (subtotal / 100 * tax);
			$('#d-subtotal').text(subtotal + ' jpy');
			$('#d-tax').text(tax + '%');
			$('#d-total').text(total + ' jpy');
		}

		/**
		 * Clear data for detail-popup when popup closed
		 */
		function clearData() {
			$('#d-sender-name').text("");
			$('#d-sender-email').text("");
			$('#d-sender-address').text("");
			$('#d-sender-phone').text("");

			$('#d-receipter-name').text("");
			$('#d-receipter-email').text("");
			$('#d-receipter-address').text("");
			$('#d-receipter-phone').text("");

			$('#d-order-id').text("");
			$('#d-order-time').text("");
			$('#d-responsible').text("");
			$('#d-paid-status').text("");

			$('#d-product-table').text('');
		}
	});
</script>
<!-- /onPage script -->

</head>

<body class="full-width">
	<!-- Page container -->
	<div class="page-container">

		<!-- Navbar -->
		<jsp:include page="_adminHeader.jsp"></jsp:include>


		<!-- Page content -->
		<div class="page-content">

			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3>
						Orders table <small>All Transport orders of YTAsia</small>
					</h3>
				</div>
			</div>
			<!-- /page header -->

			<div class="col-xs-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h6 class="panel-title">
							<i class="icon-paragraph-justify"></i> Striped datatable
						</h6>

						<ul class="breadcrumb-buttons collapse">
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown"><i class="icon-search3"></i> <span>Search</span>
									<b class="caret"></b></a>
								<div class="popup dropdown-menu dropdown-menu-right">
									<div class="popup-header">
										<span>Quick search</span>
									</div>
									<form
										action="${pageContext.request.contextPath}/adminSearchOrderBy"
										method="post" class="breadcrumb-search">
										<div class="row">
											<div class="col-xs-2">
												<select data-placeholder="search by shipment"
													class="clear-results" name="searchShipment" tabindex="2">
													<option value=""></option>
													<c:forEach items="${listShipment}" var="shipment">
														<option value="${shipment.getShipmentId()}">${shipment.getDepartTime()}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-2">
												<select data-placeholder="Status" class="clear-results"
													tabindex="2" name="searchStatus">
													<option value=""></option>
													<c:forEach items="${listStatus}" var="status">
														<option value="${status.getStatusId()}">${status.getStatusName()}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-2">
												<select data-placeholder="Location" class="clear-results"
													tabindex="2" name="searchLocation">
													<option value=""></option>
													<c:forEach items="${listLocation}" var="location">
														<option value="${location.getLocationId()}">${location.getLocationName()}</option>
													</c:forEach>
												</select>
											</div>
										</div>

										<input type="submit" class="btn btn-block btn-success"
											value="Search">
									</form>
								</div></li>
							<li class="dropdown"><a
								href="${pageContext.request.contextPath}/adminMainPage" id="all"
								style="float: left">All</a></li>
						</ul>
					</div>

					<div class="datatable">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>#Id</th>
									<th>Provider</th>
									<th>Status</th>
									<th>Location</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${orders}" var="order">
									<tr>
										<td>${order.getTransportOrderId()}</td>
										<td>${order.getProviderId()}</td>
										<td>${order.getStatusId()}</td>
										<td>${order.getLocationId()}</td>
										<td><a id="t-edit" data-toggle="modal" href="#edit-popup"
											data-id="${order.getTransportOrderId()}"
											class="btn btn-info btn-xs">Edit</a> <a data-toggle="modal"
											href="#invoice-popup" id="t-invoice"
											data-id="${order.getTransportOrderId()}"
											class="btn btn-success btn-xs">Invoice</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- /striped datatable inside panel -->
		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->

	<!-- Edit popup -->
	<div id="edit-popup" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<i class="icon-paragraph-justify2"></i> Edit User
					</h4>
				</div>
				<form action="${pageContext.request.contextPath}/adminUpdateOrder"
					method="post" role="form">
					<div class="modal-body with-padding">
						<div class="form-group row">
							<div class="col-md-6">
								<label>Provider:</label> <select class="form-control input-sm"
									name="provider" id="p-provider" tabindex="5">
									<c:forEach items="${listProvider}" var="provider">
										<option value="${provider.getProviderId()}">${provider.getProviderName()}</option>
									</c:forEach>
								</select>
							</div>

							<div class="col-md-6">
								<label>Product:</label>
								<ul id="p-product">
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Status:</label> <select class="form-control input-sm"
									name="status" id="p-status" tabindex="5">
									<c:forEach items="${listStatus}" var="status">
										<option value="${status.getStatusId()}">${status.getStatusName()}</option>
									</c:forEach>
								</select>
							</div>

							<div class="col-md-6">
								<label>Location:</label> <select class="form-control input-sm"
									name="location" id="p-location" tabindex="5">
									<c:forEach items="${listLocation}" var="location">
										<option value="${location.getLocationId()}">${location.getLocationName()}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Sender:</label> <input type="text" id="p-sender"
									name="sender" class="form-control">
							</div>

							<div class="col-md-6">
								<label>Recipient:</label> <input type="text" id="p-receipter"
									name="receipter" class="form-control">
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button name="update-id" id="submit" type="submit"
							class="btn btn-success" style="float: right; margin-right: 10px">Submit</button>
						<button id="edit" class="btn btn-info"
							style="float: right; margin-right: 10px;">Edit</button>
						<a class="btn btn-danger confirmation" style="float: left;"
							id="delete"
							href="${pageContext.request.contextPath}/adminDeleteOrder?orderId=">Delete</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /edit popup -->

	<!-- Invoice popup -->
	<div id="invoice-popup" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Modal title</h4>
				</div>

				<!-- New invoice template -->
				<div id="printThis" class="panel">
					<div class="panel-body">

						<div class="row invoice-header">
							<div class="col-sm-6">
								<h3>YTAsia</h3>
								<span>Best Japan - VietNam transport service ever !</span>
							</div>

							<div class="col-sm-6">
								<ul class="invoice-details">
									<li>Invoice # <strong id="d-order-id" class="text-danger"></strong></li>
									<li>Date of Invoice: <strong id="d-order-time"></strong></li>
									<li>Due Date: <strong></strong></li>
								</ul>
							</div>
						</div>


						<div class="row">
							<div class="col-sm-3">
								<h6>Sender:</h6>
								<ul>
									<li>Name:<strong id="d-sender-name" class="pull-right"></strong></li>
									<li>Email:<strong id="d-sender-email" class="pull-right"></strong></li>
									<li>Address:<strong id="d-sender-address"
										class="pull-right"></strong></li>
									<li>Phone:<strong id="d-sender-phone" class="pull-right"></strong></li>
								</ul>
							</div>

							<div class="col-sm-1"></div>

							<div class="col-sm-3">
								<h6>Recipient:</h6>
								<ul>
									<li>Name:<strong id="d-receipter-name" class="pull-right"></strong></li>
									<li>Email:<strong id="d-receipter-email"
										class="pull-right"></strong></li>
									<li>Address:<strong id="d-receipter-address"
										class="pull-right"></strong></li>
									<li>Phone:<strong id="d-receipter-phone"
										class="pull-right"></strong></li>
								</ul>
							</div>

							<div class="col-sm-1"></div>

							<div class="col-sm-4">
								<h6>Invoice Details:</h6>
								<ul>
									<li>Total hours spent: <strong class="pull-right">379</strong></li>
									<li>Responsible: <a href="#" class="pull-right"
										id="d-responsible"></a></li>
									<li>Issued by: <a href="#" class="pull-right"
										id="d-issued-by"></a></li>
									<li>Payment method: <strong class="pull-right">Wire
											transfer</strong></li>
									<!-- <li class="invoice-status"><strong>Current
											status: <span class="label label-danger pull-right">Unpaid</span>
									</strong></li> -->
								</ul>
							</div>
						</div>
					</div>

					<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>Type</th>
									<th>Description</th>
									<th>Unit price</th>
									<th>Quantity</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody id="d-product-table">

							</tbody>
						</table>
					</div>

					<div class="panel-body">
						<div class="col-sm-8"></div>

						<div class="col-sm-4">
							<h6>Total:</h6>
							<table class="table">
								<tbody>
									<tr>
										<th>Subtotal:</th>
										<td id="d-subtotal" class="text-right"></td>
									</tr>
									<tr>
										<th>Tax:</th>
										<td id="d-tax" class="text-right"></td>
									</tr>
									<tr>
										<th>Total:</th>
										<td id="d-total" class="text-right text-danger"><h6></h6></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- /new invoice template -->
			</div>
			<div class="btn-group pull-right">
				<button type="button" id="btnPrint" class="btn btn-success">
					<i class="icon-print2"></i> Print
				</button>
			</div>
		</div>
	</div>
	<!-- /invoice popup -->
</body>
</html>