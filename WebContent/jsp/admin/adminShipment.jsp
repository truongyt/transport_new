<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	/**
	*@author PhucNT
	*/
%>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>Catalog</title>

<link
	href="${pageContext.request.contextPath}/lib2/css/bootstrap.min.css"
	rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.contextPath}/lib2/css/londinium-theme.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/styles.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/icons.css"
	rel="stylesheet" type="text/css">
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">

<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/sparkline.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uniform.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/select2.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputmask.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/autosize.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputlimit.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/listbox.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/multiselect.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/validate.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/tags.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/switch.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.full.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.queue.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/toolbar.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/daterangepicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fancybox.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/moment.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/jgrowl.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/datatables.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/tabletools.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/colorpicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fullcalendar.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/timepicker.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/application.js"></script>


<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/jquery.datetimepicker.full.min.js"></script>
<link
	href="${pageContext.request.contextPath}/lib2/css/jquery.datetimepicker.css"
	rel="stylesheet" type="text/css">

<!-- OnPage script -->
<script type="text/javascript">
	$(function() {
		$('#sort').click();

		/**
		 * Confirmation when delete button clicked
		 */
		$('.confirmation').on('click', function() {
			console.log("delete");
			return confirm('Are you sure to delete this customer?');
		})

		/**
		 * Get info for location edit popup
		 */
		$(document)
				.on(
						'click',
						"button#edit-shipment",
						function() {
							var id = $(this).val();

							$
									.ajax({
										url : '${pageContext.request.contextPath}/adminGetShipmentInfo',
										type : "post",
										dataType : "json",
										data : {
											id : id,
										},
										success : function(result) {
											$('#e-dep').val(result.depart);
											$('#e-del').val(result.delivery);
											$('#e-usr').val(result.user);
											$('#e-id').val(result.id);
										}
									});
						});

		/**
		 * Clear new LOCATION form
		 */
		$("button#n-loc-clear").click(function() {
			$('#n-loc-id').val('');
			$('#n-loc-name').val('');
		});

		$('#depart').datetimepicker(
				{
					format : 'Y-m-d',
					onShow : function(ct) {
						this.setOptions({
							maxDate : $('#delivery').val() ? $('#delivery')
									.val() : false
						})
					},
					timepicker : false
				});

		$('#delivery').datetimepicker({
			format : 'Y-m-d',
			onShow : function(ct) {
				this.setOptions({
					minDate : $('#depart').val() ? $('#depart').val() : false
				})
			},
			timepicker : false
		});

		$('#e-dep').datetimepicker({
			format : 'Y-m-d',
			onShow : function(ct) {
				this.setOptions({
					maxDate : $('#e-del').val() ? $('#e-del').val() : false
				})
			},
			timepicker : false
		});

		$('#e-del').datetimepicker({
			format : 'Y-m-d',
			onShow : function(ct) {
				this.setOptions({
					minDate : $('#e-dep').val() ? $('#e-dep').val() : false
				})
			},
			timepicker : false
		});
	});
</script>
<!-- /onPage script -->


</head>

<body class="full-width">
	<!-- Page container -->
	<div class="page-container">

		<!-- Navbar -->
		<jsp:include page="_adminHeader.jsp"></jsp:include>


		<!-- Page content -->
		<div class="page-content">

			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3 id="h3">
						Shipment <small>Manage shipment of YTAsia</small>
					</h3>
				</div>
			</div>
			<!-- /page header -->

			<!-- Page body -->
			<div class="panel panel-default">

				<!-- Data Table -->
				<div class="col-md-8">
					<div class="datatable">
						<table class="table table-striped">
							<thead>
								<tr>
									<th id="sort">#id</th>
									<th>Departure</th>
									<th>Delivery</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${shipments}" var="shipment">
									<tr>
										<td>${shipment.getShipmentId()}</td>
										<td>${shipment.getDepartTime()}</td>
										<td>${shipment.getDeliveryTime()}</td>
										<td><button data-toggle="modal"
												href="#edit_shipment_popup" id="edit-shipment"
												class="btn btn-xs btn-info"
												value="${shipment.getShipmentId()}">Edit</button> <a
											class="btn btn-danger btn-xs confirmation"
											href="${pageContext.request.contextPath}/adminDeleteShipment?id=${shipment.getShipmentId()}">Delete</a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<!-- /data table -->

				<!-- New Shipment form -->
				<div class="col-md-4">
					<form class="form-horizontal " method="post"
						action="${pageContext.request.contextPath}/adminAddShipment">

						<div class="modal-body with-padding">
							<div class="block-inner text-danger">
								<h6 class="heading-hr">
									New Shipment <small class="display-block">Please enter
										new shipment</small>
								</h6>
							</div>

							<div class="form-group row">
								<div class="col-md-6">
									<label>Departure: </label><input type="text" id="depart"
										name="newDepart" class=" form-control"
										placeholder="Departure day" required>
								</div>
								<div class="col-md-6">
									<label>Delivery: </label><input type="text" id="delivery"
										name="newDelivery" class=" form-control"
										placeholder="Delivery day" required>
								</div>
							</div>

						</div>
						<div class="modal-footer">
							<button id="n-loc-clear" type="button" class="btn btn-warning">Clear</button>
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
				</div>
				<!-- /new shipment form -->
			</div>
			<!-- /page body -->
		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->

	<!-- Shipment edit popup -->
	<div id="edit_shipment_popup" class="modal fade" tabindex="-1"
		role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit Shipment</h4>
				</div>
				<form class="form-horizontal" method="post"
					action="${pageContext.request.contextPath}/adminEditShipment">
					<div class="modal-body with-padding">

						<div class="form-group row">
							<div class="col-sm-4">
								<label>Departure:</label> <input type="text" name="editDepart"
									id="e-dep" class="form-control input-sm" required>
							</div>
							<div class="col-sm-4">
								<label>Delivery:</label> <input type="text" name="editDelivery"
									id="e-del" class="form-control input-sm" required>
							</div>
							<div class="col-sm-4">
								<label>Transporter:</label><select class="form-control input-sm"
									name="editUser" id="e-usr" tabindex="5">
									<c:forEach items="${users}" var="user">
										<option value="${user.getUserId()}">${user.getName()}</option>
									</c:forEach>
								</select>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary" id="e-id"
							name="editId">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /location edit popup -->

</body>
</html>