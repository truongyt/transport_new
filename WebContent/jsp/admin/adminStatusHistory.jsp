<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	/**
	*@author PhucNT
	*/
%>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>Status history</title>

<link
	href="${pageContext.request.contextPath}/lib2/css/bootstrap.min.css"
	rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.contextPath}/lib2/css/londinium-theme.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/styles.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/icons.css"
	rel="stylesheet" type="text/css">
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">

<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/sparkline.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uniform.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/select2.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputmask.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/autosize.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputlimit.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/listbox.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/multiselect.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/validate.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/tags.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/switch.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.full.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.queue.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/toolbar.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/daterangepicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fancybox.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/moment.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/jgrowl.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/datatables.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/tabletools.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/colorpicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fullcalendar.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/timepicker.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/application.js"></script>


<!-- OnPage script -->
<script type="text/javascript">
	$(function() {
		$('#sort').click();
		
		$(document)
				.on(
						'click',
						"button#detail",
						function() {
							clear();
							var id = $(this).val();

							$
									.ajax({
										url : '${pageContext.request.contextPath}/adminGetStatusDetail',
										type : "post",
										dataType : "json",
										data : {
											id : id,
										},
										success : function(result) {
											$('#d-id').val(result.orderId);
											$('#d-provider').val(
													result.provider);
											$('#d-status').val(result.status);
											$('#d-location').val(
													result.location);
											$('#d-sender').val(result.sender);
											$('#d-receipter').val(
													result.receipter);
											$('#d-user').val(result.user);
											$('#d-other').val(result.other);
										}
									});
						});
		function clear() {
			$('#d-id').val('');
			$('#d-provider').val('');
			$('#d-status').val('');
			$('#d-location').val('');
			$('#d-sender').val('');
			$('#d-receipter').val('');
			$('#d-user').val('');
			$('#d-other').val('');
		}
	});
</script>
<!-- /onPage script -->


</head>

<body class="full-width">
	<!-- Page container -->
	<div class="page-container">

		<!-- Navbar -->
		<jsp:include page="_adminHeader.jsp"></jsp:include>

		<!-- Page content -->
		<div class="page-content">

			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3 id="h3">
						Status history <small>Manage history of all status's
							change</small>
					</h3>
				</div>
			</div>
			<!-- /page header -->

			<!-- Page body -->
			<div class="panel panel-default">

				<!-- Data Table -->
				<div class="datatable">
					<table class="table table-striped">
						<thead>
							<tr>
								<th id="sort">#id</th>
								<th>Order Id</th>
								<th>Status</th>
								<th>Time</th>
								<th>User</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${histories}" var="history">
								<tr>
									<td><a href="#">${history.getId()}</a></td>
									<td>${history.getTransOrderId()}</td>
									<td>${history.getStatusId()}</td>
									<td>${history.getTime()}</td>
									<td>${history.getUserId()}</td>
									<td><button data-toggle="modal" href="#detail_popup"
											id="detail" class="btn btn-xs btn-info"
											value="${history.getId()}">Detail</button></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<!-- /data table -->

			</div>
			<!-- /page body -->

		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->

	<!-- History detail modal -->
	<div id="detail_popup" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Detail</h4>
				</div>
				<div class="modal-body with-padding">
					<div class="form-group row">
						<div class="col-sm-6">
							<label>Order id:</label> <input type="text" id="d-id"
								class="form-control input-sm" readonly>
						</div>
						<div class="col-sm-6">
							<label>Provider:</label> <input type="text" id="d-provider"
								class="form-control input-sm" readonly>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-6">
							<label>Status:</label> <input type="text" id="d-status"
								class="form-control input-sm" readonly>
						</div>
						<div class="col-sm-6">
							<label>Location:</label> <input type="text" id="d-location"
								class="form-control input-sm" readonly>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-6">
							<label>Sender:</label> <input type="text" id="d-sender"
								class="form-control input-sm" readonly>
						</div>
						<div class="col-sm-6">
							<label>Recipient:</label> <input type="text" id="d-receipter"
								class="form-control input-sm" readonly>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-6">
							<label>User:</label> <input type="text" id="d-user"
								class="form-control input-sm" readonly>
						</div>
						<div class="col-sm-6">
							<label>Other:</label> <input type="text" id="d-other"
								class="form-control input-sm" readonly>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
	<!-- /history detail modal -->
</body>
</html>