<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	/**
	*@author PhucNT
	*/
%>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>Catalog</title>

<link
	href="${pageContext.request.contextPath}/lib2/css/bootstrap.min.css"
	rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.contextPath}/lib2/css/londinium-theme.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/styles.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/icons.css"
	rel="stylesheet" type="text/css">
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">

<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/sparkline.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uniform.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/select2.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputmask.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/autosize.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputlimit.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/listbox.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/multiselect.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/validate.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/tags.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/switch.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.full.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.queue.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/toolbar.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/daterangepicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fancybox.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/moment.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/jgrowl.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/datatables.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/tabletools.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/colorpicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fullcalendar.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/timepicker.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/application.js"></script>


<!-- OnPage script -->
<script type="text/javascript">
	$(function() {

		/* $("#n-prv-submit").click(function() {
				var id = $("#n-prv-id").val();
				var name = $("#n-prv-name").val();

				$.ajax({
					url : '${pageContext.request.contextPath}/addProvider',
					type : "post",
					dataType : "text",
					data : {
						id : id,
						name : name
					},
					success : function(result) {
						$('#provider-tab').switchClass('tab-pane fade in active');
					}
				});
			}); */

		/**
		 * Confirmation when delete button clicked
		 */
		$('.confirmation').on('click', function() {
			console.log("delete");
			return confirm('Are you sure to delete this customer?');
		})

		/**
		 * Get info for product edit popup
		 */

		$(document)
				.on(
						'click',
						"button#edit-product",
						function() {

							var id = $(this).val();

							$
									.ajax({
										url : '${pageContext.request.contextPath}/adminGetProductTypeInfo',
										type : "post",
										dataType : "json",
										data : {
											id : id,
										},
										success : function(result) {
											$('#e-prd-submit').val(result.id)
											$('#e-prd-name').val(result.name);
											$('#e-prd-price').val(result.price);
											$('#e-prd-unit').val(result.unit);
										}
									});
						});

		/**
		 * Get info for provider edit popup
		 */
		$(document)
				.on(
						'click',
						"button#edit-provider",
						function() {
							var id = $(this).val();

							$
									.ajax({
										url : '${pageContext.request.contextPath}/adminGetProviderInfo',
										type : "post",
										dataType : "json",
										data : {
											id : id,
										},
										success : function(result) {
											$('#e-prv-id').val(result.id);
											$('#e-prv-name').val(result.name);
										}
									});
						});

		/**
		 * Get info for status edit popup
		 */
		$(document).on('click', "button#edit-status", function() {
			var id = $(this).val();

			$.ajax({
				url : '${pageContext.request.contextPath}/adminGetStatusInfo',
				type : "post",
				dataType : "json",
				data : {
					id : id,
				},
				success : function(result) {
					$('#e-sta-id').val(result.id);
					$('#e-sta-name').val(result.name);
				}
			});
		});

		/**
		 * Get info for location edit popup
		 */
		$(document)
				.on(
						'click',
						"button#edit-location",
						function() {
							var id = $(this).val();

							$
									.ajax({
										url : '${pageContext.request.contextPath}/adminGetLocationInfo',
										type : "post",
										dataType : "json",
										data : {
											id : id,
										},
										success : function(result) {
											$('#e-loc-id').val(result.id);
											$('#e-loc-name').val(result.name);
										}
									});
						});

		/**
		 * Clear new PRODUCT TYPE form
		 */
		$("button#n-prd-clear").click(function() {
			$('#n-prd-name').val('');
			$('#n-prd-price').val('');
		});

		/**
		 * Clear new PROVIDER form
		 */
		$("button#n-prv-clear").click(function() {
			$('#n-prv-id').val('');
			$('#n-prv-name').val('');
		});

		/**
		 * Clear new STATUS form
		 */
		$("button#n-sta-clear").click(function() {
			$('#n-sta-id').val('');
			$('#n-sta-name').val('');
		});

		/**
		 * Clear new LOCATION form
		 */
		$("button#n-loc-clear").click(function() {
			$('#n-loc-id').val('');
			$('#n-loc-name').val('');
		});
	});
</script>
<!-- /onPage script -->


</head>

<body class="full-width">
	<!-- Page container -->
	<div class="page-container">

		<!-- Navbar -->
		<jsp:include page="_adminHeader.jsp"></jsp:include>


		<!-- Page content -->
		<div class="page-content">

			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3 id="h3">
						Catalog <small>Manage price, provider, status, location of
							YTAsia</small>
					</h3>
				</div>
			</div>
			<!-- /page header -->

			<!-- Page body -->

			<!-- Main frame -->
			<div class="well">
				<div class="tabbable">

					<!-- Tab bar -->
					<ul class="nav nav-pills">
						<li class="active"><a href="#product-tab" data-toggle="tab"><i
								class="icon-accessibility"></i> Product</a></li>
						<li><a href="#provider-tab" data-toggle="tab"><i
								class="icon-stack"></i> Provider</a></li>
						<li><a href="#status-tab" data-toggle="tab"><i
								class="icon-stack"></i> Status</a></li>
						<li><a href="#location-tab" data-toggle="tab"><i
								class="icon-stack"></i> Location</a></li>

					</ul>
					<!-- /tab bar -->

					<!-- Content -->
					<div class="tab-content pill-content">
						<!-- Product -->
						<div class="tab-pane fade in active" id="product-tab">
							<div class="panel-body">

								<!-- Data Table -->
								<div class="col-md-6">
									<div class="datatable">
										<table class="table table-striped">
											<thead>
												<tr>
													<th>#id</th>
													<th>Name</th>
													<th>Unit</th>
													<th>Price</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${products}" var="product">
													<tr>
														<td><a href="#">${product.getTypeId()}</a></td>
														<td>${product.getTypeName()}</td>
														<td>${product.getUnit()}</td>
														<td>${product.getPrice()}</td>
														<td><button data-toggle="modal"
																href="#edit_product_popup" id="edit-product"
																class="btn btn-xs btn-info"
																value="${product.getTypeId()}">Edit</button> <a
															class="btn btn-danger btn-xs confirmation"
															href="${pageContext.request.contextPath}/adminDeleteProductType?id=${product.getTypeId()}">Delete</a>
														</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
								<!-- /data table -->

								<!-- New product type form -->
								<div class="col-md-6">
									<form class="form-horizontal" method="post"
										action="${pageContext.request.contextPath}/addProductType">

										<div class="modal-body with-padding">
											<div class="block-inner text-danger">
												<h6 class="heading-hr">
													New Product Type <small class="display-block">Please
														enter new product type</small>
												</h6>
											</div>

											<div class="form-group row">
												<div class="col-sm-4">
													<label>Name:<span class="mandatory">*</span></label> <input
														type="text" name="newProductName" id="n-prd-name"
														class="form-control  input-sm" required>
												</div>
												<div class="col-sm-4">
													<label>Unit:</label> <select class="form-control input-sm"
														name="newUnit" id="n-prd-unit" tabindex="5">
														<option value="kg">kg</option>
														<option value="cái">cái</option>
													</select>
												</div>
												<div class="col-sm-4">
													<label>Price:<span class="mandatory">*</span></label> <input
														type="text" name="newProductPrice" id="n-prd-price"
														class="form-control  input-sm" required>
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<button id="n-prd-clear" type="button"
												class="btn btn-warning">Clear</button>
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
									</form>
								</div>
								<!-- /new product type form -->

							</div>
						</div>
						<!-- /product -->

						<!-- Provider -->
						<div class="tab-pane fade" id="provider-tab">
							<div class="panel-body">

								<!-- Data Table -->
								<div class="col-md-6">
									<div class="datatable">
										<table class="table table-striped">
											<thead>
												<tr>
													<th>#id</th>
													<th>Name</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${providers}" var="provider">
													<tr>
														<td>${provider.getProviderId()}</td>
														<td>${provider.getProviderName()}</td>
														<td><button data-toggle="modal"
																href="#edit_provider_popup" id="edit-provider"
																class="btn btn-xs btn-info"
																value="${provider.getProviderId()}">Edit</button> <a
															class="btn btn-danger btn-xs confirmation"
															href="${pageContext.request.contextPath}/adminDeleteProvider?id=${provider.getProviderId()}">Delete</a>
														</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
								<!-- /data table -->

								<!-- New provider form -->
								<div class="col-md-6">
									<form class="form-horizontal " method="post"
										action="${pageContext.request.contextPath}/addProvider">

										<div class="modal-body with-padding">
											<div class="block-inner text-danger">
												<h6 class="heading-hr">
													New Provider <small class="display-block">Please
														enter new provider</small>
												</h6>
											</div>

											<div class="form-group row">
												<div class="col-sm-6">
													<label>ID:<span class="mandatory">*</span></label> <input
														type="text" name="newProviderId" id="n-prv-id"
														class="form-control required input-sm" required>
												</div>
												<div class="col-sm-6">
													<label>Name:<span class="mandatory">*</span></label> <input
														type="text" name="newProviderName" id="n-prv-name"
														class="form-control required input-sm" required>
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<button id="n-prv-clear" type="button"
												class="btn btn-warning">Clear</button>
											<button id="n-prv-submit" type="submit"
												class="btn btn-primary">Submit</button>
										</div>
									</form>
								</div>
								<!-- /new provider form -->

							</div>
						</div>
						<!-- /provider -->

						<!-- Status -->
						<div class="tab-pane fade" id="status-tab">
							<div class="panel-body">

								<!-- Data Table -->
								<div class="col-md-6">
									<div class="datatable">
										<table class="table table-striped">
											<thead>
												<tr>
													<th>#id</th>
													<th>Name</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${statuss}" var="status">
													<tr>
														<td>${status.getStatusId()}</td>
														<td>${status.getStatusName()}</td>
														<td><button data-toggle="modal"
																href="#edit_status_popup" id="edit-status"
																class="btn btn-xs btn-info"
																value="${status.getStatusId()}">Edit</button> <a
															class="btn btn-danger btn-xs confirmation"
															href="${pageContext.request.contextPath}/adminDeleteStatus?id=${status.getStatusId()}">Delete</a>
														</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
								<!-- /data table -->

								<!-- New Status form -->
								<div class="col-md-6">
									<form class="form-horizontal " method="post"
										action="${pageContext.request.contextPath}/addStatus">

										<div class="modal-body with-padding">
											<div class="block-inner text-danger">
												<h6 class="heading-hr">
													New Status <small class="display-block">Please
														enter new status</small>
												</h6>
											</div>

											<div class="form-group row">
												<div class="col-sm-6">
													<label>ID:<span class="mandatory">*</span></label> <input
														type="text" name="newStatusId" id="n-sta-id"
														class="form-control required input-sm" required>
												</div>
												<div class="col-sm-6">
													<label>Name:<span class="mandatory">*</span></label> <input
														type="text" name="newStatusName" id="n-sta-name"
														class="form-control required input-sm" required>
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<button id="n-sta-clear" type="button"
												class="btn btn-warning">Clear</button>
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
									</form>
								</div>
								<!-- /new status form -->
							</div>
						</div>
						<!-- /status -->

						<!-- Location -->
						<div class="tab-pane fade" id="location-tab">
							<div class="panel-body">

								<!-- Data Table -->
								<div class="col-md-6">
									<div class="datatable">
										<table class="table table-striped">
											<thead>
												<tr>
													<th>#id</th>
													<th>Name</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${locations}" var="location">
													<tr>
														<td>${location.getLocationId()}</td>
														<td>${location.getLocationName()}</td>
														<td><button data-toggle="modal"
																href="#edit_location_popup" id="edit-location"
																class="btn btn-xs btn-info"
																value="${location.getLocationId()}">Edit</button> <a
															class="btn btn-danger btn-xs confirmation"
															href="${pageContext.request.contextPath}/adminDeleteLocation?id=${location.getLocationId()}">Delete</a>
														</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
								<!-- /data table -->

								<!-- New Location form -->
								<div class="col-md-6">
									<form class="form-horizontal " method="post"
										action="${pageContext.request.contextPath}/addLocation">

										<div class="modal-body with-padding">
											<div class="block-inner text-danger">
												<h6 class="heading-hr">
													New Location <small class="display-block">Please
														enter new location</small>
												</h6>
											</div>

											<div class="form-group row">
												<div class="col-sm-6">
													<label>ID:<span class="mandatory">*</span></label> <input
														type="text" name="newLocationId" id="n-loc-id"
														class="form-control required input-sm" required>
												</div>
												<div class="col-sm-6">
													<label>Name:<span class="mandatory">*</span></label> <input
														type="text" name="newLocationName" id="n-loc-name"
														class="form-control required input-sm" required>
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<button id="n-loc-clear" type="button"
												class="btn btn-warning">Clear</button>
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
									</form>
								</div>
								<!-- /new status form -->
							</div>
						</div>
						<!-- /location -->
					</div>
					<!-- /content -->
				</div>
			</div>
			<!-- /main frame -->

			<!-- /page body -->

		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->

	<!-- Product type edit popup -->
	<div id="edit_product_popup" class="modal fade" tabindex="-1"
		role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit Product Type</h4>
				</div>
				<form class="form-horizontal" method="post"
					action="${pageContext.request.contextPath}/adminEditProductType">
					<div class="modal-body with-padding">

						<div class="form-group row">
							<div class="col-sm-4">
								<label>Name:</label> <input type="text" name="editProductName"
									id="e-prd-name" class="form-control input-sm">
							</div>
							<div class="col-sm-4">
								<label>Unit:</label> <select class="form-control input-sm"
									name="editUnit" id="e-prd-unit" tabindex="5">
									<option value="kg">kg</option>
									<option value="chiếc">chiếc</option>
								</select>
							</div>
							<div class="col-sm-4">
								<label>Price:</label> <input type="text" name="editProductPrice"
									id="e-prd-price" class="form-control input-sm">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary" id="e-prd-submit"
							name="editProductId">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /product type edit popup -->

	<!-- Provider edit popup -->
	<div id="edit_provider_popup" class="modal fade" tabindex="-1"
		role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit Provider</h4>
				</div>
				<form class="form-horizontal" method="post"
					action="${pageContext.request.contextPath}/adminEditProvider">
					<div class="modal-body with-padding">

						<div class="form-group row">
							<div class="col-sm-6">
								<label>Id:</label> <input type="text" name="editProviderId"
									id="e-prv-id" class="form-control input-sm" readonly>
							</div>
							<div class="col-sm-6">
								<label>Name:</label> <input type="text" name="editProviderName"
									id="e-prv-name" class="form-control input-sm">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary" id="e-prv-submit">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /provider edit popup -->

	<!-- Status edit popup -->
	<div id="edit_status_popup" class="modal fade" tabindex="-1"
		role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit Status</h4>
				</div>
				<form class="form-horizontal" method="post"
					action="${pageContext.request.contextPath}/adminEditStatus">
					<div class="modal-body with-padding">

						<div class="form-group row">
							<div class="col-sm-6">
								<label>Id:</label> <input type="text" name="editStatusId"
									id="e-sta-id" class="form-control input-sm" readonly>
							</div>
							<div class="col-sm-6">
								<label>Name:</label> <input type="text" name="editStatusName"
									id="e-sta-name" class="form-control input-sm">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary" id="e-prv-submit">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /status edit popup -->

	<!-- Location edit popup -->
	<div id="edit_location_popup" class="modal fade" tabindex="-1"
		role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit Location</h4>
				</div>
				<form class="form-horizontal" method="post"
					action="${pageContext.request.contextPath}/adminEditLocation">
					<div class="modal-body with-padding">

						<div class="form-group row">
							<div class="col-sm-6">
								<label>Id:</label> <input type="text" name="editLocationId"
									id="e-loc-id" class="form-control input-sm" readonly>
							</div>
							<div class="col-sm-6">
								<label>Name:</label> <input type="text" name="editLocationName"
									id="e-loc-name" class="form-control input-sm">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary" id="e-prv-submit">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /location edit popup -->

</body>
</html>