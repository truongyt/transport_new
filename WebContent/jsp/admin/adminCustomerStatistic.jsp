<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	/**
	*@author PhucNT
	*/
%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>Customer Statistic</title>

<link
	href="${pageContext.request.contextPath}/lib2/css/bootstrap.min.css"
	rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.contextPath}/lib2/css/londinium-theme.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/styles.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/icons.css"
	rel="stylesheet" type="text/css">
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">

<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.orderbars.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.pie.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.time.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.animator.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/excanvas.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/flot.resize.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/sparkline.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uniform.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/select2.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputmask.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/autosize.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputlimit.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/listbox.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/multiselect.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/validate.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/tags.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/switch.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.full.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.queue.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/toolbar.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/daterangepicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fancybox.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/moment.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/jgrowl.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/datatables.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/tabletools.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/colorpicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fullcalendar.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/timepicker.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/application.js"></script>


<!-- YTAsia script -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/ytasia/statistic_customer.js"></script>
<!-- /YTAsia script -->


<!-- OnPage script -->
<script type="text/javascript">
	$(function() {

		/**
		 * Customer classify
		 */
		$.ajax({
			url : '${pageContext.request.contextPath}/getClassifyCustomer',
			type : "post",
			dataType : "json",
			data : {},
			success : function(result) {
				var data = result[0], detail = result[1];
				var classifyData = [];

				$('#c-total').text(detail.total);

				for (i = 0; i < data.length; i++) {
					var type = data[i].type;
					var number = data[i].number;

					switch (type) {
					case 1:
						classifyData[i] = {
							label : 'Khách thường',
							data : number
						}
						$('#c-normal').text(number);
						break;
					case 2:
						classifyData[i] = {
							label : 'Khách quen 1',
							data : number
						}
						$('#c-discount').text(number);
						break;
					case 3:
						classifyData[i] = {
							label : 'Khách quen 2',
							data : number
						}
						$('#c-loyal').text(number);
						break;
					}
				}

				setCustomerClassifyData(classifyData)
			}
		});

		/**
		 * By year statistic 
		 */
		$.ajax({
			url : '${pageContext.request.contextPath}/getCustomerByYear',
			type : "post",
			dataType : "json",
			data : {},
			success : function(result) {
				var data = result[0], detail = result[1];
				var yearData = [];
				var year = detail.year;

				for (i = 1; i < data.length; i++) {
					yearData.push([ data[i].month, data[i].number ]);
				}
				setByYearData(year, yearData);

				$("#s-y-total").text(detail.total);
				$("#s-y-average").text(detail.average);
			}
		});

	});
</script>
<!-- /onPage script -->

</head>

<body class="full-width">
	<!-- Page container -->
	<div class="page-container">

		<!-- Navbar -->
		<jsp:include page="_adminHeader.jsp"></jsp:include>


		<!-- Page content -->
		<div class="page-content">

			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3>
						Customer Statistic <small>All customers of YTAsia</small>
					</h3>
				</div>

				<div id="reportrange" class="range">
					<div class="visible-xs header-element-toggle">
						<a class="btn btn-primary btn-icon"><i class="icon-calendar"></i></a>
					</div>
				</div>
			</div>
			<!-- /page header -->

			<!-- Page body -->
			<div class="col-md-6">

				<!-- Customer classify -->
				<div class="panel panel-default">
					<div class="panel-heading">
						<h6 class="panel-title">
							<i class="icon-calendar2"></i> Customer classify
						</h6>
					</div>
					<div class="panel-body">
						<div class="graph-standard" id="classify_chart"></div>
					</div>
				</div>
				<!-- /customer classify -->

				<!-- By year statistics detail -->
				<h6>Details:</h6>
				<div class="col-md-6">

					<ul>
						<li>Total: <strong id="c-total" class="">379</strong> người
						</li>
						<li>Khách thường: <strong id="c-normal" class="">379</strong>
							người
						</li>
						<li>Khách quen 1: <strong id="c-discount" class="">379</strong>
							người
						</li>
						<li>Khách quen 2: <strong id="c-loyal" class="">379</strong>
							người
						</li>
					</ul>
				</div>
				<!-- /by year statistics detail -->

			</div>

			<div class="col-md-6">

				<!-- By year statistics -->
				<div class="panel panel-default">
					<div class="panel-heading">
						<h6 class="panel-title">
							<i class="icon-calendar2"></i> Annually statistic
						</h6>
					</div>
					<div class="panel-body">
						<div class="graph-standard" id="by_year_chart"></div>
					</div>
				</div>
				<!-- /by year statistics -->

				<!-- By year statistics detail -->
				<h6>Details:</h6>
				<div class="col-md-6">

					<ul>
						<li>Total: <strong id="s-y-total" class="">379</strong> người
						</li>
						<li>Average: <strong id="s-y-average" class="">379</strong>
							người/tháng
						</li>
					</ul>
				</div>
				<!-- /by year statistics detail -->

			</div>
			<!-- /page body -->
		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->

</body>
</html>