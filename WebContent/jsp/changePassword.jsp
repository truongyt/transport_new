<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	/**
	* @author PhucNT
	*/
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.11.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/lib/CSS/style.css">
<link href="${pageContext.request.contextPath}/lib/CSS/jquery-ui.css"
	rel="stylesheet" />

<script type="text/javascript">
	window.onload = function() {
		document.getElementById("pass1").onchange = validatePassword;
		document.getElementById("pass2").onchange = validatePassword;
		document.getElementById("pass2").onkeyup = validatePassword;
	}
	function validatePassword() {
		var pass2 = document.getElementById("pass2").value;
		var pass1 = document.getElementById("pass1").value;
		if (pass1 != pass2)
			document.getElementById("pass2").setCustomValidity(
					"Passwords Don't Match");
		else
			document.getElementById("pass2").setCustomValidity('');
		//empty string means no validation error
	}
</script>

<title>YTAsia社内システムへようこそ</title>
</head>
<body>
	<jsp:include page="_header_login.jsp"></jsp:include>
	<div class="container-fluid" style="width: 80%">
		<div class="row">
			<div class="well well-head"
				style="padding-bottom: 0; margin-top: 20px">
				<p>Change Password</p>
				<hr>
				<div>
					<h4 style="text-align: left; margin-bottom: 20px; color: red;">YTAsia社内システムへようこそ！パスワードを変更してください。</h4>
				</div>
				<form class="form-horizontal" role="form" method="post"
					action="${pageContext.request.contextPath}/changePassword">
					<div class="form-group">
						<label class="control-label col-sm-3">New Password</label>
						<div class="col-sm-4">
							<input type="password" placeholder="new password"
								onchange="validatePassword" name="pass1" id="pass1"
								class="form-control input-sm" required>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-3">Re-type new
							password:</label>
						<div class="col-sm-4">
							<input type="password" placeholder="confirm password"
								onchange="validatePassword" name="pass2" id="pass2"
								class="form-control input-sm" required>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-1">
							<button class="btn btn-primary" type="submit" style="width: 80px">Change</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>