<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	/**
	*@author PhucNT
	*/
%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>User Main</title>

<link href="lib2/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="lib2/css/londinium-theme.css" rel="stylesheet"
	type="text/css">
<link href="lib2/css/styles.css" rel="stylesheet" type="text/css">
<link href="lib2/css/icons.css" rel="stylesheet" type="text/css">
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">

<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

<script type="text/javascript"
	src="lib2/js/plugins/charts/sparkline.min.js"></script>

<script type="text/javascript"
	src="lib2/js/plugins/forms/uniform.min.js"></script>
<script type="text/javascript"
	src="lib2/js/plugins/forms/select2.min.js"></script>
<script type="text/javascript" src="lib2/js/plugins/forms/inputmask.js"></script>
<script type="text/javascript" src="lib2/js/plugins/forms/autosize.js"></script>
<script type="text/javascript"
	src="lib2/js/plugins/forms/inputlimit.min.js"></script>
<script type="text/javascript" src="lib2/js/plugins/forms/listbox.js"></script>
<script type="text/javascript"
	src="lib2/js/plugins/forms/multiselect.js"></script>
<script type="text/javascript"
	src="lib2/js/plugins/forms/validate.min.js"></script>
<script type="text/javascript" src="lib2/js/plugins/forms/tags.min.js"></script>
<script type="text/javascript" src="lib2/js/plugins/forms/switch.min.js"></script>

<script type="text/javascript"
	src="lib2/js/plugins/forms/uploader/plupload.full.min.js"></script>
<script type="text/javascript"
	src="lib2/js/plugins/forms/uploader/plupload.queue.min.js"></script>

<script type="text/javascript"
	src="lib2/js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript"
	src="lib2/js/plugins/forms/wysihtml5/toolbar.js"></script>

<script type="text/javascript"
	src="lib2/js/plugins/interface/daterangepicker.js"></script>
<script type="text/javascript"
	src="lib2/js/plugins/interface/fancybox.min.js"></script>
<script type="text/javascript" src="lib2/js/plugins/interface/moment.js"></script>
<script type="text/javascript"
	src="lib2/js/plugins/interface/jgrowl.min.js"></script>
<script type="text/javascript"
	src="lib2/js/plugins/interface/datatables.min.js"></script>
<script type="text/javascript"
	src="lib2/js/plugins/interface/tabletools.min.js"></script>
<script type="text/javascript"
	src="lib2/js/plugins/interface/colorpicker.js"></script>
<script type="text/javascript"
	src="lib2/js/plugins/interface/fullcalendar.min.js"></script>
<script type="text/javascript"
	src="lib2/js/plugins/interface/timepicker.min.js"></script>

<script type="text/javascript" src="lib2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="lib2/js/application.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/jquery.numeric.min.js"></script>


<!-- OnPage script -->
<script>
	var productsCache = [];

	$(function() {
		$('#sort').click();

		//--------------- SET NUMERIC INPUT ----------------//
		$('input#n-s-number').numeric();
		$('input#a-p-quantity').numeric();

		//--------------- ADD/DELETE PRODUCT ----------------//

		/**
		 * Set product id for new product form
		 */
		$(document).on('click', 'a#o-add-p', function() {
			var $id = $(this).data("id");
			$('#a-p-order-id').val($id);
			getProducts($id);
		});

		/**
		 * Add new product to database
		 */
		$(document).on('click', 'button#a-p-add', function() {
			type = $('#a-p-type').val();
			quantity = $('#a-p-quantity').val();
			name = $('#a-p-name').val();
			orderId = $('#a-p-order-id').val();

			$.ajax({
				url : '${pageContext.request.contextPath}/addProducts',
				type : "post",
				dateType : "json",
				data : {
					type : type,
					quantity : quantity,
					name : name,
					orderId : orderId
				},
				success : function(result) {
					getProducts(orderId);
				}
			});
		});

		/**
		 * Delete Product
		 */
		$(document).on('click', 'a#a-p-list-delete', function() {
			var $id = $(this).data("id");
			orderId = $('#a-p-order-id').val();

			$.ajax({
				url : '${pageContext.request.contextPath}/deleteProduct',
				type : "post",
				dateType : "json",
				data : {
					id : $id
				},
				success : function(result) {
					getProducts(orderId);
				}
			});

		});

		/**
		 * Get products
		 */
		function getProducts(id) {
			$
					.ajax({
						url : '${pageContext.request.contextPath}/getProducts',
						type : "post",
						dateType : "json",
						data : {
							orderId : id
						},
						success : function(result) {
							/**
							 * Set data from database to view
							 */
							clearNewProductForm();
							var subtotal = 0;
							var tax = 8;

							for (i = 0; i < result.length; i++) {

								subtotal = subtotal + result[i].price;

								$('#a-p-list')
										.append(
												'<li>'
														+ result[i].name
														+ ' <strong>'
														+ result[i].quantity
														+ '</strong> ('
														+ result[i].unit
														+ ') '
														+ '<a data-id="'
														+ result[i].id
														+ '" id="a-p-list-delete">xóa</a>'
														+ '</li>');
							}

							var total = subtotal + (subtotal / 100 * tax);
							$('#a-p-sub-total').text(subtotal + " vnđ");
							$('#a-p-tax').text(tax + "%");
							$('#a-p-total').text(total + " vnđ");
						}
					});
		}

		/**
		 * Clear form for new action
		 */
		function clearNewProductForm() {
			$('#a-p-quantity').val("");
			$('#a-p-name').val("");
			$('#a-p-list').text("");
			$('#a-p-sub-total').text("");
			$('#a-p-tax').text("");
			$('#a-p-total').text("");
		}

		//--------------- PRINT ----------------//

		/**
		 * Button print (invoice) on click 
		 */
		$("#btnPrint").click(function() {
			printElement(document.getElementById("printThis"));

			window.print();
		});

		/**
		 * Button print (invoice) function 
		 */
		function printElement(elem) {
			var domClone = elem.cloneNode(true);

			var $printSection = document.getElementById("printSection");

			if (!$printSection) {
				var $printSection = document.createElement("div");
				$printSection.id = "printSection";
				document.body.appendChild($printSection);
			}

			$printSection.innerHTML = "";

			$printSection.appendChild(domClone);
		}

		//--------------- NEW SUB PRODUCT ----------------//

		/**
		 * Set product id for new sub-product form
		 */
		$(document).on('click', 'a#o-new-sp', function() {
			var $id = $(this).data("id");
			$('#n-s-order-id').val($id);

		});

		/**
		 * Submit, add new sub-product list to database
		 */
		$("button#n-s-submit").click(function() {

			var orderId = $('#n-s-order-id').val();
			var number = $('#n-s-number').val();

			$('#n-s-order-id').val("");
			$('#n-s-number').val("");

			$.ajax({
				url : '${pageContext.request.contextPath}/addSubProduct',
				type : "post",
				dateType : "json",
				data : {
					orderId : orderId,
					number : number
				},
				success : function(result) {
				}
			});
		});

		//--------------- VIEW SUB PRODUCT ----------------//

		/**
		 * Get all sub products of order
		 */
		$(document)
				.on(
						'click',
						"a#o-view-sp",
						function() {
							$('#s-table').text("");

							var $id = $(this).data("id");
							$('#panel-order-below').text($id);

							$
									.ajax({
										url : '${pageContext.request.contextPath}/getSubProducts',
										type : "post",
										dataType : "json",
										data : {
											id : $id
										},
										success : function(result) {
											for (i = 0; i < result.length; i++) {
												if (result[i].location == "Out Japan") {
													$('#s-table')
															.append(
																	'<tr><td>'
																			+ result[i].fId
																			+ '</td><td>'
																			+ result[i].location
																			+ '</td><td>'
																			+ '<a href="${pageContext.request.contextPath}/subProductInVietnam?id='
																			+ result[i].fId
																			+ '" class="text">Nhập kho</a>'
																			+ '</td></tr>');
												} else {
													$('#s-table')
															.append(
																	'<tr><td>'
																			+ result[i].fId
																			+ '</td><td>'
																			+ result[i].location
																			+ '</td></tr>');
												}
											}
										}
									});
						});

		//--------------- VIEW INVOICE ----------------//

		/**
		 * Show invoice-popup when OrderId clicked
		 */
		$(document).on('click', 'a#o-invoice', function() {
			clearData();

			var $id = $(this).data("id");
			$.ajax({
				url : '${pageContext.request.contextPath}/viewInvoice',
				type : "post",
				dataType : "json",
				data : {
					orderId : $id
				},
				success : function(result) {
					var detail = result[0], products = result[1];
					setDetail(detail);
					setProducts(products)
				}
			});
		});

		/**
		 * Set data for detail-popup when popup showed
		 */
		function setDetail(detail) {
			$('#d-sender-name').text(detail.senderName);
			$('#d-sender-email').text(detail.senderEmail);
			$('#d-sender-address').text(detail.senderAddress);
			$('#d-sender-phone').text(detail.senderPhone);

			$('#d-receipter-name').text(detail.receipterName);
			$('#d-receipter-email').text(detail.receipterEmail);
			$('#d-receipter-address').text(detail.receipterAddress);
			$('#d-receipter-phone').text(detail.receipterPhone);

			$('#d-order-id').text(detail.fId);
			$('#d-order-time').text(detail.time);
			$('#d-responsible').text(detail.user);
			$('#d-other').text(detail.other);

			switch (detail.paidStatus) {
			case 0:
				$('#d-paid-status').text('Unpaid');
				$("#d-paid-status").attr('class',
						'label label-danger pull-right');
				break;

			case 1:
				$('#d-paid-status').text('Paid');
				$("#d-paid-status").attr('class',
						'label label-success pull-right');
				break;
			}
		}

		/**
		 * Set data for invoice-popup when popup showed
		 */
		function setProducts(products) {
			var subtotal = 0;
			var tax = 0;
			var total = 0;

			for (i = 0; i < products.length; i++) {
				$('#d-product-table').append(
						'<tr><td>' + products[i].type + '</td><td>'
								+ products[i].name + '</td><td>'
								+ products[i].unitPrice + '</td><td>'
								+ products[i].quantity + '</td><td><strong>'
								+ products[i].price + '</strong></td></tr>');
				subtotal = subtotal + products[i].price;
			}
			total = subtotal + (subtotal / 100 * tax);
			$('#d-subtotal').text(subtotal + ' jpy');
			$('#d-tax').text(tax + '%');
			$('#d-total').text(total + ' jpy');
		}

		/**
		 * Clear data for detail-popup when popup closed
		 */
		function clearData() {
			$('#d-sender-name').text("");
			$('#d-sender-email').text("");
			$('#d-sender-address').text("");
			$('#d-sender-phone').text("");

			$('#d-receipter-name').text("");
			$('#d-receipter-email').text("");
			$('#d-receipter-address').text("");
			$('#d-receipter-phone').text("");

			$('#d-order-id').text("");
			$('#d-order-time').text("");
			$('#d-responsible').text("");
			$('#d-paid-status').text("");
			$('#d-other').text("");

			$('#d-product-table').text('');
		}

		//--------------- CONFIRMATION ----------------//

		/**
		 * Confirmation when delete button clicked
		 */

		$('.confirmation').on('click', function() {
			console.log("delete");
			return confirm('Are you sure to delete this order?');
		})

		//--------------- PAY (CHANGE PAID STATUS) ----------------//

		/**
		 * Pay
		 */
		$(document).on(
				'click',
				'button#btnPay',
				function() {
					id = $('#d-order-id').text();

					$.ajax({
						url : '${pageContext.request.contextPath}/payOrder',
						type : "post",
						dateType : "json",
						data : {
							id : id
						},
						success : function(result) {
							$('#d-paid-status').text('Paid');
							$("#d-paid-status").attr('class',
									'label label-success pull-right');
							$("#" + id + " td:nth-child(5)").text("");
							$("#" + id + " td:nth-child(5)").append(
									"<strong>Paid</strong>");
							$("#" + id + " td:nth-child(5)").attr('class',
									'text-success');
						}
					});

				});

		//--------------- UPDATE ORDER ----------------//

	});
</script>
<!-- /onPage script -->

</head>

<body class="full-width">
	<!-- Page container -->
	<div class="page-container">

		<!-- Navbar -->
		<jsp:include page="_userHeader.jsp"></jsp:include>

		<!-- Page content -->
		<div class="page-content">

			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3>
						Orders table <small>All Transport orders of YTAsia</small>
					</h3>
				</div>
			</div>
			<!-- /page header -->

			<!-- Orders -->
			<div class="col-sm-7">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h6 class="panel-title">
							<i class="icon-paragraph-justify"></i> Orders
						</h6>

						<ul class="breadcrumb-buttons collapse">
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown"><i class="icon-search3"></i> <span>Search</span>
									<b class="caret"></b></a>
								<div class="popup dropdown-menu dropdown-menu-right">
									<form
										action="${pageContext.request.contextPath}/userSearchOrderBy"
										method="post" class="breadcrumb-search">
										<div class="row">
											<div class="col-xs-2">
												<select data-placeholder="Shipment" class="clear-results"
													name="searchShipment" tabindex="2">
													<option value=""></option>
													<c:forEach items="${listShipment}" var="shipment">
														<option value="${shipment.getShipmentId()}">${shipment.getDepartTime()}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-2">
												<select data-placeholder="Status" class="clear-results"
													tabindex="2" name="searchStatus">
													<option value=""></option>
													<c:forEach items="${listStatus}" var="status">
														<option value="${status.getStatusId()}">${status.getStatusName()}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<input type="submit" class="btn btn-block btn-success"
											value="Search">
									</form>
								</div></li>
							<li class="dropdown"><a
								href="${pageContext.request.contextPath}/userMainPage" id="all"
								style="float: left">All</a></li>
						</ul>
					</div>

					<div class="datatable">
						<table class="table table-striped">
							<thead>
								<tr>
									<th id="sort">#</th>
									<th>Order ID</th>
									<th>Status</th>
									<th>Location</th>
									<th>Paid</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${orders}" var="order">
									<tr id="${order.getTransportOrderId()}">
										<td>${order.getId()}</td>
										<td>${order.getTransportOrderId()}</td>
										<td>${order.getStatusId()}</td>
										<td>${order.getLocationId()}</td>
										<td><c:if test="${order.getPaidStatus() == '0'}">
												<span class="text-danger"><strong>Unpaid</strong></span>
											</c:if> <c:if test="${order.getPaidStatus() == '1'}">
												<span class="text-success"><strong>Paid</strong></span>
											</c:if></td>
										<td><a id="o-invoice"
											data-id="${order.getTransportOrderId()}" data-toggle="modal"
											href="#invoice-popup"
											class="btn btn-warning btn-xs icon-coin"></a> <c:if
												test="${order.getStatusId() != 'DET'}">
												<c:if test="${order.getLocationId() == 'WAI'}">
													<div class="btn-group">
														<button type="button"
															class="btn btn-info btn-xs icon-cog dropdown-toggle"
															data-toggle="dropdown"></button>
														<ul class="dropdown-menu icons-right dropdown-menu-right">
															<li><a
																href="${pageContext.request.contextPath}/userUpdateOrderPage?id=${order.getTransportOrderId()}"><i
																	class="icon-cog"></i> Edit </a></li>
															<li><a id="o-add-p"
																data-id="${order.getTransportOrderId()}"
																data-toggle="modal" href="#add-product-popup"><i
																	class="icon-cart-add"></i> Add Product </a></li>
															<li><a
																href="${pageContext.request.contextPath}/userDonePreOrder?id=${order.getTransportOrderId()}"><i
																	class="icon-cart-checkout"></i> Complete</a></li>
														</ul>
													</div>

													<a
														href="${pageContext.request.contextPath}/userDeleteOrder?from=new&id=${order.getTransportOrderId()}"
														class="btn btn-danger btn-xs confirmation icon-remove3"></a>
												</c:if>
												<c:if test="${order.getLocationId() != 'WAI'}">
													<c:if test="${order.getLocationId() == 'JPI'}">
														<a id="o-new-sp" data-id="${order.getTransportOrderId()}"
															data-toggle="modal" href="#new-sub-product-popup"
															class="btn btn-primary btn-xs icon-tree2"></a>
													</c:if>
													<a id="o-view-sp" data-id="${order.getTransportOrderId()}"
														class="btn btn-success btn-xs icon-search3"></a>
													<a
														href="${pageContext.request.contextPath}/userDeleteOrder?from=new&id=${order.getTransportOrderId()}"
														class="btn btn-danger btn-xs confirmation icon-remove3"></a>
												</c:if>
											</c:if> <c:if test="${order.getStatusId()== 'DET'}">
												<a
													href="${pageContext.request.contextPath}/userPutBackOrder?&id=${order.getTransportOrderId()}"
													class="btn btn-success btn-xs">Hồi sinh</a>
											</c:if></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- /orders -->

			<!-- Sub-Products -->
			<div class="col-sm-5">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h6 class="panel-title">Sub-Product list</h6>
						<h6 id="panel-order-below" class="panel-title text-danger"></h6>
					</div>
					<div class="table-responsive">
						<table class="table table-striped ">
							<thead>
								<tr>
									<th>#id</th>
									<th>Location</th>
								</tr>
							</thead>
							<tbody id="s-table">

							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- /sub-products -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<!-- Invoice popup -->
	<div id="invoice-popup" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Modal title</h4>
				</div>

				<!-- New invoice template -->
				<div id="printThis" class="panel">
					<div class="panel-body">

						<div class="row invoice-header">
							<div class="col-sm-6">
								<h3>YTAsia</h3>
								<span>Best Japan - VietNam transport service ever !</span>
							</div>

							<div class="col-sm-6">
								<ul class="invoice-details">
									<li><h6>
											Invoice # <strong id="d-order-id" class="text-danger"></strong>
										</h6></li>
									<li>Date of Invoice: <strong id="d-order-time"></strong></li>
								</ul>
							</div>
						</div>


						<div class="row">
							<div class="col-sm-3">
								<h6>Sender:</h6>
								<ul>
									<li>Name:<strong id="d-sender-name" class="pull-right"></strong></li>
									<li>Email:<strong id="d-sender-email" class="pull-right"></strong></li>
									<li>Address:<strong id="d-sender-address"
										class="pull-right"></strong></li>
									<li>Phone:<strong id="d-sender-phone" class="pull-right"></strong></li>
								</ul>
							</div>

							<div class="col-sm-1"></div>

							<div class="col-sm-3">
								<h6>Recipient:</h6>
								<ul>
									<li>Name:<strong id="d-receipter-name" class="pull-right"></strong></li>
									<li>Email:<strong id="d-receipter-email"
										class="pull-right"></strong></li>
									<li>Address:<strong id="d-receipter-address"
										class="pull-right"></strong></li>
									<li>Phone:<strong id="d-receipter-phone"
										class="pull-right"></strong></li>
								</ul>
							</div>

							<div class="col-sm-1"></div>

							<div class="col-sm-4">
								<h6>Invoice Details:</h6>
								<ul>
									<li>Responsible: <a href="#" class="pull-right"
										id="d-responsible"></a></li>
									<li class="invoice-status"><strong>Current
											status: <span id="d-paid-status"
											class="label label-danger pull-right">Unpaid</span>
									</strong></li>
								</ul>
							</div>
						</div>
					</div>

					<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>Type</th>
									<th>Description</th>
									<th>Unit price</th>
									<th>Quantity</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody id="d-product-table">

							</tbody>
						</table>
					</div>

					<div class="panel-body">
						<div class="col-sm-8">
							<h6>Detail:</h6>
							<!-- <textarea id="d-other" style="border: none; min-height: 200px"
								class="form-control"></textarea> -->
							<div id="d-other" style="border: none; white-space: pre" class="form-control"></div>
						</div>

						<div class="col-sm-4">
							<h6>Total:</h6>
							<table class="table">
								<tbody>
									<tr>
										<th>Subtotal:</th>
										<td id="d-subtotal" class="text-right"></td>
									</tr>
									<tr>
										<th>Tax:</th>
										<td id="d-tax" class="text-right"></td>
									</tr>
									<tr>
										<th>Total:</th>
										<td id="d-total" class="text-right text-danger"><h6></h6></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- /new invoice template -->
			</div>
			<div class="btn-group pull-left">
				<button type="button" id="btnPay" class="btn btn-success">
					<i class="icon-checkbox-partial"></i> Confirm payment
				</button>
			</div>
			<div class="btn-group pull-right">
				<button type="button" id="btnPrint" class="btn btn-info">
					<i class="icon-print2"></i> Print
				</button>
			</div>
		</div>
	</div>
	<!-- /invoice popup -->

	<!-- New sub-product -->
	<div id="new-sub-product-popup" class="modal fade" tabindex="-1"
		role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<i class="icon-paragraph-justify2"></i> Edit sub-product
					</h4>
				</div>

				<div class="modal-body with-padding">

					<div class="form-group row">
						<div class="col-sm-8">
							<label>Order Id:</label> <input type="text" id="n-s-order-id"
								class="form-control input-sm" readonly="readonly">
						</div>
						<div class="col-sm-4">
							<label>Number:</label> <input type="text" id="n-s-number"
								class="form-control input-sm">
						</div>

					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
					<button id="n-s-submit" class="btn btn-primary"
						data-dismiss="modal">Submit</button>
				</div>
			</div>
		</div>
	</div>
	<!-- /new sub-product -->


	<!-- Add Product -->
	<div id="add-product-popup" class="modal fade" tabindex="-1"
		role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<i class="icon-paragraph-justify2"></i> Add product
					</h4>
				</div>

				<div class="modal-body with-padding">
					<div class="form-group row">
						<div class="col-md-4">
							<label>Order:</label> <input type="text" id="a-p-order-id"
								class="form-control" readonly="readonly">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-4">
							<label>Type:</label> <select class="select-search" tabindex="1"
								id="a-p-type">
								<c:forEach items="${listProductType}" var="product">
									<option value="${product.getTypeId()}">${product.getTypeName()}
										(${product.getPrice()} vnđ/${product.getUnit()})</option>
								</c:forEach>
							</select>
						</div>

						<div class="col-md-1"></div>

						<div class="col-md-2">
							<label>Quantity:</label> <input type="text" id="a-p-quantity"
								tabindex="2" class="form-control">
						</div>

						<div class="col-md-5">
							<label>Name:</label> <input type="text" id="a-p-name"
								tabindex="3" class="form-control">
						</div>
					</div>

					<div class="form-actions">
						<button id="a-p-add" class="btn btn-success" type="submit"
							tabindex="4" style="float: right">Add</button>
					</div>
				</div>
				<div class="modal-body with-padding">
					<div class="form-group row">
						<div class="col-sm-7">
							<h6>Products:</h6>
							<ul id="a-p-list">

							</ul>
						</div>

						<div class="col-sm-5">
							<h6>Total:</h6>
							<table class="table">
								<tbody>
									<tr>
										<th>Subtotal:</th>
										<td id="a-p-sub-total" class="text-right"></td>
									</tr>
									<tr>
										<th>Tax:</th>
										<td id="a-p-tax" class="text-right"></td>
									</tr>
									<tr>
										<th>Total:</th>
										<td id="a-p-total" class="text-right text-danger"><h6></h6></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /add product -->

</body>
</html>