<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	/**
	*@author PhucNT
	*/
%>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>New Pre Order</title>

<link
	href="${pageContext.request.contextPath}/lib2/css/bootstrap.min.css"
	rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.contextPath}/lib2/css/londinium-theme.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/styles.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/icons.css"
	rel="stylesheet" type="text/css">
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">

<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/sparkline.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uniform.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/select2.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputmask.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/autosize.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputlimit.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/listbox.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/multiselect.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/validate.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/tags.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/switch.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.full.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.queue.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/toolbar.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/daterangepicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fancybox.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/moment.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/jgrowl.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/datatables.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/tabletools.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/colorpicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fullcalendar.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/timepicker.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/application.js"></script>


<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/jquery.datetimepicker.full.min.js"></script>
<link
	href="${pageContext.request.contextPath}/lib2/css/jquery.datetimepicker.css"
	rel="stylesheet" type="text/css">

<!-- OnPage script -->
<script type="text/javascript">
	function onSenderSelect() {
		$('#o-sender-d-name').text("");
		$('#o-sender-d-phone').text("");
		$('#o-sender-d-email').text("");
		$('#o-sender-d-address').text("");

		var selectBox = document.getElementById("o-sender-select");
		var id = selectBox.options[selectBox.selectedIndex].value;

		$.ajax({
			url : '${pageContext.request.contextPath}/getCustomer',
			type : "post",
			dateType : "json",
			data : {
				id : id,
			},
			success : function(result) {
				$('#o-sender-d-name').text(result.name);
				$('#o-sender-d-phone').text(result.phone);
				$('#o-sender-d-email').text(result.email);
				$('#o-sender-d-address').text(result.address);
			}
		});
	}

	function onReceipterSelect() {
		$('#o-receipter-d-name').text("");
		$('#o-receipter-d-phone').text("");
		$('#o-receipter-d-email').text("");
		$('#o-receipter-d-address').text("");

		var selectBox = document.getElementById("o-receipter-select");
		var id = selectBox.options[selectBox.selectedIndex].value;

		$.ajax({
			url : '${pageContext.request.contextPath}/getCustomer',
			type : "post",
			dateType : "json",
			data : {
				id : id,
			},
			success : function(result) {
				$('#o-receipter-d-name').text(result.name);
				$('#o-receipter-d-phone').text(result.phone);
				$('#o-receipter-d-email').text(result.email);
				$('#o-receipter-d-address').text(result.address);
			}
		});
	}

	$(function() {

		/**
		 * Submit, add new customer to database
		 */
		$(document)
				.on(
						'click',
						'#n-submit',
						function() {

							var name = $('#n-name').val();
							var email = $('#n-email').val();
							var phone = $('#n-phone').val();
							var type = $('#n-type').val();
							var address = $('#n-address').val();

							$('#n-name').val("");
							$('#n-email').val("");
							$('#n-phone').val("");
							$('#n-type').val("");
							$('#n-address').val("");

							$
									.ajax({
										url : '${pageContext.request.contextPath}/userAddCustomer',
										type : "post",
										dateType : "json",
										data : {
											name : name,
											email : email,
											phone : phone,
											type : type,
											address : address
										},
										success : function(result) {
											$('#o-sender-select').text('');
											$('#o-sender-select').append(
													$('<option>', {
														value : '',
														text : ''
													}));
											for (i = 0; i < result.length; i++) {
												$('#o-sender-select')
														.append(
																$(
																		'<option>',
																		{
																			value : result[i].id,
																			text : result[i].name
																					+ ' - '
																					+ result[i].phone
																					+ ' - '
																					+ result[i].address
																					+ ' - '
																					+ result[i].email
																		}));

											}

											$('#o-receipter-select').text('');
											$('#o-receipter-select').append(
													$('<option>', {
														value : '',
														text : ''
													}));
											for (i = 0; i < result.length; i++) {
												$('#o-receipter-select')
														.append(
																$(
																		'<option>',
																		{
																			value : result[i].id,
																			text : result[i].name
																					+ ' - '
																					+ result[i].phone
																					+ ' - '
																					+ result[i].address
																					+ ' - '
																					+ result[i].email
																		}));

											}
										}
									});
						});

	});
</script>
<!-- /onPage script -->


</head>

<body class="full-width">
	<!-- Page container -->
	<div class="page-container">

		<!-- Navbar -->
		<jsp:include page="_userHeader.jsp"></jsp:include>


		<!-- Page content -->
		<div class="page-content">

			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3 id="h3">
						New Pre-Order<small>Manage Product and Sub-Product of
							Shipment</small>
					</h3>
				</div>
			</div>
			<!-- /page header -->

			<!-- New order form -->
			<div class="col-md-7">
				<form method="post" class="validate"
					action="${pageContext.request.contextPath}/userDoNewPreOrder"
					enctype="multipart/form-data" role="form">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h6 class="panel-title">New transport order form</h6>
						</div>
						<div class="panel-body">

							<!-- Row 1 -->
							<div class="form-group row">
								<div class="col-md-4">
									<label style="color: white">nothing here </label>
									<div>
										<button data-toggle="modal" href="#new_customer_popup"
											type="button" id="new-sender" class="btn btn-success">New
											Customer</button>
									</div>
								</div>

								<div class="col-md-4">
									<label>Shipment: <span class="mandatory">*</span></label>
									<div>
										<select data-placeholder="select shipment"
											class="clear-results required" name="shipment" tabindex="5">
											<option value=""></option>
											<c:forEach items="${shipments}" var="shipment">
												<option value="${shipment.getShipmentId()}">${shipment.getDepartTime()}</option>
											</c:forEach>
										</select>
									</div>
								</div>

								<div class="col-md-4">
									<label>Transport Code: </label> <input type="text" tabindex="6"
										id="test" name="transCode" class="form-control" placeholder="">
								</div>

								<!-- Product cache Json -->
								<input type="hidden" name="productsCache" id="p-cache"
									class="form-control">
								<!-- /product cache Json -->

							</div>
							<!-- /row 1 -->

							<div class="form-group row">
								<div class="col-md-4"></div>
							</div>

							<!-- Row 2 -->
							<div class="form-group row">
								<div class="col-md-6">
									<label>Sender: <span class="mandatory">*</span></label>
									<div>
										<select data-placeholder="search by phone number"
											name="sender" id="o-sender-select"
											onchange="onSenderSelect();" class="clear-results required"
											tabindex="7">
											<option value=""></option>
											<c:forEach items="${customers}" var="customer">
												<option value="${customer.getCustomerId()}">${customer.getName()}
													- ${customer.getPhone()} - ${customer.getAdress()} -
													${customer.getEmail()}</option>
											</c:forEach>
										</select>
									</div>

									<ul>
										<li>Name : <strong id="o-sender-d-name" class=""></strong></li>
										<li>Email : <strong id="o-sender-d-email" class=""></strong></li>
										<li>Phone : <strong id="o-sender-d-phone"
											class="text-danger"></strong></li>
										<li>Address : <strong id="o-sender-d-address" class=""></strong></li>
									</ul>

								</div>

								<div class="col-md-6">
									<label>Recipient: <span class="mandatory">*</span></label>
									<div>
										<select data-placeholder="search by phone number"
											name="receipter" onchange="onReceipterSelect();"
											id="o-receipter-select" class="clear-results " tabindex="8"
											required>
											<option value=""></option>
											<c:forEach items="${customers}" var="customer">
												<option value="${customer.getCustomerId()}">${customer.getName()}
													- ${customer.getPhone()} - ${customer.getAdress()} -
													${customer.getEmail()}</option>
											</c:forEach>
										</select>
									</div>

									<ul>
										<li>Name : <strong id="o-receipter-d-name" class=""></strong></li>
										<li>Email : <strong id="o-receipter-d-email" class=""></strong></li>
										<li>Phone : <strong id="o-receipter-d-phone"
											class="text-danger"></strong></li>
										<li>Address : <strong id="o-receipter-d-address" class=""></strong></li>
									</ul>
								</div>
							</div>
							<!-- /row 2 -->

							<div class="form-group row">
								<div class="col-md-4"></div>
							</div>

							<!-- Row 3 -->
							<div class="form-group row">
								<div class="col-md-6">
									<label>Other:</label>
									<textarea rows="5" cols="5" name="other"
										class="elastic form-control"></textarea>
								</div>
								<div class="col-md-6">
									<label>Attachment: </label>
									<div class="block">
										<input type="file" name="attach" class="styled form-control"
											class="file"> <input type="file" name="attach"
											class="styled form-control" class="file"><input
											type="file" name="attach" class="styled form-control"
											class="file"><span class="help-block">Accepted
											formats: gif, png, jpg. Max file size 2Mb</span>
									</div>
								</div>
							</div>
							<!-- /row 3 -->

							<!-- Action button -->
							<div class="form-actions text-right">
								<input type="button" id="o-cancel" value="Cancel"
									class="btn btn-danger"> <input type="submit"
									value="Submit report" class="btn btn-primary" tabindex="9">
							</div>
							<!-- /action button -->
						</div>
					</div>
				</form>
			</div>
			<!-- /new order form -->

			<!-- New Customer popup -->
			<div id="new_customer_popup" class="modal fade" tabindex="-1"
				role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title">
								<i class="icon-paragraph-justify2"></i> New Customer
							</h4>
						</div>

						<!-- Form inside modal -->

						<div class="modal-body with-padding">

							<div class="form-group row">
								<div class="col-sm-6">
									<label>Name:</label> <input type="text" name="newCustomerName"
										id="n-name" class="form-control input-sm">
								</div>
								<div class="col-sm-6">
									<label>Email:</label> <input type="text"
										name="newCustomerEmail" id="n-email"
										class="form-control input-sm">
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm-6">
									<label>Phone:</label> <input type="text"
										name="newCustomerPhone" id="n-phone"
										class="form-control input-sm">
								</div>
								<div class="col-sm-6">
									<label>Type:</label> <select class="form-control input-sm "
										name="newCustomerType" id="n-type" tabindex="5">
										<c:forEach items="${types}" var="type">
											<option value="${type.getId()}">${type.getType()}</option>
										</c:forEach>
									</select>
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm-12">
									<label>Address:</label> <input type="text"
										name="newCustomerAddress" id="n-address"
										class="form-control input-sm">
								</div>
							</div>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-warning"
								data-dismiss="modal">Close</button>
							<button class="btn btn-primary" id="n-submit"
								data-dismiss="modal">Submit</button>
						</div>

					</div>
				</div>
			</div>
			<!-- /new customer popup -->

		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->

</body>
</html>