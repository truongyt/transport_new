<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%
	/**
	*@author PhucNT
	*/
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<!-- Navbar -->
	<div class="navbar navbar-inverse" role="navigation">
		<div class="navbar-header">
			<a class="navbar-brand" href="#"><img src="lib2/images/logo.png"
				alt="Londinium"></a>
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#navbar-icons">
				<span class="sr-only">Toggle navbar</span> <i class="icon-grid3"></i>
			</button>
			<button type="button" class="navbar-toggle offcanvas">
				<span class="sr-only">Toggle navigation</span> <i
					class="icon-paragraph-justify2"></i>
			</button>
		</div>

		<ul class="nav navbar-nav collapse" id="navbar-menu">
			<li><a href="${pageContext.request.contextPath}/userMainPage"><span>Orders</span>
					<i class="icon-database"></i></a></li>
			<li class="dropdown"><a href="#" class="dropdown-toggle"
				data-toggle="dropdown"><i class="icon-plus-circle"></i> <span>New Order</span>
					<b class="caret"></b></a>
				<ul class="dropdown-menu dropdown-menu-right icons-right">
					<li><a href="${pageContext.request.contextPath}/userNewFullOrder"><span>Full
								Order</span> <i class=""></i></a></li>
					<li><a href="${pageContext.request.contextPath}/userNewPreOrder"><span>Pre
								Order</span> <i class=""></i></a></li>
				</ul></li>

			<li><a
				href="${pageContext.request.contextPath}/userShipmentPage"><span>Shipment</span>
					<i class="icon-airplane2"></i></a></li>
		</ul>

		<ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">
			<li class="user dropdown"><a class="dropdown-toggle"
				data-toggle="dropdown"> <span>${sessionScope.userName}</span> <i
					class="caret"></i>
			</a>
				<ul class="dropdown-menu dropdown-menu-right icons-right">
					<li><a href="#"><i class="icon-user"></i> Profile</a></li>
					<c:if test="${sessionScope.type=='admin'}">
						<li><a
							href="${pageContext.request.contextPath}/adminMainPage"><i
								class="icon-skull"></i>Admin Mode</a></li>
					</c:if>
					<li><a href="${pageContext.request.contextPath}/logout"><i
							class="icon-exit"></i> Logout</a></li>
				</ul></li>
		</ul>
	</div>
	<!-- /navbar -->
</body>
</html>