<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	/**
	*@author PhucNT
	*/
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>Transport Login</title>

<link
	href="${pageContext.request.contextPath}/lib2/css/bootstrap.min.css"
	rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.contextPath}/lib2/css/londinium-theme.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/styles.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/lib2/css/icons.css"
	rel="stylesheet" type="text/css">
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">

<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/charts/sparkline.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uniform.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/select2.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputmask.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/autosize.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/inputlimit.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/listbox.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/multiselect.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/validate.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/tags.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/switch.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.full.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/uploader/plupload.queue.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/forms/wysihtml5/toolbar.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/daterangepicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fancybox.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/moment.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/jgrowl.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/datatables.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/colorpicker.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/fullcalendar.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/plugins/interface/timepicker.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/application.js"></script>

<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.11.3.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib2/js/jsencrypt.min.js"></script>

<script>
	var public_key = '-----BEGIN PUBLIC KEY----- MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDEvUE4XDxm/YkgBPuBWdsUj6H+ WBgPWM8e7EXPyAm9E1l1KH22cuPl+tOALU0a5YQh3zXUiAWWcSk3dl7CZgwJL5tz 54fewVXVp25Uq3h0238tgGlhbnzyHlQ9UEaTwCEA+c74nvWfzMNTYSuyES9/i/UL mzPMmGPZdKHGiTPBWwIDAQAB -----END PUBLIC KEY-------';

	function rsa() {
		var val = $("#password").val();

		var encrypt = new JSEncrypt();
		encrypt.setPublicKey(public_key);

		$("#password").val(encrypt.encrypt(val));
	}

	$(document).ready(function() {
		var changePass = "${changePass}";
		console.log(changePass);
		if (changePass != "")
			alert("パスワード変更出来ました。もう一度ログインして下さい。")
	})
</script>
</head>

<body class="full-width page-condensed">

	<!-- Navbar -->
	<div class="navbar navbar-inverse" role="navigation">
		<div class="navbar-header">
			<a class="navbar-brand" href="#"><img
				src="${pageContext.request.contextPath}/lib2/images/logo.png"
				alt="Londinium"></a>
		</div>
	</div>
	<!-- /navbar -->


	<!-- Login wrapper -->
	<div class="login-wrapper">
		<form role="form" method="POST"
			action="${pageContext.request.contextPath}/doLogin" name="loginForm">
			<div class="popup-header">
				<span class="text-semibold">User Login</span>
				<!-- <div class="btn-group pull-right">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
						class="icon-cogs"></i></a>
					<ul class="dropdown-menu icons-right dropdown-menu-right">
						<li><a href="#"><i class="icon-people"></i> Change user</a></li>
						<li><a href="#"><i class="icon-info"></i> Forgot
								password?</a></li>
						<li><a href="#"><i class="icon-support"></i> Contact
								admin</a></li>
						<li><a href="#"><i class="icon-wrench"></i> Settings</a></li>
					</ul>
				</div> -->
			</div>
			<div class="well">
				<div class="form-group has-feedback">
					<label>Username</label> <input id="userName" name="userName"
						type="text" class="form-control" placeholder="Username" required>
					<i class="icon-users form-control-feedback"></i>
				</div>

				<div class="form-group has-feedback">
					<label>Password</label> <input name="password" id="password"
						type="password" class="form-control" placeholder="Password"
						required> <i class="icon-lock form-control-feedback"></i>
				</div>

				<div class="row form-actions">
					<div class="col-xs-6">
						<!-- <div class="checkbox checkbox-success">
							<label> <input type="checkbox" class="styled">
								Remember me
							</label>
						</div> -->
					</div>

					<div class="col-xs-6">
						<button type="submit" class="btn btn-warning pull-right"
							onClick="rsa()">
							<i class="icon-menu2"></i> Sign in
						</button>
						<!-- <button type="submit" class="btn btn-warning pull-right">
							<i class="icon-menu2"></i> Sign in
						</button> -->
					</div>
				</div>
			</div>
		</form>
	</div>
	<!-- /login wrapper -->


	<!-- Footer -->
	<div class="footer clearfix">
		<div class="pull-left">
			2016. <a href="http://ytasia.co.jp">YTAsia</a> Transport
		</div>
		<!-- <div class="pull-right icons-group">
			<a href="#"><i class="icon-screen2"></i></a> <a href="#"><i
				class="icon-balance"></i></a> <a href="#"><i class="icon-cog3"></i></a>
		</div> -->
	</div>
	<!-- /footer -->


</body>
</html>